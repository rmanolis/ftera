package ftutils

import (
	"errors"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"

	"github.com/rakyll/magicmime"
)

const MAX_MEMORY = 6 * 1024 * 1024

func Files(r *http.Request) ([][]byte, error) {
	form, err := MultiForm(r)
	if err != nil {
		return nil, err
	}
	return ReadFiles(form)
}

func MultiForm(r *http.Request) (*multipart.Form, error) {
	err := r.ParseMultipartForm(MAX_MEMORY)
	return r.MultipartForm, err
}

func ReadFiles(form *multipart.Form) ([][]byte, error) {
	mm, err := magicmime.New(magicmime.MAGIC_MIME_TYPE | magicmime.MAGIC_SYMLINK | magicmime.MAGIC_ERROR)
	if err != nil {
		panic(err)
	}

	files := form.File
	buf_ls := [][]byte{}
	for _, fileHeaders := range files {
		for _, fileHeader := range fileHeaders {
			log.Println(fileHeader.Filename)
			file, err := fileHeader.Open()
			if err != nil {
				return nil, err
			}
			buf, err := ioutil.ReadAll(file)
			if err != nil {
				return nil, err
			}
			mt, err := mm.TypeByBuffer(buf)
			if err != nil {
				log.Println(err)
				return nil, err
			}
			if mt != "image/jpeg" && mt != "image/png" {
				return nil, errors.New("it is not the correct type")
			}

			buf_ls = append(buf_ls, buf)
		}
	}
	return buf_ls, nil
}
