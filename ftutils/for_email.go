package ftutils

import (
	"log"
	"net/smtp"
	"strconv"
)

type EmailUser struct {
	Email       string
	Username    string
	Password    string
	EmailServer string
	Port        int
}

type SmtpTemplateData struct {
	To      string
	Subject string
	Body    string
}

var GmailUser = EmailUser{
	Email:       "noreply@pullmeout.com",
	Username:    "noreply@pullmeout.com",
	Password:    "xyz32emailserver32xyz",
	EmailServer: "smtp.gmail.com",
	Port:        587,
}

//TODO check port and ssl

func SendMail(eu *EmailUser, std *SmtpTemplateData) {
	auth := smtp.PlainAuth("",
		eu.Username,
		eu.Password,
		eu.EmailServer)
	body := "To: " + std.To + "\r\nSubject: " +
		std.Subject + "\r\n\r\n" + std.Body
	err := smtp.SendMail(eu.EmailServer+":"+strconv.Itoa(eu.Port), auth, eu.Email,
		[]string{std.To}, []byte(body))
	if err != nil {
		log.Fatal(err)
	}
}
