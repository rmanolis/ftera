package ftutils

import (
	"github.com/garyburd/redigo/redis"
)

func Publish(channel string, value interface{}) {
	c, err := redis.Dial("tcp", ":6379")
	if err != nil {
		panic(err)
	}
	defer c.Close()
	c.Do("PUBLISH", channel, value)
}

type RedisMessage struct {
	Channel string
	Pattern string
	Message []byte
}
