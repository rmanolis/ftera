package ftutils

import (
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

func PageSize(r *http.Request) (int, int) {
	page_str := r.FormValue("page")
	page, err := strconv.Atoi(page_str)
	if err != nil {
		page = 0
	}

	size_str := r.FormValue("size")
	size, err := strconv.Atoi(size_str)
	if err != nil {
		size = 10
	}

	return page, size
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandSeq(n int) string {
	rand.Seed(time.Now().Unix())
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
