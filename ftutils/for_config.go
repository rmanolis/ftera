package ftutils

import (
	"encoding/json"
	"os"
)

func Configuration(filename string) (map[string]string, error) {
	file, _ := os.Open("../" + filename)
	decoder := json.NewDecoder(file)
	configuration := map[string]string{}
	err := decoder.Decode(&configuration)
	if err != nil {
		return configuration, err
	}
	return configuration, nil
}
