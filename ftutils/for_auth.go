package ftutils

import (
	"crypto/md5"
	"encoding/hex"
	"io"
)

func EncodePassword(password string) string {
	h := md5.New()
	io.WriteString(h, password)
	encpass := hex.EncodeToString(h.Sum(nil))
	return encpass
}
