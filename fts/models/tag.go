package ftsmodels

import (
	"ftera/fts/models/tablenames"

	"github.com/imdario/mergo"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Tag struct {
	Id          bson.ObjectId `bson:"_id,omitempty"`
	Name        string
	Explanation string
	TypeNum     int
}

func AddTag(db *mgo.Database, name string, typ int) {
	c := db.C(tablenames.Tag)
	n, _ := c.Find(bson.M{"name": name, "typenum": typ}).Count()
	if n == 0 {
		tag := new(Tag)
		tag.Id = bson.NewObjectId()
		tag.Name = name
		tag.TypeNum = typ
		c.Insert(tag)
	}
}

func PageTagsByType(db *mgo.Database, typ, page, size int) ([]Tag, error) {
	c := db.C(tablenames.Tag)
	skip := size * page
	tags := []Tag{}
	err := c.Find(bson.M{"typenum": typ}).Skip(skip).Limit(size).All(&tags)
	return tags, err
}

func SetTag(db *mgo.Database, tag Tag) {
	c := db.C(tablenames.Tag)
	n, _ := c.Find(bson.M{"name": tag.Name, "typenum": tag.TypeNum}).Count()
	if n == 0 {
		tag.Id = bson.NewObjectId()
		c.Insert(tag)
	}
}

func FindTag(db *mgo.Database, name string) (*Tag, error) {
	c := db.C(tablenames.Tag)
	tag := new(Tag)
	err := c.Find(bson.M{"name": bson.M{"$eq": name}}).One(&tag)
	return tag, err
}

func SearchTags(db *mgo.Database, search string, typ *int) ([]Tag, error) {
	c := db.C(tablenames.Tag)
	var tags = []Tag{}
	if search[0] == '+' || search[0] == '-' {
		search = "\\" + search
	}
	query := bson.M{"name": bson.M{"$regex": search}}
	if typ != nil {
		mergo.Merge(&query, bson.M{"typenum": *typ})
	}
	err := c.Find(query).Limit(10).All(&tags)
	return tags, err
}

func IsTagNameExists(db *mgo.Database, tag string) bool {
	c := db.C(tablenames.Tag)
	n, _ := c.Find(bson.M{"name": tag}).Count()
	return (n > 0)
}

func CleanTags(db *mgo.Database, tags []string) []string {
	ntags := []string{}
	for _, v := range tags {
		if IsTagNameExists(db, v) {
			ntags = append(ntags, v)
		}
	}
	return ntags
}
