package ftsmodels

import (
	"ftera/fts/models/tablenames"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var TypeReportList = []string{
	"Racist",
	"Abusive",
	"Sexist",
	"Unapropriate Content",
	"Scammer",
}

type Report struct {
	Id          bson.ObjectId `bson:"_id,omitempty"`
	UserId      bson.ObjectId
	ForUserId   bson.ObjectId
	Description string
	Type        int
}

func (u *User) ReportUser(db *mgo.Database, user_id bson.ObjectId,
	typ string, dscr string) error {
	var rep = new(Report)
	rep.Id = bson.NewObjectId()
	rep.UserId = u.Id
	rep.ForUserId = user_id
	rep.Description = dscr
	rep.Type = GetIndex(TypeReportList, typ)
	c := db.C(tablenames.Report)
	return c.Insert(&rep)
}
