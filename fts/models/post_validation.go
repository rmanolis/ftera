package ftsmodels

import (
	"ftera/fts/models/tablenames"
	"log"

	"errors"
	"strconv"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type PostValidation struct{}

var ValidatePost = PostValidation{}

func (vp PostValidation) checkTime(db *mgo.Database, oid bson.ObjectId, field string) error {
	post := new(Post)
	c := db.C(tablenames.Post)
	err := c.Find(bson.M{field: oid}).Sort("-date").One(&post)
	if err != nil {
		return nil
	}
	log.Println(time.Since(post.Date).Minutes())
	if time.Since(post.Date).Minutes() < 20 {
		return errors.New("wait " + strconv.Itoa(int(20-time.Since(post.Date).Minutes())) + " minutes to post again ")
	}
	return nil

}

func (vp PostValidation) CheckTimeForUser(db *mgo.Database, user *User) error {
	return vp.checkTime(db, user.Id, "userid")
}

func (vp PostValidation) CheckTimeForBusiness(db *mgo.Database, business *Business) error {
	return vp.checkTime(db, business.Id, "businessid")
}

func (vp PostValidation) CheckTimeForEvent(db *mgo.Database, event *Event) error {
	return vp.checkTime(db, event.Id, "eventid")
}

func (vp PostValidation) CheckTextLen(text string) error {
	if len(text) > 150 {
		return errors.New("text can not be over 150 characters")
	}
	return nil
}
