package ftsmodels

import (
	"errors"
)

type MessageValidation struct{}

var ValidateMessage = MessageValidation{}

func (mv MessageValidation) StopSpammer(mr *Messenger, sender *User) error {
	for _, v := range mr.Users {
		if v.UserId != sender.Id {
			if v.UnreadMessages >= 2 && v.IsMessaged == false {
				return errors.New("You can not send until the other person send a message")
			}
		}
	}

	return nil
}
