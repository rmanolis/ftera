package ftsmodels

import (
	"errors"
	"ftera/fts/models/tablenames"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Event struct {
	Id          bson.ObjectId `bson:"_id,omitempty"`
	UserId      bson.ObjectId
	PhotoId     bson.ObjectId `bson:",omitempty"`
	Name        string
	StartDate   time.Time
	Geolocation []float64
	Info        BusinessInfo
	TypeLists   TypeLists
	IsPublic    bool
}

func (b *Event) Validate() error {
	if len(b.Name) == 0 {
		return errors.New("the name is empty")
	}
	return nil
}

func (u *User) AddEvent(db *mgo.Database, e *Event) error {
	c := db.C(tablenames.Event)
	e.Id = bson.NewObjectId()
	e.UserId = u.Id
	return c.Insert(e)
}

func (u *User) Event(db *mgo.Database, event_id bson.ObjectId) (*Event, error) {
	c := db.C(tablenames.Event)
	event := new(Event)
	err := c.Find(bson.M{"_id": event_id, "userid": u.Id}).One(&event)
	return event, err
}

func (u *User) Events(db *mgo.Database) ([]Event, error) {
	c := db.C(tablenames.Event)
	var events = []Event{}
	err := c.Find(bson.M{"userid": u.Id}).All(&events)
	return events, err
}

func GetEvent(db *mgo.Database, event_id bson.ObjectId) (*Event, error) {
	c := db.C(tablenames.Event)
	event := new(Event)
	err := c.Find(bson.M{"_id": event_id}).One(&event)
	return event, err
}

func (e *Event) Update(db *mgo.Database) error {
	c := db.C(tablenames.Event)
	return c.UpdateId(e.Id, e)
}

func (e *Event) Delete(db *mgo.Database) error {
	c := db.C(tablenames.Event)
	attentions, err := e.Attentions(db)
	if err == nil {
		for _, v := range attentions {
			v.Delete(db)
		}
	}

	posts, err := e.AllPosts(db)
	if err == nil {
		for _, v := range posts {
			v.Delete(db)
		}
	}

	return c.Remove(e)
}

func (e *Event) Photo(db *mgo.Database) (*Photo, error) {
	photo := new(Photo)
	c := db.C(tablenames.Photo)
	err := c.FindId(e.PhotoId).One(&photo)
	if err != nil {
		return nil, err
	}
	return photo, nil
}
