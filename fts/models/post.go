package ftsmodels

import (
	"ftera/fts/models/enums"
	"ftera/fts/models/tablenames"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Post struct {
	Id           bson.ObjectId `bson:"_id,omitempty"`
	OwnerId      bson.ObjectId
	OwnerPhotoId bson.ObjectId `bson:",omitempty"`
	OwnerSex     string        `bson:",omitempty"`
	UserId       bson.ObjectId `bson:",omitempty"`
	BusinessId   bson.ObjectId `bson:",omitempty"`
	EventId      bson.ObjectId `bson:",omitempty"`
	Name         string
	Text         string
	PhotoId      bson.ObjectId `bson:",omitempty"`
	Geolocation  []float64
	Date         time.Time
}

func (u *User) AddPost(db *mgo.Database, p *Post) error {
	c := db.C(tablenames.Post)
	p.Id = bson.NewObjectId()
	p.UserId = u.Id
	p.OwnerId = u.Id
	p.OwnerPhotoId = u.Info.PhotoId
	p.OwnerSex = enums.SexList[u.Info.Sex]
	p.Name = u.Name
	p.Date = time.Now()
	p.Geolocation = u.Geolocation
	return c.Insert(p)
}

func (u *User) Posts(db *mgo.Database, page int, size int) ([]Post, error) {
	c := db.C(tablenames.Post)
	var posts = []Post{}
	skip := size * page
	err := c.Find(bson.M{"userid": u.Id}).Skip(skip).Limit(size).Sort("-date").All(&posts)
	return posts, err
}

func allPosts(db *mgo.Database, field string, id bson.ObjectId) ([]Post, error) {
	c := db.C(tablenames.Post)
	var posts = []Post{}
	err := c.Find(bson.M{field: id}).All(&posts)
	return posts, err

}
func (u *User) AllPosts(db *mgo.Database) ([]Post, error) {
	return allPosts(db, "userid", u.Id)
}

func (b *Business) AllPosts(db *mgo.Database) ([]Post, error) {
	return allPosts(db, "businessid", b.Id)
}

func (e *Event) AllPosts(db *mgo.Database) ([]Post, error) {
	return allPosts(db, "eventid", e.Id)
}

func (u *User) NearPosts(db *mgo.Database, page int, size int) ([]Post, error) {
	c := db.C(tablenames.Post)
	c.EnsureIndex(GEO_INDEX)
	pref := u.Preferences
	query_geo := bson.M{
		"$near": bson.M{
			"$geometry":    bson.M{"type": "Point", "coordinates": u.Geolocation},
			"$maxDistance": pref.Kilometer * 1000}}
	skip := size * page

	var posts = []Post{}
	err := c.Find(bson.M{"geolocation": query_geo}).Skip(skip).Limit(size).Sort("-date").All(&posts)
	return posts, err
}

func (p *Post) SessionIdFromNearUsers(db *mgo.Database, km int) ([]string, error) {
	c := db.C(tablenames.User)
	c.EnsureIndex(GEO_INDEX)
	query_geo := bson.M{
		"$near": bson.M{
			"$geometry":    bson.M{"type": "Point", "coordinates": p.Geolocation},
			"$maxDistance": km * 1000}}
	var users = []User{}
	err := c.Find(bson.M{"sessionids": bson.M{"$not": bson.M{"$size": 0}},
		"geolocation": query_geo}).All(&users)
	session_ids := []string{}
	for _, v := range users {
		session_ids = append(session_ids, v.SessionIds...)
	}
	return session_ids, err
}

func (b *Business) AddPost(db *mgo.Database, p *Post) error {
	c := db.C(tablenames.Post)
	p.Id = bson.NewObjectId()
	p.BusinessId = b.Id
	p.OwnerId = b.UserId
	p.OwnerPhotoId = b.PhotoId
	p.Name = b.Name
	p.Date = time.Now()
	p.Geolocation = b.Geolocation
	return c.Insert(p)
}

func (b *Business) Posts(db *mgo.Database, page int, size int) ([]Post, error) {
	c := db.C(tablenames.Post)
	var posts = []Post{}
	skip := size * page
	err := c.Find(bson.M{"businessid": b.Id}).Skip(skip).Limit(size).Sort("-date").All(&posts)
	return posts, err
}

func (e *Event) AddPost(db *mgo.Database, p *Post) error {
	c := db.C(tablenames.Post)
	p.Id = bson.NewObjectId()
	p.EventId = e.Id
	p.OwnerId = e.UserId
	p.OwnerPhotoId = e.PhotoId
	p.Name = e.Name
	p.Date = time.Now()
	p.Geolocation = e.Geolocation
	return c.Insert(p)
}

func (e *Event) Posts(db *mgo.Database, page int, size int) ([]Post, error) {
	c := db.C(tablenames.Post)
	var posts = []Post{}
	skip := size * page
	err := c.Find(bson.M{"eventid": e.Id}).Skip(skip).Limit(size).Sort("-date").All(&posts)
	return posts, err
}

func GetPost(db *mgo.Database, id bson.ObjectId) (*Post, error) {
	c := db.C(tablenames.Post)
	p := new(Post)
	err := c.FindId(id).One(&p)
	return p, err
}

func (p *Post) Delete(db *mgo.Database) error {
	c := db.C(tablenames.Post)
	if p.PhotoId.Valid() {
		cp := db.C(tablenames.Photo)
		photo := new(Photo)
		err := cp.FindId(p.PhotoId).One(&photo)
		if err == nil {
			photo.Delete(db)
		}
	}
	return c.Remove(bson.M{"_id": p.Id})
}

func (p *Post) Update(db *mgo.Database) error {
	c := db.C(tablenames.Post)
	return c.UpdateId(p.Id, p)
}
