package ftsmodels

import (
	"ftera/fts/models/tablenames"
	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Like struct {
	Id      bson.ObjectId `bson:"_id,omitempty"`
	UserId  bson.ObjectId
	PhotoId bson.ObjectId `bson:",omitempty"`
	PostId  bson.ObjectId
}

func (u *User) AddLike(db *mgo.Database, postid bson.ObjectId) (*Like, error) {
	c := db.C(tablenames.Like)
	like := new(Like)
	like.Id = bson.NewObjectId()
	like.PhotoId = u.Info.PhotoId
	like.UserId = u.Id
	like.PostId = postid
	return like, c.Insert(like)
}

func (u *User) IsLikeExists(db *mgo.Database, postid bson.ObjectId) bool {
	c := db.C(tablenames.Like)
	n, _ := c.Find(bson.M{"userid": u.Id, "postid": postid}).Count()
	return (n > 0)
}

func (u *User) Likes(db *mgo.Database) ([]Like, error) {
	c := db.C(tablenames.Like)
	likes := []Like{}
	err := c.Find(bson.M{"userid": u.Id}).All(&likes)
	return likes, err
}

func (l *Like) Remove(db *mgo.Database) error {
	c := db.C(tablenames.Like)
	return c.RemoveId(l.Id)
}

func (p *Post) Likes(db *mgo.Database) ([]Like, error) {
	c := db.C(tablenames.Like)
	likes := []Like{}
	err := c.Find(bson.M{"postid": p.Id}).All(&likes)
	return likes, err
}

func (p *Post) Like(db *mgo.Database, userid bson.ObjectId) (*Like, error) {
	c := db.C(tablenames.Like)
	like := new(Like)
	err := c.Find(bson.M{"userid": userid, "postid": p.Id}).One(&like)
	return like, err
}

func (p *Post) CountLikes(db *mgo.Database) int {
	c := db.C(tablenames.Like)
	n, err := c.Find(bson.M{"postid": p.Id}).Count()
	if err != nil {
		log.Println(err.Error())
	}
	return n
}
