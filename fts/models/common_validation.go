package ftsmodels

import (
	"ftera/fts/utils/server_errors"
	"strings"
)

type CommonValidation struct{}

var ValidateCommon = CommonValidation{}

func (vc CommonValidation) Name(name string) error {
	if len(name) < 3 {
		return server_errors.ErrNameLen
	}
	return nil

}

func (vc CommonValidation) Email(email string) error {
	if !strings.Contains(email, "@") {
		return server_errors.ErrNotEmail
	}
	return nil
}

func (vc CommonValidation) Password(password string) error {
	if len(password) < 6 {
		return server_errors.ErrPasswordLen
	}
	return nil
}
