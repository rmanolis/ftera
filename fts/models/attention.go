package ftsmodels

import (
	"ftera/fts/models/tablenames"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Attention struct {
	Id      bson.ObjectId `bson:"_id,omitempty"`
	EventId bson.ObjectId
	UserId  bson.ObjectId
	PhotoId bson.ObjectId `bson:",omitempty"`
}

func (u *User) IsAttentionExists(db *mgo.Database, event_id bson.ObjectId) bool {
	c := db.C(tablenames.Attention)
	n, _ := c.Find(bson.M{"userid": u.Id, "eventid": event_id}).Count()
	return (n > 0)
}

func (u *User) AddAttention(db *mgo.Database, event_id bson.ObjectId) (*Attention, error) {
	c := db.C(tablenames.Attention)
	at := new(Attention)
	at.Id = bson.NewObjectId()
	at.EventId = event_id
	at.UserId = u.Id
	at.PhotoId = u.Info.PhotoId
	err := c.Insert(&at)
	return at, err
}

func (e *Event) Attentions(db *mgo.Database) ([]Attention, error) {
	c := db.C(tablenames.Attention)
	var at_ls = []Attention{}
	err := c.Find(bson.M{"eventid": e.Id}).All(&at_ls)
	return at_ls, err
}

func (e *Event) Attention(db *mgo.Database,
	userid bson.ObjectId) (*Attention, error) {
	c := db.C(tablenames.Attention)
	var at = new(Attention)
	err := c.Find(bson.M{"eventid": e.Id, "userid": userid}).One(&at)
	return at, err
}

func (e *Event) SessionIdFromAttentions(db *mgo.Database) []string {
	attentions, _ := e.Attentions(db)
	session_ids := []string{}
	for _, v := range attentions {
		u, err := GetUserById(db, v.UserId.Hex())
		if err == nil {
			session_ids = append(session_ids, u.SessionIds...)
		}
	}
	return session_ids
}

func (e *Event) CountAttentions(db *mgo.Database) (int, error) {
	c := db.C(tablenames.Attention)
	n, err := c.Find(bson.M{"eventid": e.Id}).Count()
	return n, err
}

func (at *Attention) Delete(db *mgo.Database) error {
	c := db.C(tablenames.Attention)
	return c.Remove(at)
}

func (u *User) Attentions(db *mgo.Database) ([]Attention, error) {
	c := db.C(tablenames.Attention)
	attentions := []Attention{}
	err := c.Find(bson.M{"userid": u.Id}).All(&attentions)
	return attentions, err
}
