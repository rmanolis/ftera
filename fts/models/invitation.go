package ftsmodels

import (
	"ftera/fts/models/tablenames"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Invitation struct {
	Id         bson.ObjectId `bson:"_id,omitempty"`
	FromUserId bson.ObjectId
	ToUserId   bson.ObjectId
	Date       time.Time
	IsAccepted bool
	EventId    *bson.ObjectId
	BusinessId *bson.ObjectId
}

func GetInvitation(db *mgo.Database, inv_id bson.ObjectId) (*Invitation, error) {
	c := db.C(tablenames.Invitation)
	inv := new(Invitation)
	err := c.Find(bson.M{"_id": inv_id}).One(&inv)
	return inv, err
}

func (u *User) ToInvitations(db *mgo.Database) ([]Invitation, error) {
	c := db.C(tablenames.Invitation)
	var inv_ls []Invitation
	err := c.Find(bson.M{"touserid": u.Id, "date": bson.M{"$gte": time.Now()}}).All(&inv_ls)
	return inv_ls, err
}

func (u *User) FromInvitations(db *mgo.Database) ([]Invitation, error) {
	c := db.C(tablenames.Invitation)
	var inv_ls []Invitation
	err := c.Find(bson.M{"fromuserid": u.Id, "date": bson.M{"$gte": time.Now()}}).All(&inv_ls)
	return inv_ls, err
}

func (inv *Invitation) Insert(db *mgo.Database, from_user, to_user bson.ObjectId) error {
	c := db.C(tablenames.Invitation)
	inv.Id = bson.NewObjectId()
	inv.FromUserId = from_user
	inv.ToUserId = to_user
	return c.Insert(inv)
}

func (inv *Invitation) Update(db *mgo.Database) error {
	c := db.C(tablenames.Invitation)
	return c.UpdateId(inv.Id, inv)
}

func (inv *Invitation) Name(db *mgo.Database) string {
	name := ""
	if inv.BusinessId != nil {
		b, _ := GetBusiness(db, *inv.BusinessId)
		name = b.Name
	} else if inv.EventId != nil {
		e, _ := GetEvent(db, *inv.EventId)
		name = e.Name
	}
	return name
}

func (inv *Invitation) Delete(db *mgo.Database) error {
	c := db.C(tablenames.Invitation)
	return c.Remove(inv)
}
