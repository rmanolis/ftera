package ftsmodels

import (
	"errors"
	"ftera/fts/models/enums"
	"ftera/fts/models/tablenames"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

var (
	ErrSelectedNotFound = errors.New("selected not found")
)

type UserInfo struct {
	PhotoId   bson.ObjectId `json:",omitempty" bson:",omitempty"`
	BirthDate int           `json:",omitempty" bson:"birthdate,omitempty"`
	Sex       int
	Bio       string
	Location  string
}

func (u *User) NewInfo() {
	u.Info = UserInfo{Sex: 0, Bio: "", BirthDate: 1988, Location: "Athens, Greece"}
}

func (u *User) Photo(db *mgo.Database) (*Photo, error) {
	photo := new(Photo)
	c := db.C(tablenames.Photo)
	err := c.FindId(u.Info.PhotoId).One(&photo)
	if err != nil {
		return nil, err
	}
	return photo, nil
}

func (u *User) SetBio(bio string) {
	u.Info.Bio = bio
}

func (u *User) SetPhoto(photo_id bson.ObjectId) {
	u.Info.PhotoId = photo_id
}

func GetIndex(list []string, name string) int {
	for i := range list {
		if name == list[i] {
			return i
		}
	}
	return 0
}

func (u *User) SetSex(name string) {
	i := GetIndex(enums.SexList, name)
	u.Info.Sex = i
}

func (u *User) Sex() string {
	return enums.SexList[u.Info.Sex]
}

func (u *User) SetBirthDate(birth_date int) error {
	if (time.Now().Year() - birth_date) < 18 {
		return errors.New("too young")
	}
	if (time.Now().Year() - birth_date) > 90 {
		return errors.New("too old")
	}

	u.Info.BirthDate = birth_date
	return nil
}
