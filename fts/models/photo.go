package ftsmodels

import (
	"fmt"
	"ftera/fts/models/tablenames"
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Photo struct {
	Id        bson.ObjectId `bson:"_id,omitempty"`
	OwnerId   bson.ObjectId
	Name      string
	Filename  string
	Directory string
}

func (u *User) NewPhoto(db *mgo.Database, file []byte) (*Photo, error) {
	p := new(Photo)
	p.Id = bson.NewObjectId()
	p.Filename = p.Id.Hex()
	p.OwnerId = u.Id

	c := db.C(tablenames.Photo)
	err := c.Insert(&p)
	if err != nil {
		return nil, err
	}

	path := fmt.Sprintf("images/%s", p.Filename)
	err = ioutil.WriteFile(path, file, os.ModePerm)
	if err != nil {
		return nil, err
	}

	return p, nil
}

func (p *Photo) Delete(db *mgo.Database) error {
	path := fmt.Sprintf("images/%s", p.Filename)
	err := os.Remove(path)
	if err != nil {
		log.Println(err.Error())
	}
	c := db.C(tablenames.Photo)
	return c.RemoveId(p.Id)
}
