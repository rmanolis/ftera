package ftsmodels

import (
	"ftera/fts/models/tablenames"
	"log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type MessengerUser struct {
	UserId         bson.ObjectId
	IsMessaged     bool
	IsIgnoring     bool
	UnreadMessages int
}

type Messenger struct {
	Id          bson.ObjectId `bson:"_id,omitempty"`
	Users       []MessengerUser
	LastUpdated time.Time
	IsRead      bool
}

type Message struct {
	Id          bson.ObjectId `bson:"_id,omitempty"`
	MessengerId bson.ObjectId
	Text        string
	UserId      bson.ObjectId
	Date        time.Time
}

func (u *User) Messengers(db *mgo.Database) ([]Messenger, error) {
	mrt := db.C(tablenames.Messenger)
	var messengers = []Messenger{}
	err := mrt.Find(bson.M{"users.userid": u.Id}).Sort("-lastupdated").All(&messengers)
	return messengers, err
}

func (u *User) Messenger(db *mgo.Database, id bson.ObjectId) (*Messenger, error) {
	mrt := db.C(tablenames.Messenger)
	var messenger Messenger
	err := mrt.Find(bson.M{"_id": id, "users.userid": u.Id}).One(&messenger)
	if err != nil {
		return nil, err
	}
	return &messenger, nil
}

func (u *User) IgnoreMessenger(db *mgo.Database, id bson.ObjectId) error {
	msn, err := u.Messenger(db, id)
	if err != nil {
		return err
	}
	mu_ls := []MessengerUser{}
	for _, v := range msn.Users {
		if v.UserId == u.Id {
			v.IsIgnoring = true
		}
		mu_ls = append(mu_ls, v)
	}
	msn.Users = mu_ls
	mrt := db.C(tablenames.Messenger)
	return mrt.UpdateId(msn.Id, msn)
}

func (u *User) OtherFromChat(db *mgo.Database, msn_id bson.ObjectId) (*User, error) {
	msn, err := u.Messenger(db, msn_id)
	if err != nil {
		return nil, err
	}
	other := new(User)
	for _, v := range msn.Users {
		if u.Id != v.UserId {
			err = other.Get(db, v.UserId.Hex())
			if err != nil {
				return nil, err
			}
		}
	}
	return other, nil
}

func (u *User) ReadChat(db *mgo.Database, msn_id bson.ObjectId) error {
	msn, err := u.Messenger(db, msn_id)
	if err != nil {
		return err
	}

	users := msn.Users
	for i := range users {
		if u.Id == users[i].UserId {
			users[i].UnreadMessages = 0
		}
	}
	msn.Users = users
	c := db.C(tablenames.Messenger)
	return c.UpdateId(msn.Id, msn)
}

func (u *User) CountNewMessage(db *mgo.Database, msn_id bson.ObjectId) error {
	msn, err := u.Messenger(db, msn_id)
	if err != nil {
		return err
	}

	users := msn.Users
	for i := range users {
		if u.Id == users[i].UserId {
			users[i].IsMessaged = true
		} else {
			users[i].UnreadMessages += 1
		}
	}
	msn.Users = users

	return msn.UpdateWithDate(db)

}

func GetMessengerByUsers(db *mgo.Database, user1 bson.ObjectId, user2 bson.ObjectId) (*Messenger, error) {
	mrt := db.C(tablenames.Messenger)
	msgr := new(Messenger)
	err := mrt.Find(bson.M{"$and": []bson.M{bson.M{"users.userid": user1},
		bson.M{"users.userid": user2}}}).One(&msgr)
	return msgr, err
}

func (u *User) SetMessenger(db *mgo.Database, user_id bson.ObjectId) (*Messenger, error) {
	mrt := db.C(tablenames.Messenger)
	msgr, err := GetMessengerByUsers(db, u.Id, user_id)
	new_users := []MessengerUser{}
	change := false
	for _, v := range msgr.Users {
		if v.UserId == u.Id {
			if v.IsIgnoring {
				v.IsIgnoring = false
				change = true
			}
		}
		new_users = append(new_users, v)
	}
	if change {
		msgr.Users = new_users
		err = msgr.UpdateWithDate(db)
		if err != nil {
			log.Println(err.Error())
		}
	}
	if err == nil {
		return msgr, nil
	}
	msgr.Id = bson.NewObjectId()

	m_user := MessengerUser{
		UserId:     u.Id,
		IsMessaged: false,
	}
	m_other := MessengerUser{
		UserId:     user_id,
		IsMessaged: false,
	}
	msgr.Users = []MessengerUser{m_user, m_other}
	msgr.LastUpdated = time.Now()
	err = mrt.Insert(&msgr)
	return msgr, err
}

func (mr *Messenger) Messages(db *mgo.Database, size int, page int) ([]Message, error) {
	mst := db.C(tablenames.Message)
	var messages = []Message{}
	skip := size * (page - 1)
	err := mst.Find(bson.M{"messengerid": mr.Id}).
		Sort("-_id").Skip(skip).Limit(size).All(&messages)
	return messages, err
}

func (mr *Messenger) AllMessages(db *mgo.Database) ([]Message, error) {
	mst := db.C(tablenames.Message)
	var messages = []Message{}
	err := mst.Find(bson.M{"messengerid": mr.Id}).All(&messages)
	return messages, err
}

func (mr *Messenger) CountMessages(db *mgo.Database) int {
	mst := db.C(tablenames.Message)
	n, _ := mst.Find(bson.M{"messengerid": mr.Id}).Count()
	return n
}

func (user *User) AddMessage(db *mgo.Database, mr *Messenger, text string) (*Message, error) {
	mst := db.C(tablenames.Message)
	msg := new(Message)
	msg.Id = bson.NewObjectId()
	msg.MessengerId = mr.Id
	msg.UserId = user.Id
	msg.Text = text
	msg.Date = time.Now()
	return msg, mst.Insert(&msg)
}

func (mr *Messenger) UpdateWithDate(db *mgo.Database) error {
	mrt := db.C(tablenames.Messenger)
	mr.LastUpdated = time.Now()
	return mrt.UpdateId(mr.Id, mr)
}

func (mr *Messenger) Delete(db *mgo.Database) error {
	mrt := db.C(tablenames.Messenger)
	msgs, err := mr.AllMessages(db)
	if err == nil {
		for _, v := range msgs {
			v.Delete(db)
		}
	}
	return mrt.Remove(mr)
}

func (msg *Message) Delete(db *mgo.Database) error {
	mst := db.C(tablenames.Message)
	return mst.Remove(msg)
}
