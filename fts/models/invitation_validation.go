package ftsmodels

import (
	"ftera/fts/utils/server_errors"

	"gopkg.in/mgo.v2"
)

type InvitationValidation struct{}

var ValidateInvitation = InvitationValidation{}

func (vi InvitationValidation) CheckMessenger(db *mgo.Database, user *User,
	other *User) error {
	mr, err := GetMessengerByUsers(db, user.Id, other.Id)
	if err != nil {
		return server_errors.ErrNoInvitationWithoutChat
	}
	for _, v := range mr.Users {
		if other.Id == v.UserId && v.IsMessaged == false {
			return server_errors.ErrNoInvitationWithoutChat
		}
	}
	return nil

}
