package ftsmodels

import (
	"errors"
	"ftera/fts/models/tablenames"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type TypeLists struct {
	MusicTypes    []string
	StuffTypes    []string
	BusinessTypes []string
	EventTypes    []string
	ProductTypes  []string
	Products      []string
	Comforts      []string
}

type BusinessDuration struct {
	StartTime time.Time
	EndTime   time.Time
}

type BusinessInfo struct {
	Address     string
	Telephone   string
	Website     string
	Email       string
	Description string
}

type Price struct {
	From int
	To   int
}

type Comfort struct {
	CanReservate       bool
	CanDeliver         bool
	CanTakeOut         bool
	AcceptsCreditCards bool
	HasParking         bool
	CanSmoke           bool
	HasOutdoorSeating  bool
	IsDogsAllowed      bool
	HasWaiterService   bool
	IsCater            bool
}

type Business struct {
	Id          bson.ObjectId `bson:"_id,omitempty"`
	UserId      bson.ObjectId
	PhotoId     bson.ObjectId `bson:",omitempty"`
	Name        string
	OpenDays    []string
	Duration    BusinessDuration
	Geolocation []float64
	TypeLists   TypeLists
	Info        BusinessInfo
	Price       Price
	Stars       float64
	IsPublic    bool
}

func (u *User) AddBusiness(db *mgo.Database, business *Business) error {
	c := db.C(tablenames.Business)
	business.Id = bson.NewObjectId()
	business.UserId = u.Id
	return c.Insert(business)
}

func (b *Business) Validate() error {
	if len(b.Name) == 0 {
		return errors.New("the name is empty")
	}
	return nil
}

func (u *User) Business(db *mgo.Database, bs_id bson.ObjectId) (*Business, error) {
	c := db.C(tablenames.Business)
	b := new(Business)
	err := c.Find(bson.M{"_id": bs_id, "userid": u.Id}).One(&b)
	return b, err
}

func GetBusiness(db *mgo.Database, bs_id bson.ObjectId) (*Business, error) {
	c := db.C(tablenames.Business)
	b := new(Business)
	err := c.Find(bson.M{"_id": bs_id}).One(&b)
	return b, err
}

func (u *User) Businesses(db *mgo.Database) ([]Business, error) {
	c := db.C(tablenames.Business)
	var b = []Business{}
	err := c.Find(bson.M{"userid": u.Id}).All(&b)
	return b, err
}

func (b *Business) Update(db *mgo.Database) error {
	c := db.C(tablenames.Business)
	return c.UpdateId(b.Id, b)
}

func (b *Business) Delete(db *mgo.Database) error {
	c := db.C(tablenames.Business)
	posts, err := b.AllPosts(db)
	if err == nil {
		for _, v := range posts {
			v.Delete(db)
		}
	}

	return c.Remove(b)
}

func (b *Business) Photo(db *mgo.Database) (*Photo, error) {
	photo := new(Photo)
	c := db.C(tablenames.Photo)
	err := c.FindId(b.PhotoId).One(&photo)
	if err != nil {
		return nil, err
	}
	return photo, nil
}
