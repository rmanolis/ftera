package tablenames

const (
	User       = "user"
	Block      = "block"
	Business   = "business"
	Event      = "event"
	Invitation = "invitation"
	Like       = "like"
	Messenger  = "messenger"
	Message    = "message"
	Photo      = "photo"
	Post       = "post"
	Report     = "report"
	Tag        = "tag"
	Attention  = "attention"
)
