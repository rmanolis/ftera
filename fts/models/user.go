package ftsmodels

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"ftera/fts/models/tablenames"
	"ftera/fts/utils"
	"ftera/ftutils"
	"ftera/ftws/pool"
	"io"
	"math/rand"
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	Id             bson.ObjectId `bson:"_id,omitempty"`
	Name           string
	Email          string
	Password       string
	IsNew          bool
	IsBusiness     bool
	IsAdmin        bool
	IsPublic       bool
	IsOnline       bool
	IsValidated    bool
	Language       string
	ValidationCode string
	SessionIds     []string
	Geolocation    []float64
	Info           UserInfo
	Preferences    UserPreferences
}

var GEO_INDEX = mgo.Index{
	Key: []string{"$2dsphere:geolocation"},
}

func GetUserById(db *mgo.Database, id string) (*User, error) {
	user := new(User)
	err := user.Get(db, id)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func LoadUser(r *http.Request) (*User, error) {
	user := new(User)
	err := user.Load(r)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func CleanAllSessionIds(dt *ftutils.DataStore) {
	ses := dt.Session.Clone()
	db := ses.DB(dt.DBName)
	defer ses.Close()
	c := db.C(tablenames.User)
	users := []User{}
	c.Find(bson.M{}).All(&users)
	for _, v := range users {
		v.SessionIds = []string{}
		v.Update(db)
	}
}

func NewUser(db *mgo.Database, name string, email string, password string) (*User, error) {
	u := new(User)
	u.Name = name
	u.Email = email
	u.IsAdmin = false
	u.IsPublic = true
	u.IsBusiness = false
	u.IsNew = true
	u.IsOnline = false
	u.Id = bson.NewObjectId()
	u.Geolocation = []float64{23.729359899999963, 37.983917}
	u.Info.Location = "Athens, Greece"
	h := md5.New()
	io.WriteString(h, password)
	u.Password = hex.EncodeToString(h.Sum(nil))
	c := db.C(tablenames.User)
	err := c.Insert(&u)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func FindUserByEmail(db *mgo.Database, email string) (*User, error) {
	c := db.C(tablenames.User)
	u := new(User)
	err := c.Find(bson.M{
		"email": email,
	}).One(&u)
	return u, err
}

func FindUserBySession(db *mgo.Database, session string) (*User, error) {
	c := db.C(tablenames.User)
	u := new(User)
	err := c.Find(bson.M{"sessionids": bson.M{"$elemMatch": bson.M{"$eq": session}}}).One(&u)
	return u, err
}

func (u *User) Update(db *mgo.Database) error {
	c := db.C(tablenames.User)

	return c.UpdateId(u.Id, u)
}

func (u *User) Get(db *mgo.Database, id string) error {

	if bson.IsObjectIdHex(id) {
		return db.C(tablenames.User).FindId(bson.ObjectIdHex(id)).One(&u)
	} else {
		return errors.New("user: it is not an ID")
	}
}

func (u *User) Load(r *http.Request) error {
	db := ftsutils.GetDB(r)
	if db == nil {
		return errors.New("user: database is not connected")
	}
	id, err := ftsutils.UserId(r)
	if err != nil {
		return err
	}

	return u.Get(db, id)
}

func (u *User) Authenticate(db *mgo.Database, email string, password string) error {
	h := md5.New()
	io.WriteString(h, password)
	hex_password := hex.EncodeToString(h.Sum(nil))
	err := db.C(tablenames.User).Find(bson.M{
		"password": hex_password,
		"email":    email,
	}).One(&u)
	return err
}

func (u *User) SetPassword(db *mgo.Database, old_pass string, new_pass string) error {
	old_h := md5.New()
	io.WriteString(old_h, old_pass)
	old_hex_password := hex.EncodeToString(old_h.Sum(nil))
	num, _ := db.C(tablenames.User).Find(bson.M{
		"password": old_hex_password,
		"email":    u.Email,
	}).Count()
	if num == 0 {
		return errors.New("wrong old password")
	}

	err := ValidateCommon.Password(new_pass)
	if err != nil {
		return err
	}
	new_h := md5.New()
	io.WriteString(new_h, new_pass)
	new_hex_password := hex.EncodeToString(new_h.Sum(nil))
	u.Password = new_hex_password
	return nil
}

func (u *User) SetEmail(db *mgo.Database, email string) error {
	if u.Email != email {
		err := ValidateUser.Email(db, email)
		if err != nil {
			return err
		}
		u.Email = email
	}
	return nil
}

func (u *User) SetName(name string) error {
	err := ValidateUser.Name(name)
	if err != nil {
		return err
	}
	u.Name = name
	return nil
}

func (u *User) SetLocation(location string, geolocation []float64) {
	u.Info.Location = location
	u.Geolocation = geolocation
}

func (u *User) SetValidation(db *mgo.Database) {
	c := db.C(tablenames.User)
	u.IsValidated = false
	for {
		var code = rand.Intn(1000000000000000)
		n, _ := c.Find(bson.M{"validationcode": strconv.Itoa(code)}).Count()
		if n == 0 {
			u.ValidationCode = strconv.Itoa(code)
			break
		}
	}
}

func (u *User) Delete(db *mgo.Database) error {
	c := db.C(tablenames.User)
	posts, err := u.AllPosts(db)
	if err == nil {
		for _, v := range posts {
			v.Delete(db)
		}
	}
	businesses, err := u.Businesses(db)
	if err == nil {
		for _, v := range businesses {
			v.Delete(db)
		}
	}
	events, err := u.Events(db)
	if err == nil {
		for _, v := range events {
			v.Delete(db)
		}
	}
	from_invs, err := u.FromInvitations(db)
	if err == nil {
		for _, v := range from_invs {
			v.Delete(db)
		}
	}
	to_invs, err := u.ToInvitations(db)
	if err == nil {
		for _, v := range to_invs {
			v.Delete(db)
		}
	}

	msngrs, err := u.Messengers(db)
	if err == nil {
		for _, v := range msngrs {
			v.Delete(db)
		}
	}

	blocks, err := u.Blocks(db)
	if err == nil {
		for _, v := range blocks {
			v.Delete(db)
		}
	}

	blockeds, err := u.BlocksByOthers(db)
	if err == nil {
		for _, v := range blockeds {
			v.Delete(db)
		}
	}
	attentions, err := u.Attentions(db)
	if err == nil {
		for _, v := range attentions {
			v.Delete(db)
		}
	}
	return c.Remove(u)
}

func ValidateUserToken(db *mgo.Database, code string) error {
	c := db.C(tablenames.User)
	u := new(User)

	err := c.Find(bson.M{"validationcode": code}).One(&u)
	if err != nil {
		return err
	}
	u.IsValidated = true
	u.ValidationCode = ""
	return u.Update(db)
}

func (u *User) ToChannel(channel string, m []byte) {
	for _, v := range u.SessionIds {
		ftwspool.ToSession(channel, v, m)
	}
}
