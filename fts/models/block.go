package ftsmodels

import (
	"errors"
	"ftera/fts/models/tablenames"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Block struct {
	Id          bson.ObjectId `bson:"_id,omitempty"`
	UserId      bson.ObjectId
	BlockUserId bson.ObjectId
}

func (u *User) BlockUser(db *mgo.Database, userid bson.ObjectId) error {
	c := db.C(tablenames.Block)
	bl := Block{
		Id:          bson.NewObjectId(),
		UserId:      u.Id,
		BlockUserId: userid,
	}
	if u.IsBlockingUser(db, userid) {
		return errors.New("is already blocked")
	} else {
		return c.Insert(&bl)
	}
}

func (u *User) Blocks(db *mgo.Database) ([]Block, error) {
	c := db.C(tablenames.Block)
	var blocks = []Block{}
	err := c.Find(bson.M{"userid": u.Id}).All(&blocks)
	return blocks, err
}

func (u *User) BlocksByOthers(db *mgo.Database) ([]Block, error) {
	c := db.C(tablenames.Block)
	var blocks = []Block{}
	err := c.Find(bson.M{"blockuserid": u.Id}).All(&blocks)
	return blocks, err

}

func (u *User) IsBlockingUser(db *mgo.Database, blockuserid bson.ObjectId) bool {
	c := db.C(tablenames.Block)
	n, _ := c.Find(bson.M{"userid": u.Id,
		"blockuserid": blockuserid}).Count()
	return (n > 0)
}

func (u *User) IsBlockedByUser(db *mgo.Database, userid bson.ObjectId) bool {
	c := db.C(tablenames.Block)
	n, _ := c.Find(bson.M{"userid": userid,
		"blockuserid": u.Id}).Count()
	return (n > 0)
}

func (u *User) UnblockUser(db *mgo.Database, blockuserid bson.ObjectId) error {
	c := db.C(tablenames.Block)
	var block Block
	err := c.Find(bson.M{"userid": u.Id, "blockuserid": blockuserid}).One(&block)
	if err != nil {
		return err
	}

	err = c.RemoveId(block.Id)
	if err != nil {
		return err
	}
	return nil
}

func (b *Block) Delete(db *mgo.Database) error {
	c := db.C(tablenames.Block)
	return c.Remove(b)
}
