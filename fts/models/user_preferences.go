package ftsmodels

import (
	"errors"
	"ftera/fts/models/enums"
	"gopkg.in/mgo.v2"
)

type UserPreferences struct {
	FromAge int
	ToAge   int
	Sex     int `json:",omitempty"`

	Kilometer    int `json:",omitempty"`
	SearchOption int
}

var PREF_INDEX = mgo.Index{
	Key: []string{"$2dsphere:user"},
}

func (u *User) NewPreferences() {
	u.Preferences = UserPreferences{
		FromAge:      19,
		ToAge:        30,
		Sex:          1,
		Kilometer:    2,
		SearchOption: 0,
	}
}

func (u *User) SetPrefSex(name string) {
	i := GetIndex(enums.SexList, name)
	u.Preferences.Sex = i
}
func (u *User) PrefSex() string {
	return enums.SexList[u.Preferences.Sex]
}

func (u *User) SetPrefSearchOption(name string) {
	i := GetIndex(enums.SearchOptions, name)
	u.Preferences.SearchOption = i
}

func (u *User) PrefSearchOption() string {
	return enums.SearchOptions[u.Preferences.SearchOption]
}

func (u *User) SetPrefAge(from_age int, to_age int) error {
	if from_age < 18 {
		return errors.New("too young")
	}
	if to_age > 100 {
		return errors.New("too old")
	}

	u.Preferences.FromAge = from_age
	u.Preferences.ToAge = to_age
	return nil
}

func (u *User) SetPrefKilometer(km int) error {
	if km < 2 {
		errors.New("the distance is too low")
	} else if km > 20 {
		errors.New("the distance is too high")
	}
	u.Preferences.Kilometer = km
	return nil
}
