package enums

var MbtiList = []string{"None", "INFP",
	"INFJ",
	"INTJ",
	"INTP",
	"ISFJ",
	"ISFP",
	"ISTJ",
	"ISTP",
	"ENFJ",
	"ENFP",
	"ENTJ",
	"ENTP",
	"ESFJ",
	"ESFP",
	"ESTJ",
	"ESTP"}

var HoroscopeList = []string{
	"None",
	"Aries",
	"Taurus",
	"Gemini",
	"Cancer",
	"Leo",
	"Virgo",
	"Libra",
	"Scorpio",
	"Sagittarius",
	"Capricorn",
	"Aquarius",
	"Pisces",
}

var SexList = []string{"Male", "Female", "Transexual"}
