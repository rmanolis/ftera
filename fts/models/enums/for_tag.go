package enums

const (
	MusicTypeTag    = iota
	StuffTypeTag    = iota
	BusinessTypeTag = iota
	EventTypeTag    = iota
	ProductTypeTag  = iota
	ProductTag      = iota
	ComfortTag      = iota
)
