package ftsmodels

import (
	"ftera/fts/models/tablenames"
	"ftera/fts/utils/server_errors"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UserValidation struct{}

var ValidateUser = UserValidation{}

func (vu UserValidation) Name(name string) error {
	err := ValidateCommon.Name(name)
	if err != nil {
		return err
	}
	return nil
}

func (vu UserValidation) Email(db *mgo.Database, email string) error {
	err := ValidateCommon.Email(email)
	if err != nil {
		return err
	}

	n, _ := db.C(tablenames.User).Find(bson.M{"email": email}).Count()
	if n > 0 {
		return server_errors.ErrUserExists
	}
	return nil
}

func (vu UserValidation) RegistrationForm(db *mgo.Database, name, email, password string) []error {
	var err_ls = []error{}
	err := vu.Name(name)
	if err != nil {
		err_ls = append(err_ls, err)
	}
	err = vu.Email(db, email)
	if err != nil {
		err_ls = append(err_ls, err)
	}
	err = ValidateCommon.Password(password)
	if err != nil {
		err_ls = append(err_ls, err)
	}
	return err_ls
}
