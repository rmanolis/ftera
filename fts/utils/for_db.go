package ftsutils

import (
	"ftera/ftutils"
	"net/http"

	"gopkg.in/mgo.v2"
)

func GetDB(r *http.Request) *mgo.Database {
	return ftutils.GetDB(r, "ftera")
}
