package server_errors

import "errors"

var (
	ErrUserExists = errors.New("user exists already")
)
