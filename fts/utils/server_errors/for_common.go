package server_errors

import "errors"

var (
	ErrNotEmail    = errors.New("not an email")
	ErrPasswordLen = errors.New("password can be only over 6 characters")
	ErrNameLen     = errors.New("name can be only over 3 characters")
)
