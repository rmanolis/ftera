package server_errors

import "errors"

var ErrNoInvitationWithoutChat = errors.New("invitation can not be send without a chat first")
