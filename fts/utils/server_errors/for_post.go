package server_errors

import "errors"

var (
	ErrTimePost = errors.New("wait 20 minutes to post again")
)
