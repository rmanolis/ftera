package main

import (
	"ftera/ftconfigs"
	"ftera/fts/ctrls"
	"log"

	"ftera/fts/models"
	"ftera/fts/scripts"
	"ftera/fts/ws/receivers"
	"ftera/ftws/conn"
	"ftera/ftws/pool"
	"net/http"
	"time"

	"github.com/codegangsta/negroni"
	"github.com/goincremental/negroni-sessions"
	"github.com/goincremental/negroni-sessions/cookiestore"
)

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	time.Local = time.FixedZone("UTC", 0)
	go ftwspool.HubRun(ftswreceivers.Routes)
	store := cookiestore.New([]byte("secretkey789"))

	router := ftsctrls.LoadRoutes()
	n := negroni.Classic()
	static := negroni.NewStatic(http.Dir("static"))
	static.Prefix = "/static"
	public := negroni.NewStatic(http.Dir("static/public"))
	public.Prefix = "/public"
	images := negroni.NewStatic(http.Dir("images"))
	images.Prefix = "/images"

	n.Use(static)
	n.Use(public)
	n.Use(images)
	n.Use(sessions.Sessions("global_session_store", store))

	dt := ftconfigs.FteraDB()
	defer dt.Session.Close()
	n.Use(negroni.HandlerFunc(dt.MgoMiddleware))
	ftsscripts.LoadTags(dt)
	//ftsscripts.FixUsers(dt)
	ftsmodels.CleanAllSessionIds(dt)
	router.HandleFunc("/ws", ftwsconn.ServeWs)
	n.UseHandler(router)
	n.Run(":3001")
}
