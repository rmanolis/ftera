package logicbusiness

import (
	"ftera/fts/models"
	"strconv"
	"strings"
	"time"
)

type BusinessDurationBody struct {
	StartTime string
	EndTime   string
}

type BusinessBody struct {
	Name        string
	Duration    BusinessDurationBody
	OpenDays    []string
	Geolocation []float64
	Info        ftsmodels.BusinessInfo
	TypeLists   ftsmodels.TypeLists
	IsPublic    bool
}

func SetupBusiness(b *BusinessBody, business *ftsmodels.Business) {
	str_time := strings.Split(b.Duration.StartTime, ":")
	end_time := strings.Split(b.Duration.EndTime, ":")
	str_hour, _ := strconv.Atoi(str_time[0])

	str_min, _ := strconv.Atoi(str_time[1])

	end_hour, _ := strconv.Atoi(end_time[0])

	end_min, _ := strconv.Atoi(end_time[1])
	end_day := 1
	if end_hour >= 0 && end_hour < str_hour {
		end_day = 2
	}

	business.Name = b.Name
	business.Duration.StartTime = time.Date(2015, 1, 1, str_hour, str_min, 0, 0, time.Local)
	business.Duration.EndTime = time.Date(2015, 1, end_day, end_hour, end_min, 0, 0, time.Local)
	business.OpenDays = b.OpenDays
	business.Geolocation = b.Geolocation
	business.Info = b.Info
	business.TypeLists = b.TypeLists
	business.IsPublic = b.IsPublic
}
