package logicbusiness

import (
	"ftera/fts/models"
	"time"
)

type EventBody struct {
	Name        string
	StartDate   time.Time
	Info        ftsmodels.BusinessInfo
	TypeLists   ftsmodels.TypeLists
	Geolocation []float64
	IsPublic    bool
}

func SetupEvent(eb *EventBody, event *ftsmodels.Event) {
	event.Name = eb.Name
	event.StartDate = eb.StartDate
	event.Info = eb.Info
	event.TypeLists = eb.TypeLists
	event.Geolocation = eb.Geolocation
	event.IsPublic = eb.IsPublic
}
