package logicinviter

import (
	"fmt"
	"ftera/fts/models"
	"ftera/fts/models/enums"
	"ftera/fts/models/tablenames"
	"log"
	"time"

	"github.com/imdario/mergo"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type CalendarBody struct {
	Date        time.Time //to check the end on
	Address     string
	Geolocation []float64
	Tags        []string
	TypeLists   ftsmodels.TypeLists
	Kilometer   int
}

func getQueryDate(date time.Time) bson.M {
	return bson.M{
		"duration.starttime": bson.M{"$lte": date},
		"duration.endtime":   bson.M{"$gte": date},
	}

}

func getQueryList(name_list string, list []string) bson.M {
	query := bson.M{}
	name := "typelists." + name_list
	if len(list) > 0 {
		query = bson.M{name: bson.M{"$all": list}}
	}
	return query
}

// +smoking will query smoking to true
func getQueryTags(db *mgo.Database, stags []string) bson.M {
	tags := []ftsmodels.Tag{}
	for _, v := range stags {
		tag, err := ftsmodels.FindTag(db, v)
		if err == nil {
			tags = append(tags, *tag)
		}
		if err != nil {
			log.Println(err.Error())
		}
	}

	grouped := map[int][]string{
		enums.BusinessTypeTag: []string{},
		enums.ComfortTag:      []string{},
		enums.EventTypeTag:    []string{},
		enums.MusicTypeTag:    []string{},
		enums.ProductTag:      []string{},
		enums.ProductTypeTag:  []string{},
		enums.StuffTypeTag:    []string{},
	}
	for _, v := range tags {
		grouped[v.TypeNum] = append(grouped[v.TypeNum], v.Name)
	}

	query_tag := bson.M{}

	mergo.Merge(&query_tag, getQueryList("businesstypes", grouped[enums.BusinessTypeTag]))
	mergo.Merge(&query_tag, getQueryList("comforts", grouped[enums.ComfortTag]))
	mergo.Merge(&query_tag, getQueryList("eventtypes", grouped[enums.EventTypeTag]))
	mergo.Merge(&query_tag, getQueryList("musictypes", grouped[enums.MusicTypeTag]))
	mergo.Merge(&query_tag, getQueryList("products", grouped[enums.ProductTag]))
	mergo.Merge(&query_tag, getQueryList("producttypes", grouped[enums.ProductTypeTag]))
	mergo.Merge(&query_tag, getQueryList("stufftypes", grouped[enums.StuffTypeTag]))
	return query_tag
}

func SearchBusinesses(db *mgo.Database, cb *CalendarBody, page, size int) ([]ftsmodels.Business, error) {
	bt := db.C(tablenames.Business)
	bt.EnsureIndex(ftsmodels.GEO_INDEX)
	log.Println(cb)
	hour := cb.Date.Hour()
	min := cb.Date.Minute()
	loc := time.FixedZone("UTC", 0)

	first_date := time.Date(2015, 1, 1, hour, min, 0, 0, loc)

	query_date := getQueryDate(first_date)
	if hour >= 0 && hour < 5 {
		second_date := time.Date(2015, 1, 2, hour, min, 0, 0, loc)
		query_date = bson.M{"$or": []bson.M{
			getQueryDate(first_date),
			getQueryDate(second_date)}}
	}

	query := bson.M{"ispublic": true}

	const layout = "Monday"
	str_day := fmt.Sprint(cb.Date.Format(layout))
	query_day := bson.M{"opendays": bson.M{"$elemMatch": bson.M{"$eq": str_day}}}
	if len(cb.Tags) > 0 {
		query_tags := getQueryTags(db, cb.Tags)
		log.Println(query_tags)
		mergo.Merge(&query, query_tags)
	}

	query_geo := bson.M{
		"geolocation": bson.M{"$near": bson.M{
			"$geometry":    bson.M{"type": "Point", "coordinates": cb.Geolocation},
			"$maxDistance": cb.Kilometer * 1000}},
	}
	mergo.Merge(&query, query_date)
	mergo.Merge(&query, query_day)

	mergo.Merge(&query, query_geo)

	log.Println(query)
	businesses := []ftsmodels.Business{}

	skip := size * (page - 1)
	err := bt.Find(query).Skip(skip).Limit(size).All(&businesses)
	if err != nil {
		return businesses, err
	}
	return businesses, nil
}

func SearchEvents(db *mgo.Database, cb *CalendarBody, page, size int) ([]ftsmodels.Event, error) {
	et := db.C(tablenames.Event)
	et.EnsureIndex(ftsmodels.GEO_INDEX)
	events := []ftsmodels.Event{}
	query := bson.M{"ispublic": true}

	query_date := bson.M{"startdate": bson.M{"$gte": cb.Date}}
	query_geo := bson.M{
		"geolocation": bson.M{"$near": bson.M{
			"$geometry":    bson.M{"type": "Point", "coordinates": cb.Geolocation},
			"$maxDistance": cb.Kilometer * 1000}},
	}
	mergo.Merge(&query, query_date)
	mergo.Merge(&query, query_geo)
	if len(cb.Tags) > 0 {
		query_tags := getQueryTags(db, cb.Tags)
		mergo.Merge(&query, query_tags)
	}

	skip := size * (page - 1)
	err := et.Find(query).Skip(skip).Limit(size).All(&events)
	if err != nil {
		return events, err
	}
	return events, nil

}
