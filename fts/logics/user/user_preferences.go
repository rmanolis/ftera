package logicuser

import (
	"fmt"
	"ftera/fts/models"
	"ftera/fts/models/tablenames"

	"math"
	"time"

	"github.com/imdario/mergo"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UserJson struct {
	UserId   bson.ObjectId
	PhotoId  bson.ObjectId
	IsOnline bool
	Sex      string
	Age      int
	Name     string
}

func getNotInQuery(db *mgo.Database, user *ftsmodels.User) *bson.M {
	blocks, _ := user.Blocks(db)
	var ls_user []bson.ObjectId = []bson.ObjectId{}
	for _, v := range blocks {
		ls_user = append(ls_user, v.BlockUserId)
	}

	blocks, _ = user.BlocksByOthers(db)
	for _, v := range blocks {
		ls_user = append(ls_user, v.UserId)
	}
	if len(ls_user) > 0 {
		return &bson.M{"userid": bson.M{"$nin": ls_user}}
	} else {
		return nil
	}
}

func getQuery(user *ftsmodels.User) bson.M {
	pref := user.Preferences
	info := user.Info
	query_info := bson.M{}
	query_info["info.sex"] = pref.Sex

	this_year := time.Now().Year()
	from_birth := this_year - pref.FromAge
	to_birth := this_year - pref.ToAge
	query_info["info.birthdate"] = bson.M{"$gte": to_birth,
		"$lt": from_birth}
	query_geo := bson.M{
		"$near": bson.M{
			"$geometry":    bson.M{"type": "Point", "coordinates": user.Geolocation},
			"$maxDistance": pref.Kilometer * 1000}}

	query_pref := bson.M{}

	query_pref["preferences.sex"] = info.Sex

	query := bson.M{
		"geolocation": query_geo,
		"ispublic":    true,
	}

	if pref.SearchOption == 1 {
		query_online := bson.M{"sessionids.0": bson.M{"$exists": true}}
		mergo.Merge(&query, query_online)
	}

	mergo.Merge(&query, query_info)
	mergo.Merge(&query, query_pref)

	return query
}

type SearchResult struct {
	Total int
	Rows  []UserJson
	Pages int
}

func CollectUsers(db *mgo.Database, user *ftsmodels.User,
	size int, page int) *SearchResult {
	col := db.C(tablenames.User)
	col.EnsureIndex(ftsmodels.GEO_INDEX)
	query := getQuery(user)
	query_not_in := getNotInQuery(db, user)
	if query_not_in != nil {
		mergo.Merge(&query, query_not_in)
	}
	fmt.Println(query)
	var users = []ftsmodels.User{}
	skip := size * page
	total, _ := col.Find(query).Count()
	pages := int(math.Ceil(float64(total) / float64(size)))
	col.Find(query).Sort("-_id").Skip(skip).Limit(size).All(&users)
	var uj_ls = []UserJson{}
	for i := range users {
		u := users[i]
		uj := UserJson{
			UserId:   u.Id,
			Name:     u.Name,
			PhotoId:  u.Info.PhotoId,
			IsOnline: u.IsOnline,
			Sex:      u.Sex(),
			Age:      time.Now().Year() - u.Info.BirthDate,
		}
		uj_ls = append(uj_ls, uj)
	}
	sres := SearchResult{Total: total, Rows: uj_ls, Pages: pages}
	return &sres
}

type UserPreferencesBody struct {
	Id           string
	UserId       string
	FromAge      int
	ToAge        int
	Sex          string
	Kilometer    int
	SearchOption string
}

func SetPreferences(db *mgo.Database, user *ftsmodels.User, data *UserPreferencesBody) error {
	user.SetPrefSex(data.Sex)

	user.SetPrefAge(data.FromAge, data.ToAge)
	user.SetPrefKilometer(data.Kilometer)
	user.SetPrefSearchOption(data.SearchOption)
	return user.Update(db)
}
