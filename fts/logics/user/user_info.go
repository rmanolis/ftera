package logicuser

import (
	"encoding/json"
	"ftera/fts/models"
	"time"
)

type UserInfoBody struct {
	Name        string
	UserId      string
	PhotoId     string
	BirthDate   int
	Age         int
	Geolocation []float64
	Location    string
	Sex         string
	Bio         string
}

func WriteUserInfo(u *ftsmodels.User) ([]byte, error) {
	oui := new(UserInfoBody)
	oui.Name = u.Name
	oui.UserId = u.Id.Hex()
	oui.PhotoId = u.Info.PhotoId.Hex()

	sex := u.Sex()
	oui.Sex = sex

	oui.Bio = u.Info.Bio
	oui.BirthDate = u.Info.BirthDate
	oui.Age = time.Now().Year() - oui.BirthDate
	oui.Geolocation = u.Geolocation
	oui.Location = u.Info.Location
	return json.Marshal(oui)

}
