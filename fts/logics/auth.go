package logic

import (
	"ftera/fts/models"
	"ftera/ftutils"

	"gopkg.in/mgo.v2"
)

func Register(db *mgo.Database,
	name string, email string, password string) []error {
	err_ls := ftsmodels.ValidateUser.RegistrationForm(db, name, email, password)
	if len(err_ls) == 0 {
		user, err := ftsmodels.NewUser(db, name, email, password)
		if err != nil {
			err_ls = append(err_ls, err)
		}

		user.NewInfo()
		user.NewPreferences()
		user.SetValidation(db)
		user.IsValidated = false
		err = user.Update(db)
		if err != nil {
			err_ls = append(err_ls, err)
		} else {

			eu := ftutils.GmailUser
			std := ftutils.SmtpTemplateData{
				To:      user.Email,
				Subject: "Validate your registration to Pullmeout",
				Body:    "Link to https://pullmeout.com/auth/token/" + user.ValidationCode,
			}
			ftutils.SendMail(&eu, &std)
		}

	}
	return err_ls

}
