package logicmessenger

import (
	"ftera/fts/models"
	"ftera/fts/models/enums"
	"ftera/fts/ws/senders"

	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type ChatJson struct {
	Name           string
	Sex            string
	PhotoId        string
	UserId         string
	MessengerId    string
	IsRead         bool
	IsOnline       bool
	UnreadMessages int
}

func GetChats(db *mgo.Database, user *ftsmodels.User) (*[]ChatJson, error) {
	msgers, err := user.Messengers(db)
	if err != nil {
		return nil, err
	}

	var chats = []ChatJson{}
	for _, v := range msgers {
		other, err := user.OtherFromChat(db, v.Id)
		if err == nil {
			chat := new(ChatJson)
			chat.MessengerId = v.Id.Hex()
			chat.Name = other.Name
			chat.Sex = enums.SexList[other.Info.Sex]
			chat.IsRead = v.IsRead
			chat.IsOnline = other.IsOnline
			chat.PhotoId = other.Info.PhotoId.Hex()
			chat.UserId = other.Id.Hex()
			for _, uv := range v.Users {
				if uv.UserId == user.Id {
					chat.UnreadMessages = uv.UnreadMessages
					if !uv.IsIgnoring {
						chats = append(chats, *chat)
					}
				}
			}

		}
	}

	return &chats, err
}

type MessageJson struct {
	Id          string
	Name        string
	UserId      string
	Text        string
	MessengerId string
	Date        string
}

func GetMessages(db *mgo.Database, user *ftsmodels.User,
	mrid bson.ObjectId,
	size int, page int) ([]MessageJson, error) {
	var msgs_json = []MessageJson{}

	mr, err := user.Messenger(db, mrid)
	if err != nil {
		return msgs_json, err
	}

	other, err := user.OtherFromChat(db, mr.Id)
	if err != nil {
		log.Fatalln(err.Error())
		return msgs_json, err

	}

	ftswsenders.ReadMessage(other, mr.Id)

	msgs, err := mr.Messages(db, size, page)
	if err != nil {
		return msgs_json, err
	}
	for _, v := range msgs {
		mj := MessageJson{
			Id:          v.Id.Hex(),
			MessengerId: v.MessengerId.Hex(),
			Text:        v.Text,
			Date:        v.Date.Format("02 Jan 15:04"),
		}
		if other.Id == v.UserId {
			mj.Name = other.Name
			mj.UserId = other.Id.Hex()
		}
		if user.Id == v.UserId {
			mj.Name = user.Name
			mj.UserId = user.Id.Hex()
		}
		msgs_json = append(msgs_json, mj)
	}
	user.ReadChat(db, mr.Id)

	mr.IsRead = true
	mr.UpdateWithDate(db)
	return msgs_json, nil
}
