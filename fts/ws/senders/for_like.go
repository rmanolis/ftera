package ftswsenders

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/ws/senders/routes"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type WSLike struct {
	UserName string
	UserId   string
	PostId   string
}

func UserLikedPost(db *mgo.Database, user *ftsmodels.User, owner_user *ftsmodels.User,
	postid bson.ObjectId) {
	wp := new(WSLike)
	wp.UserId = user.Id.Hex()
	wp.PostId = postid.Hex()
	wp.UserName = user.Name

	out, _ := json.Marshal(wp)
	owner_user.ToChannel(ftswsroutes.NewLike, out)
}
