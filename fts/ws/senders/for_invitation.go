package ftswsenders

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/ws/senders/routes"
	"time"
)

func InviteUser(other *ftsmodels.User, inv *ftsmodels.Invitation) {
	out, _ := json.Marshal(inv)
	other.ToChannel(ftswsroutes.Invited, out)
}

type WSReplyInvitation struct {
	ToUserName string
	WhereName  string
	Date       time.Time
}

func ReplyInvitation(from_user *ftsmodels.User, to_user *ftsmodels.User,
	where_name string, date time.Time, is_accepted bool) {
	wri := new(WSReplyInvitation)
	wri.ToUserName = to_user.Name
	wri.Date = date
	wri.WhereName = where_name
	out, _ := json.Marshal(wri)
	if is_accepted {
		from_user.ToChannel(ftswsroutes.InvitationAccepted, out)
	} else {
		from_user.ToChannel(ftswsroutes.InvitationDeclined, out)
	}
}
