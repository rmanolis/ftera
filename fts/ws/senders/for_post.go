package ftswsenders

import (
	"ftera/fts/models"
	"ftera/fts/ws/senders/routes"
	"ftera/ftws/pool"
	"log"

	"gopkg.in/mgo.v2"
)

func PostToNearUsers(db *mgo.Database, post *ftsmodels.Post) {
	sesids, err := post.SessionIdFromNearUsers(db, 3)
	if err != nil {
		log.Println(err)
	}
	for _, v := range sesids {
		ftwspool.ToSession(ftswsroutes.GetNewPost, v, []byte(post.Id.Hex()))
	}
}
