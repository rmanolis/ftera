package ftswsenders

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/ws/senders/routes"
	"ftera/ftws/pool"

	"gopkg.in/mgo.v2"
)

type WSAttend struct {
	UserName  string
	UserId    string
	EventName string
	EventId   string
}

func UserWillGo(db *mgo.Database, user *ftsmodels.User,
	event *ftsmodels.Event) {
	wp := new(WSAttend)
	wp.UserId = user.Id.Hex()
	wp.EventId = event.Id.Hex()
	wp.UserName = user.Name
	wp.EventName = event.Name
	ses_ls := event.SessionIdFromAttentions(db)
	out, _ := json.Marshal(wp)

	for _, v := range ses_ls {
		ftwspool.ToSession(ftswsroutes.NewAttendance, v, out)
	}
}
