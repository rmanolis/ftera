package ftswsenders

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/ws/senders/routes"
	"ftera/ftws/pool"

	"gopkg.in/mgo.v2/bson"
)

type MessageJson struct {
	Id          string
	Name        string
	UserId      string
	Text        string
	MessengerId string
	Date        string
}

func NewMessageToOther(user *ftsmodels.User, other *ftsmodels.User,
	messenger *ftsmodels.Messenger, msg *ftsmodels.Message) {

	var wm = MessageJson{
		Text:        msg.Text,
		UserId:      user.Id.Hex(),
		MessengerId: messenger.Id.Hex(),
		Name:        user.Name,
		Date:        msg.Date.Format("02 Jan 15:04"),
	}

	var sesids = other.SessionIds
	sesids = append(sesids, user.SessionIds...)
	out, _ := json.Marshal(wm)
	for _, v := range sesids {
		ftwspool.ToSession(messenger.Id.Hex(), v, out)
	}
	for _, v := range messenger.Users {
		if v.UserId == other.Id {
			if v.IsIgnoring {
				return
			}
		}
	}
	for _, v := range other.SessionIds {
		ftwspool.ToSession(ftswsroutes.NewMessage, v, out)
	}
}

func ReadMessage(other *ftsmodels.User, msngrId bson.ObjectId) {
	out, _ := json.Marshal(map[string]string{"MessengerId": msngrId.Hex()})
	for _, v := range other.SessionIds {
		ftwspool.ToSession(ftswsroutes.ReadMessage, v, out)
	}
}
