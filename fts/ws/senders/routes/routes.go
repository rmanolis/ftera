package ftswsroutes

const (
	Invited            = "invited"
	InvitationAccepted = "invitation-accepted"
	InvitationDeclined = "invitation-declined"

	GetNewPost    = "get-new-post"
	NewLike       = "new-like"
	NewAttendance = "new-attendance"
	NewMessage    = "new-message"
	ReadMessage   = "read-message"
)
