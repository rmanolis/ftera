package ftswreceivers

import (
	"ftera/ftconfigs"
	"ftera/fts/models"
	"ftera/fts/ws/senders"
	"ftera/ftws/structs"
	"gopkg.in/mgo.v2/bson"
	"log"
)

func onReadMessage(con *ftwsstructs.WSConn, wsm *ftwsstructs.WSMessage) {
	dt := ftconfigs.FteraDB()
	defer dt.Session.Close()
	db := dt.Session.DB(dt.DBName)
	user, err := ftsmodels.FindUserBySession(db, con.Id)
	if err != nil {
		log.Println(err.Error())
		return
	}
	if !bson.IsObjectIdHex(wsm.Message) {
		return
	}
	msngrId := bson.ObjectIdHex(wsm.Message)
	other, _ := user.OtherFromChat(db, msngrId)
	msngr, _ := user.Messenger(db, msngrId)
	msngr.IsRead = true
	msngr.UpdateWithDate(db)
	ftswsenders.ReadMessage(other, msngrId)
}
