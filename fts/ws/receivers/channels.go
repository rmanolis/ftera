package ftswreceivers

import (
	"encoding/json"
	"ftera/ftconfigs"
	"ftera/fts/models"
	"ftera/ftws/pool"
	"ftera/ftws/structs"
	"log"
)

var Routes = map[string]func(*ftwsstructs.WSConn, *ftwsstructs.WSMessage){
	"broadcast":     onBroadcast,
	"register":      onRegister,
	"unregister":    onUnregister,
	"sendMySession": onSendMySession,
	"readMessage":   onReadMessage,
}

func onRegister(con *ftwsstructs.WSConn, wsm *ftwsstructs.WSMessage) {

}

func onUnregister(con *ftwsstructs.WSConn, wsm *ftwsstructs.WSMessage) {
	dt := ftconfigs.FteraDB()
	defer dt.Session.Close()
	db := dt.Session.DB(dt.DBName)

	user, err := ftsmodels.FindUserBySession(db, con.Id)
	if err != nil {
		log.Println(err.Error())
		return
	}
	index := -1
	for i, v := range user.SessionIds {
		if v == con.Id {
			index = i
		}
	}
	nls := append(user.SessionIds[:index], user.SessionIds[index+1:]...)
	user.SessionIds = nls
	if len(nls) == 0 {
		user.IsOnline = false
	}
	user.Update(db)
}

func onBroadcast(con *ftwsstructs.WSConn, wsm *ftwsstructs.WSMessage) {
	out, _ := json.Marshal(wsm)
	ftwspool.Broadcast(out)
}

func onSendMySession(con *ftwsstructs.WSConn, wsm *ftwsstructs.WSMessage) {
	log.Println("sending my session")
	con.ToChannel("mySession", []byte(con.Id))
}
