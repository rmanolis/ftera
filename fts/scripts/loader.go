package ftsscripts

import (
	"ftera/fts/models"
	"ftera/fts/models/enums"
	"ftera/ftutils"
)

func LoadTags(dt *ftutils.DataStore) {
	session := dt.Session.Clone()
	db := session.DB("ftera")
	defer session.Close()

	for _, v := range MusicTags {
		ftsmodels.AddTag(db, v, enums.MusicTypeTag)
	}

	for _, v := range StuffTags {
		ftsmodels.AddTag(db, v, enums.StuffTypeTag)
	}

	for _, v := range EventTags {
		ftsmodels.AddTag(db, v, enums.EventTypeTag)
	}

	for _, v := range BusinessTags {
		ftsmodels.AddTag(db, v, enums.BusinessTypeTag)
	}

	for _, v := range ProductTags {
		ftsmodels.AddTag(db, v, enums.ProductTag)
	}

	for _, v := range ProductTypeTags {
		ftsmodels.AddTag(db, v, enums.ProductTypeTag)
	}

	for _, v := range ComfortTags {
		v.TypeNum = enums.ComfortTag
		ftsmodels.SetTag(db, v)
	}

}
