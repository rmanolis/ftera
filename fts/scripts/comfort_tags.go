package ftsscripts

import "ftera/fts/models"

var ComfortTags = []ftsmodels.Tag{
	ftsmodels.Tag{Name: "+smoking",
		Explanation: "Smoking is allowed"},
	ftsmodels.Tag{Name: "-smoking",
		Explanation: "Smoking is not allowed"},

	ftsmodels.Tag{Name: "+reservation",
		Explanation: "Reservation is allowed"},
	ftsmodels.Tag{Name: "-reservation",
		Explanation: "Reservation is not allowed"},

	ftsmodels.Tag{Name: "+delivery",
		Explanation: "Delivery is allowed"},
	ftsmodels.Tag{Name: "-delivery",
		Explanation: "Delivery is not allowed"},

	ftsmodels.Tag{Name: "+take-out",
		Explanation: "Take out is allowed"},
	ftsmodels.Tag{Name: "-take-out",
		Explanation: "Take out is not allowed"},

	ftsmodels.Tag{Name: "+credit-card",
		Explanation: "Credit card is allowed"},
	ftsmodels.Tag{Name: "-credit-card",
		Explanation: "Credit card is not allowed"},

	ftsmodels.Tag{Name: "+parking",
		Explanation: "Parking is allowed"},
	ftsmodels.Tag{Name: "-parking",
		Explanation: "Parking is not allowed"},

	ftsmodels.Tag{Name: "+outdoor-seating",
		Explanation: "It has outdoor seating"},
	ftsmodels.Tag{Name: "-outdoor-seating",
		Explanation: "It does not have outdoor seating"},

	ftsmodels.Tag{Name: "+pets",
		Explanation: "Pets are allowed"},
	ftsmodels.Tag{Name: "-pets",
		Explanation: "Pets are not allowed"},

	ftsmodels.Tag{Name: "+waiters",
		Explanation: "It has waiters"},
	ftsmodels.Tag{Name: "-waiters",
		Explanation: "It does not have waiters"},

	ftsmodels.Tag{Name: "+cater",
		Explanation: "It has cater"},
	ftsmodels.Tag{Name: "-cater",
		Explanation: "It does not have cater"},

	ftsmodels.Tag{Name: "+view",
		Explanation: "It has a view"},
	ftsmodels.Tag{Name: "-view",
		Explanation: "It does not have a view"},
}
