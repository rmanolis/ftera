from mongoengine import *

connect('ftera')

class Tag(Document):
    name = StringField(required=True)
    typenum = IntField()

tags = [
    "Dubstep",
    "jazz",
    "chinese-food",
    "indian-food",
    "japanese-food",
    "club",
    "bar",
    "coffee-bar",
    "coffee-shop",
    "restaurant",
    "greek-food",
    "italian-food",
    "pizza",
    "kebab-shop",
    "vegan",
    "vegeterian",
    "strip-club",
    "fast-food",
    "folk",
    "bar",
    "hotel",
    "rock",
    "metal",
    "drum-n-bass",
    "dubstep",
    "house-music",
    "pop",
    "rent-a-car"
]
def insert_tags():
    for tag in tags:
        t = Tag.objects(name=tag).first()
        if not t:
            t = Tag()
            t.name = tag
            t.save()

insert_tags()

