package ftsscripts

import (
	"ftera/fts/models"
	"ftera/fts/models/tablenames"
	"ftera/ftutils"

	"gopkg.in/mgo.v2/bson"
)

func FixUsers(dt *ftutils.DataStore) {
	session := dt.Session.Clone()
	db := session.DB("ftera")
	defer session.Close()
	c := db.C(tablenames.User)
	users := []ftsmodels.User{}
	c.Find(bson.M{}).All(&users)
	for _, v := range users {
		if v.Geolocation[0] == 0 {
			v.Geolocation = []float64{23.729359899999963, 37.983917}
			v.Info.Location = "Athens, Greece"
			v.Update(db)
		}
	}
}
