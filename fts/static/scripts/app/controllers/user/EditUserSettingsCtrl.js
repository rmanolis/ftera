app.controller('EditUserSettingsCtrl',
    ["$rootScope","$location", "$scope","toastr", "UserSrv",
    function($rootScope,$location, $scope,toastr, UserSrv){
  $scope.user = {};
  $scope.blocked_users = [];
  $scope.blocked_by_users = [];
  UserSrv.getProfile().success(function(data){
    $scope.user = data;
  })

  UserSrv.getBlockedUsers().success(function(data){
    $scope.blocked_users = data;
  })

  UserSrv.getBlockedByUsers().success(function(data){
    console.log(data)
    $scope.blocked_by_users = data;
  })

  $scope.save = function(user){
    UserSrv.setSettings(user).success(function(){ 
      $rootScope.$broadcast("is:business");
      toastr.info("Your settings has been saved");
      $location.path('/');
    }).error(function(data){
      alert(data);
    })
  }

  $scope.delete = function(){
    UserSrv.deleteUser().success(function(){
      toastr.info("Your profile has been deleted");
      UserSrv.logout();
    })
  }

  $scope.unblock = function(user_id){
    UserSrv.unblockUser(user_id).success(function(){
      UserSrv.getBlockedUsers().success(function(data){
        $scope.blocked_users = data;
      })
    }).error(function(data){
        console.log(data);
      })

  }

}]);
