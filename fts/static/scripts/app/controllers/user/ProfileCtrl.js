app.controller('ProfileCtrl', 
    ["$scope", "$routeParams","$rootScope", "$location" ,"UserSrv", 
    function($scope, $routeParams,$rootScope, $location ,UserSrv){
      $scope.user = {};
      $scope.info = {};
      $scope.isOtherUser = false;


      UserSrv.getProfile()
        .success(function(data){
          $scope.user = data; 
        })

      UserSrv.getProfileInfo().success(function(info){
        $scope.info = info;
        $scope.info.BirthDate = moment(new Date(info.BirthDate)).format('YYYY/MM/DD');
        if(!_.isUndefined($scope.info.Mbti) && !_.isNull($scope.info.Mbti)){
          $scope.info.Mbti = info.Mbti;
        }else{
          $scope.info.Mbti = "None";
        }
        if(!_.isUndefined($scope.info.Sex) && !_.isNull($scope.info.Sex)){
          $scope.info.Sex =info.Sex;
        }else{
          $scope.info.Sex = "Male";
        }
        if(!_.isUndefined($scope.info.Horoscope) && !_.isNull($scope.info.Horoscope)){
          $scope.info.Horoscope =  info.Horoscope;
        }else{
          $scope.info.Horoscope = "None";
        }

      });





    }])
