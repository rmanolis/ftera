app.controller('UserCtrl', 
    ["$scope", "$routeParams", "$location" ,"UserSrv", "ReportSrv", "MessengerSrv",
    function($scope, $routeParams, $location ,UserSrv, ReportSrv, MessengerSrv){
  $scope.user = {};
  $scope.info = {};
  $scope.userId = $routeParams.id;
  $scope.isOtherUser = true;
  
  UserSrv.getUser($routeParams.id).success(function(data){
    $scope.user = data;
  })
  UserSrv.getUserInfo($routeParams.id).success(function(data){
      $scope.info = data;
  }).error(function(){
    $location.path('/');
  });
      
  

  $scope.block = function(){
    UserSrv.blockUser($routeParams.id).success(function(){
      $location.path('/'); 
    })
  }

  $scope.report = function(){
    ReportSrv.openReport($routeParams.id);
  }

  $scope.message = function(){
    MessengerSrv.createMessenger($routeParams.id).success(function(messenger){
      MessengerSrv.openMessenger($routeParams.id);
    })
  }

 
  

  

}])
