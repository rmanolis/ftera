app.controller('EditUserInfoCtrl',
    ["$scope","$rootScope","$location", "$modal", "$http",
      "UserSrv","AddressSrv",
    function($scope,$rootScope,$location, $modal, $http,
      UserSrv,AddressSrv){

  $scope.info = { Location:"athens, greece"};

  $scope.sexList = [];

  
  UserSrv.getSexList().success(function(data){
    $scope.sexList = angular.copy(data);
  })
 

  UserSrv.getProfile().success(function(user){
    $scope.user = user;
  });
  function getInfo(){
    UserSrv.getProfileInfo().success(function(info){
      $scope.info = angular.copy(info); 
    })
  }
  getInfo();

  $scope.deletePhoto = function(){
    UserSrv.deletePhoto().success(function(){
        $scope.info.PhotoId = "";
    })
  }

  $scope.openCamera = function () {
    var modalInstance = $modal.open({
      templateUrl: '/static/views/user/camera_photo.html',
      controller: 'CameraPhotoCtrl',
      size: 'lg',
      resolve: {
      }
    });

    modalInstance.result.then(function () {
      getInfo(); 
    }, function () {
      //Do nothing after cancel     
    });
  };

  
  $scope.getLocation = AddressSrv.getAutocompletion;
  $scope.save = function(info){
    console.log(info.Sex)
    AddressSrv.findAddress(info.Location, function(geolocation){
      info.Geolocation = geolocation;
      console.log(info.Geolocation);
      UserSrv.setProfileInfo(info).success(function(){
        $location.path('/');
        $rootScope.$broadcast("updated:user");
      }).error(function(data){
        console.log(data);
      })
    });

  }


}]);
