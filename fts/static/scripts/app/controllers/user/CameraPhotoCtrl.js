app.controller('CameraPhotoCtrl',
    ["$scope","$timeout","$modalInstance",
    function($scope,$timeout,$modalInstance){
  $scope.isCameraOn = false; 
  $scope.image = null;
  $scope.style="width:320px; height:240px;";
  $scope.openCamera = function(){
    $scope.isCameraOn=true;  
    $timeout(function(){
    Webcam.attach( '#my_camera' );
    },300);
  }

  $scope.snap = function(){
    Webcam.snap( function(data_uri) {
      $scope.image = data_uri;
      document.getElementById('my_result').innerHTML = '<img src="'+$scope.image+'"/>';

    });
  }

  $scope.ok = function () {
    if($scope.image){
      Webcam.upload( $scope.image, '/users/profile/photo', function(code, text) {
        console.log(text);
        Webcam.reset();
        $modalInstance.close();
      });
    }
    
    
  };

  $scope.cancel = function () {
    if(Webcam.container){
      Webcam.reset();
    }
    $modalInstance.dismiss('cancel');
  };


}])
