app.controller("ForgotPasswordCtrl",[
    "$scope","$location", "AuthSrv","toastr",
    function($scope,$location, AuthSrv,toastr){
    $scope.send = function(email){
      AuthSrv.forgotPassword(email).success(function(){
        toastr.info("We are sending your new password");
        $location.path("/");
      })
    }

    $scope.login = function(){
      $location.path("/");
    }
}])
