app.controller('LoginCtrl', 
    ["$scope", "$window","$location" ,"$rootScope", "AuthSrv",
      "UserSrv",     
      function($scope, $window, $location, $rootScope, AuthSrv,
      UserSrv){
  $scope.user = {}
  $scope.user.email = "";
  $scope.user.password = "";
  $scope.login = function (user) {
    AuthSrv.login({email: user.email, password: user.password}).
      success(function () {
         $rootScope.$broadcast("successful:login");
         $location.path('/');   
      })
    .error(function(error){
        alert('Wrong email and password');
    });
  };

  $scope.register = function(){
    $location.path("/register");
  }

}]);
