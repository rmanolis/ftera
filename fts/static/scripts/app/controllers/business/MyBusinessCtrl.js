app.controller("MyBusinessCtrl", 
    ["$scope","$location","toastr", "BusinessSrv", "EventSrv",
    function($scope,$location,toastr, BusinessSrv, EventSrv){
  $scope.businesses = [];
  $scope.choosenBusiness = null;
  $scope.events = [];

  function getBusinesses(){
    BusinessSrv.getBusinesses().success(function(businesses){
      $scope.businesses = businesses;
    }).error(function(data){
      console.log(data)
    })
  }

  function getEvents(){
    EventSrv.getEvents().success(function(events){
      $scope.events=events;
    })
  }

  getBusinesses();
  getEvents();



  $scope.addBusiness = function(){
    $location.path("/businesses/add");
  }

  $scope.editBusiness = function(business){
    $location.path("/businesses/edit/"+business.Id);
  }

  $scope.deleteBusiness = function(business){
    BusinessSrv.deleteBusiness(business.Id).success(function(){
      toastr.info("Business deleted");
      getBusinesses();
    })
  }

  $scope.addEvent = function(){
    $location.path("/events/add")
  }

  $scope.editEvent = function(ev){
    $location.path("/events/edit/"+ev.Id)
  }

  $scope.deleteEvent = function(event){
    EventSrv.deleteEvent(event.Id).success(function(){
      toastr.info("Event deleted");
      getEvents();
    })
  }


  $scope.showEvent = function(ev){
    EventSrv.openEvent(ev.Id);
  }

  $scope.showBusiness = function(business){
    BusinessSrv.openBusiness(business.Id);
  }


}])
