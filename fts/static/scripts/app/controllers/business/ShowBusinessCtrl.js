app.controller("ShowBusinessCtrl", 
    ["$scope","$rootScope", "$modalInstance", "PostSrv","PostUISrv", "BusinessSrv","business_id",
    function($scope,$rootScope, $modalInstance, PostSrv,PostUISrv, BusinessSrv,business_id){
  $scope.business = {};
  $scope.posts = [];
  $scope.userId = $rootScope.userId;
  $scope.numPostPages = 1;
  $scope.hasPostPages = true;
  $scope.size = 2;
  

  BusinessSrv.showBusiness(business_id).success(function(business){
    $scope.business = business;
    console.log(business);
    getPosts();    
  }).error(function(){
    $modalInstance.dismiss('cancel'); 
  });
  
  $scope.getStrTime = BusinessSrv.getStrTime;
  $scope.showPost = function(post_id){
   PostSrv.openShowPost(post_id);
  }
  
 
  $scope.showByComma= function(list){
     var str = "";
    angular.forEach(list, function(item,i){
      if(i == 0){
        str += item;
      }else{
        str += ", " + item;
      }
    })
    return str;

  }
    

  function getPosts(){
    PostSrv.getBusinessPosts(business_id,0,$scope.size).success(function(posts){
      $scope.posts = posts;
      if(posts.length >= $scope.size){
        $scope.hasPostPages = true;
      }else{
        $scope.hasPostPages = false;
      }
    })
  }
  $scope.getPosts= getPosts;

  $scope.addPost = function(){
    var modalInstance = PostSrv.openAddPost(business_id,null);
     modalInstance.result.then(function () {
     getPosts(); 
    });
  }

  $scope.close = function(){
   $modalInstance.dismiss('cancel'); 
  }

   $scope.morePosts = function(){
    PostSrv.getBusinessPosts(business_id,$scope.numPostPages,$scope.size)
      .success(function(posts){
      if(posts.length > 0){
        angular.forEach(posts, function(post){
          $scope.posts.push(post);
        })
        $scope.numPostPages += 1;
      }else{
        $scope.hasPostPages = false;
      }
    })
  }


}])
