app.controller("ShowEventCtrl", 
  ["$scope", "$modalInstance", "EventSrv", "PostSrv","WillGoSrv","event_id",
  function($scope, $modalInstance, EventSrv, PostSrv,WillGoSrv,event_id){
  function countAttentions(){
    WillGoSrv.countAttentions(event_id).success(function(data){
      $scope.numAttentions = data.Count;
    })
  }

  $scope.event = {};
  $scope.isAttending = false;
  $scope.posts = [];
  $scope.numAttentions = 0 ;
  $scope.numPostPages = 1;
  $scope.hasPostPages = true;
  $scope.size = 2;
  WillGoSrv.getAttention(event_id).success(function(){
    $scope.isAttending = true;
  }).error(function(data){
    console.log(data);
    $scope.isAttending = false;
  })
  countAttentions();

  EventSrv.showEvent(event_id).success(function(event){
    $scope.event = event;
    $scope.event.StartDate = moment(event.StartDate).format("DD/MM/YYYY HH:mm")
    getPosts();
  }).error(function(){
    $modalInstance.dismiss('cancel'); 
  });

  $scope.close = function(){
   $modalInstance.dismiss('cancel'); 
  }

  $scope.showPost = function(post_id){
   PostSrv.openShowPost(post_id);
  }
  
  $scope.showByComma= function(list){
     var str = "";
    angular.forEach(list, function(item,i){
      if(i == 0){
        str += item;
      }else{
        str += ", " + item;
      }
    })
    return str;

  }

   function getPosts(){
    PostSrv.getEventPosts(event_id,0,$scope.size).success(function(posts){
      $scope.posts = posts;
      if(posts.length >= $scope.size){
         $scope.hasPostPages = true;
      }else{
         $scope.hasPostPages = false;
      }
    }).error(function(data){
      console.log(data);
    })
  }
$scope.getPosts= getPosts;

   
  $scope.addPost = function(){
   var modelInstance = PostSrv.openAddPost(null,$scope.event.Id);
   modelInstance.result.then(function () {
     getPosts(); 
    });

  }

  $scope.willGo = function(){
    WillGoSrv.addAttention(event_id).success(function(){
      $scope.isAttending = true;
      countAttentions();
    }).error(function(data){
      console.log(data);
    })
  }

  $scope.willNotGo = function(){
    WillGoSrv.removeAttention(event_id).success(function(){
      $scope.isAttending = false;
      countAttentions();
    })
  }

  $scope.whoWillGo = function(){
    WillGoSrv.openAttentionList(event_id);
  }

  $scope.morePosts = function(){
    PostSrv.getEventPosts(event_id,$scope.numPostPages, $scope.size)
      .success(function(posts){
      if(posts.length > 0){
        angular.forEach(posts, function(post){
          $scope.posts.push(post);
        })
        $scope.numPostPages += 1;
      }else{
        $scope.hasPostPages = false;
      }
    })
  }


}])
