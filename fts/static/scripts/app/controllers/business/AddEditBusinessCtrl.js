app.controller("AddEditBusinessCtrl", 
    ["$log","$location","$scope","$http", "Upload",
      "$routeParams","BusinessSrv", "AddressSrv",
    function($log,$location,$scope,$http, Upload,
      $routeParams,BusinessSrv, AddressSrv ){
  $scope.days = [];
  $scope.startTime = new Date();
  $scope.endTime = new Date();
  $scope.business = {
    Duration :{
      StartMin:0,
      StartHour:0,
      EndMin:0,
      EndHour:0
    },
    Info : {},
    TypeLists:{
      MusicTypes :   [],
      StuffTypes  :  [],
      BusinessTypes: [],
      EventTypes  :  [],
      ProductTypes : [],
      Products     : [],
      Comforts :[],
    },
    IsPublic: true
  }
  $scope.businessId = null;
  if($routeParams.id){
    $scope.businessId = $routeParams.id;
  }

  BusinessSrv.getDays().success(function(days){
    angular.forEach(days,function(day){
      $scope.days.push({ text:day})
    })
    $scope.openDays=days;
  });

  $scope.upload = function(files){
    $scope.Files = files;
    var reader = new FileReader();
    var preview =  $('#preview');
    reader.onload = function (e) {
     preview.attr('src', e.target.result);
    }
    if (files.length > 0) {
      reader.readAsDataURL(files[0]);
      $scope.business.PhotoId=false;
    }else{
      preview.src = "";
    }

  }

  if($scope.businessId){
    BusinessSrv.getBusiness($scope.businessId)
      .success(function(business){   
        $scope.business = business;
        var start_time =new Date( business.Duration.StartTime);
        var end_time = new Date(business.Duration.EndTime);
        $scope.startTime =new Date();
        $scope.startTime.setHours(start_time.getUTCHours());
        $scope.startTime.setMinutes(start_time.getUTCMinutes());

        $scope.endTime = new Date();
        $scope.endTime.setHours(end_time.getUTCHours());
        $scope.endTime.setMinutes(end_time.getUTCMinutes());
        $scope.openDays = business.OpenDays;
        $scope.tags = business.Tags;
      }).error(function(){

      })
  }

  $scope.reloadDays = function() {
    $scope.openDays = angular.copy($scope.days);

  };


  function uploadFile(business_id){
    if ($scope.Files && $scope.Files.length) {
      for (var i = 0; i < $scope.Files.length; i++) {
        var file = $scope.Files[i];
        Upload.upload({
          url: '/users/profile/businesses/'+business_id +'/photo',
          file: file
        }).progress(function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
        }).success(function (data, status, headers, config) {
          $scope.business.PhotoId = data.PhotoId 
        }).error(function(data){
          console.log(data);
        });
      }
    } 
  }

  function fixTags(tags){
    return tags.map(function(tag) { return tag.text; })
  }
  $scope.save = function(business,openDays,typelists){
    var object2 = angular.copy(business);
    object2.OpenDays = fixTags(openDays);
    object2.Duration.StartTime = $scope.startTime.getHours() + ":" + $scope.startTime.getMinutes();
    object2.Duration.EndTime =  $scope.endTime.getHours() + ":" + $scope.endTime.getMinutes();
    var tls = angular.copy(typelists);
    var mtls = fixTags(tls.MusicTypes);
    var stls =  fixTags(tls.StuffTypes);
    var btls = fixTags(tls.BusinessTypes);
    var etls =  fixTags(tls.EventTypes);
    var ptls = fixTags(tls.ProductTypes);
    var pls = fixTags(tls.Products);
    var cls = fixTags(tls.Comforts);
    object2.TypeLists = {
      MusicTypes: mtls,
      StuffTypes : stls,
      BusinessTypes : btls,
      EventTypes : etls,
      ProductTypes : ptls,
      Products : pls,
      Comforts: cls,
    };

    console.log(object2)

      AddressSrv.findAddress(object2.Info.Address,
          function(geolocation){
            if($scope.businessId){
              BusinessSrv.editBusiness($scope.businessId,object2).success(function(){
                uploadFile($scope.businessId)
                  $location.path("/businesses");
              }).error(function(data){
                console.log(data);
              })
            }else{

              object2.Geolocation = geolocation;
              BusinessSrv.addBusiness(object2).success(function(business_id){
                uploadFile(business_id)
                  $location.path("/businesses");
              }).error(function(data){
                console.log(data);
              })

            }
          }) 
  }

  $scope.cancel = function(){
    $location.path("/businesses");
  }




}])
