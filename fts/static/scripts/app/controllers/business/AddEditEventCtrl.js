app.controller("AddEditEventCtrl",
    ["$scope","$rootScope", "$location","$http","Upload",
      "$routeParams","$filter", "EventSrv", "DateSrv","AddressSrv",
    function($scope,$rootScope, $location,$http,Upload,
      $routeParams,$filter, EventSrv, DateSrv,AddressSrv){
  $scope.eventId = $routeParams.event_id;
  $scope.userId = $rootScope.userId;
  $scope.Files = [];
  $scope.business = {
    StartDate: new Date()
  };

  if( $scope.eventId){
    EventSrv.getEvent( $scope.eventId).success(function(ev){
      $scope.business = ev;
      $scope.tags = ev.Tags;
    })
  }


  
  $scope.getLocation = AddressSrv.getAutocompletion;

  $scope.upload = function(files){
    $scope.Files=files;
     var reader = new FileReader();
    var preview =  $('#preview');
    reader.onload = function (e) {
     preview.attr('src', e.target.result);
    }
    if (files.length > 0) {
      reader.readAsDataURL(files[0]);
      $scope.business.PhotoId=false;
    }else{
      preview.src = "";
    }
  }

  function uploadFile(event_id){
     if ($scope.Files.length > 0) {
        var file = $scope.Files[0];
        Upload.upload({
          url: '/users/profile/events/'+event_id +"/photo",
          file: file
        }).progress(function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
        }).success(function (data, status, headers, config) {
          $scope.business.PhotoId = data.PhotoId 
        }).error(function(data){
          console.log(data);
        });
    } 
  }


   function fixTags(tags){
    return tags.map(function(tag) { return tag.text; })
  }

  $scope.save = function(ev, tags){
    var ev = angular.copy(ev);
    ev.StartDate = moment(ev.StartDate).format();
    var tls = angular.copy(ev.TypeLists);
    var mtls = fixTags(tls.MusicTypes);
    var stls =  fixTags(tls.StuffTypes);
    var btls = fixTags(tls.BusinessTypes);
    var etls =  fixTags(tls.EventTypes);
    var ptls = fixTags(tls.ProductTypes);
    var pls = fixTags(tls.Products);
    var cls = fixTags(tls.Comforts);
    ev.TypeLists = {
      MusicTypes: mtls,
      StuffTypes : stls,
      BusinessTypes : btls,
      EventTypes : etls,
      ProductTypes : ptls,
      Products : pls,
      Comforts: cls,
    };
    AddressSrv.findAddress(ev.Info.Address,
        function(geolocation){
          ev.Geolocation = geolocation;
          if( $scope.eventId){
            EventSrv.editEvent($scope.eventId,ev).
              success(function(){
                uploadFile($scope.eventId)
                $location.path('/businesses');
              }).error(function(error){
                console.log(error);
              })
          }else {
            EventSrv.addEvent(ev).success(function(event_id){
              uploadFile(event_id)
              $location.path( '/businesses' );
            }).error(function(error){
              console.log(error);
            })
          }
        })
  }

  $scope.cancel = function(){
    $location.path("/businesses");
  }




}])
