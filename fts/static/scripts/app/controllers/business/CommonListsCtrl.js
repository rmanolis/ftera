app.controller("CommonListsCtrl",[
    "$scope", "TagSrv","$http",
    function($scope, TagSrv,$http){
  $scope.loadMusicTypes = function(query){
    return TagSrv.searchTags(query,0);
  }

  $scope.loadStuffTypes = function(query){
    return TagSrv.searchTags(query,1);
  }

  $scope.loadProducts = function(query){
    return TagSrv.searchTags(query,5);
  }

  $scope.loadComforts = function(query){
    return TagSrv.searchTags(query,6);
  }


  $scope.loadProductTypes = function(query){
    return TagSrv.searchTags(query,4);
  }

  $scope.loadBusinessTypes = function(query){
    return TagSrv.searchTags(query,2);
  }


  $scope.loadEventTypes = function(query){
    return TagSrv.searchTags(query,3);
  }


}])
