app.controller("FindPlaceCtrl",
    ["$scope","$filter","$http","toastr", 
      "AddressSrv", "BusinessSrv", "EventSrv", "DateSrv","CalendarSrv","InvitationSrv",
    function($scope,$filter,$http,toastr,
      AddressSrv, BusinessSrv, EventSrv, DateSrv,CalendarSrv,InvitationSrv ){
 
  $scope.tags = [];
  $scope.query = {
    Date: new Date(),
    Kilometer: 1,
    Address:"Athens, Greece"
  };
  $scope.copiedQuery = angular.copy($scope.query);
  $scope.hasEvents = false;
  $scope.hasBusinesses = false;
  $scope.pageEvents = 2;
  $scope.pageBusinesses = 2;

  $scope.size = 5;
  $scope.declinedToInvitations = [];
  $scope.acceptedToInvitations = [];

  InvitationSrv.getToInvitations().success(function(invitations){
    console.log(invitations);

    angular.forEach(invitations, function(data){
      if(data.Invitation.IsAccepted){
        $scope.acceptedToInvitations.push(data);
      }else{
        $scope.declinedToInvitations.push(data);
      }
    })

  })

  $scope.declinedFromInvitations = [];
  $scope.acceptedFromInvitations = [];

  InvitationSrv.getFromInvitations().success(function(invitations){
    console.log(invitations);
    angular.forEach(invitations, function(data){
      if(data.Invitation.IsAccepted){
        $scope.acceptedFromInvitations.push(data);
      }else{
        $scope.declinedFromInvitations.push(data);
      }
    })

  })


  function findAddress (info, cb) {
    geocoder.geocode( { 'address':info.Location}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var geo = results[0].geometry.location;
        info.Geolocation = [geo.D, geo.k];
        cb(info);
      } else {
        alert("Geocode was not successful for the following reason: " + status);
        cb(null);
      }
    });
  }

  $scope.getLocation = AddressSrv.getAutocompletion;

  $scope.loadTags = function(query){
    return $http.post("/tags/search",{Search:query});
  }

  $scope.search = function(query, tags){
    var query = angular.copy(query);
    $scope.pageEvents = 2;
    $scope.pageBusinesses = 2;
    
    query.Tags = tags.map(function(tag) { return tag.text; });    
    query.Date = moment(query.Date).format(); 

    AddressSrv.findAddress(query.Address,
        function(geolocation){
          query.Geolocation = geolocation;          
          $scope.copiedQuery = angular.copy(query);

          CalendarSrv.searchBusinesses(query,1,$scope.size).success(function(businesses){
            $scope.businesses = businesses;
            if(businesses.length >= $scope.size){
              $scope.hasBusinesses = true;
            }else{
              $scope.hasBusinesses = false;
            }
          }).error(function(data){
            console.log(data);
          })
          CalendarSrv.searchEvents(query,1,$scope.size).success(function(events){
            $scope.events = events;
            if(events.length >= $scope.size){
              $scope.hasEvents = true;             
            }else{
              $scope.hasEvents = false;
              
            }
          }).error(function(data){
            console.log(data);
          })

        });
  }


  $scope.getInvitationStrDate = function(invitation){
    return moment(invitation.Invitation.Date).format("DD/MM HH:mm")
  }
  $scope.inviteBusiness = function(user_id,query,business){
    InvitationSrv.sendInvitation({
      Date:  moment(query.Date).format(),
      ToUserId: user_id,
      BusinessId: business.Id,
    }).success(function(data){
      console.log(data.InvitationId);
    }).error(function(data){
      toastr.error(data);
    })
  }

  $scope.inviteEvent = function(user_id,query,event){
    InvitationSrv.sendInvitation({
      Date: moment(query.Date).format(),
      ToUserId: user_id,
      EventId: event.Id,
    }).success(function(data){
      console.log(data.InvitationId);
    }).error(function(data){
      toastr.error(data);
    })
  }

  $scope.openInvitation = function(inv_id){
    InvitationSrv.openInvitation(inv_id);
  }

  $scope.moreBusinesses = function(){
          CalendarSrv.searchBusinesses($scope.copiedQuery, 
              $scope.pageBusinesses,$scope.size).success(function(businesses){
             angular.forEach(businesses, function(business){
                $scope.businesses.push(business);
             });
            if(businesses.length > 0){
              $scope.pageBusinesses += 1;
            }else{
              $scope.hasBusinesses = false;
            }
          }).error(function(data){
            console.log(data);
          })
  }

  $scope.getBusinessStrTime = BusinessSrv.getStrTime;
  $scope.getEventStrDate = EventSrv.getStrDate;
  $scope.openBusiness = BusinessSrv.openBusiness;
  $scope.openEvent = EventSrv.openEvent;
  $scope.moreEvents = function(){
     CalendarSrv.searchEvents($scope.copiedQuery,
         $scope.pageEvents,$scope.size).success(function(events){
            angular.forEach(events, function(event){
                $scope.events.push(event);
             });
            if(events.length > 0){
              $scope.pageEvents += 1;             
            }else{
              $scope.hasEvents = false;
            }
          }).error(function(data){
            console.log(data);
          })

  }

}])
