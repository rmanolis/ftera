app.controller("InvitationCtrl",
    ["$scope","$modalInstance","toastr", "UserSrv","EventSrv",
      "BusinessSrv","InvitationSrv" ,"invitation_id",
    function($scope,$modalInstance,toastr, UserSrv,EventSrv,
      BusinessSrv,InvitationSrv ,invitation_id){
  $scope.where = {};
  $scope.fromUser = {};
  $scope.toUser={};
  InvitationSrv.getInvitation(invitation_id).success(function(invitation){
    console.log(invitation);
    if(invitation.BusinessId){
      BusinessSrv.showBusiness(invitation.BusinessId).success(function(business){
        $scope.where.Name = business.Name;
        $scope.where.show = function(){
            BusinessSrv.openBusiness(invitation.BusinessId);
        }
      })
    }else if(invitation.EventId){
      EventSrv.showEvent(invitation.EventId).success(function(event){
        $scope.where.Name = event.Name;
        $scope.where.show = function(){
            EventSrv.openEvent(invitation.EventId);
        }
      })
    }
    $scope.where.Date = moment(invitation.Date).format("DD/MM/YYYY hh:mm");

    UserSrv.getUserInfo(invitation.FromUserId).success(function(user){
      $scope.fromUser.Name = user.Name;
    })

    UserSrv.getUserInfo(invitation.ToUserId).success(function(user){
      $scope.toUser.Name = user.Name;
    })
  });

  $scope.accept = function(){
    InvitationSrv.acceptInvitation(invitation_id).success(function(){
      toastr.info("You accepted the invitation");
      $modalInstance.dismiss('cancel'); 
    });
  }

  $scope.decline = function(){
    InvitationSrv.declineInvitation(invitation_id).success(function(){
      toastr.info("You declined the invitation");
       $modalInstance.dismiss('cancel');
    })
  }




}])
