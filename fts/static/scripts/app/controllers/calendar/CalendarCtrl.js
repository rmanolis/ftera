app.controller("CalendarCtrl",
    ["$scope","$filter","$http","toastr", "$modalInstance",
      "AddressSrv", "BusinessSrv", "EventSrv", "DateSrv","CalendarSrv","InvitationSrv", "other_id",
    function($scope,$filter,$http,toastr, $modalInstance,
      AddressSrv, BusinessSrv, EventSrv, DateSrv,CalendarSrv,InvitationSrv, other_id ){

  $scope.otherId = other_id;
  $scope.close = function(){
    $modalInstance.dismiss('cancel'); 
  }
 
}])
