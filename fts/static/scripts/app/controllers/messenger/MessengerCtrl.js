app.controller("MessengerCtrl",
    ["$scope","$rootScope","$modalInstance",
    "$timeout","MessengerSrv" ,"toastr", "CalendarSrv", "UserSrv","MessengerChanSrv" ,"user_id",
    function($scope,$rootScope, $modalInstance,
      $timeout,MessengerSrv ,toastr, CalendarSrv, UserSrv,MessengerChanSrv ,user_id){
      UserSrv.getProfileId().success(function(data){
        $scope.UserId = data.Id;
      });

      $scope.message="";
      $scope.messengers = [];
      $scope.messages = [];
      $scope.choosenMessenger = null;
      $scope.page = 2;
      $scope.hasPages = false;
      $scope.isReadLastMessage = false;


      MessengerChanSrv.onReadMessage(function(data){
        var mr = JSON.parse(data);
        if($scope.choosenMessenger && 
            $scope.choosenMessenger.MessengerId === mr.MessengerId){
          $scope.isReadLastMessage = true;
          if($scope.messages && $scope.messages[$scope.messages.length - 1].UserId !== $scope.UserId){
            $scope.isReadLastMessage = false;
          }
        }
      })

      function checkIsOnline(messenger,cb){
        UserSrv.getUser(messenger.UserId).success(function(data){
          cb(data);
        });
      }

      function onNewMessage(messenger){
        MessengerChanSrv.onMessage(messenger.MessengerId, function(data){
          var messenger_id = messenger.MessengerId;
          var message = JSON.parse(data); 
          if($scope.choosenMessenger && 
              $scope.choosenMessenger.MessengerId === messenger_id){
            $scope.$apply(function(){
              $scope.messages.push(message);
              if(message.UserId !== $scope.UserId){
                console.log(message.UserId);
                console.log($scope.UserId);
                $scope.isReadLastMessage = false;
                MessengerChanSrv.readMessage(messenger_id);
              }
            })
          }else{
            MessengerSrv.getMessengers().success(function(messengers){
              $scope.messengers = angular.copy(messengers); 
            })
          }
        });
      }

      function getMessengers(){
        MessengerSrv.getMessengers().success(function(messengers){
          $scope.messengers = angular.copy(messengers);
          angular.forEach(messengers, function(messenger){
            onNewMessage(messenger);
          })

          if(_.isUndefined(user_id) || _.isNull(user_id)){
            if(!_.isEmpty(messengers)){
              checkIsOnline(messengers[0],function(data){
                $scope.isChoosenOnline = data.IsOnline; 
              });

              $scope.choosenMessenger = messengers[0];
            }
          }else{
            angular.forEach(messengers, function(messenger){
              if(messenger.UserId == user_id){
                $scope.choosenMessenger = messenger;
              }
            })
          }
          console.log($scope.choosenMessenger);
          if($scope.choosenMessenger){
            getMessages($scope.choosenMessenger);
          }
        })
      }
      getMessengers();



      function getMessages(messenger){
        MessengerSrv.getMessages(messenger.MessengerId).success(function(messages){
          $scope.messages = _.chain(messages).reverse().value();
          if(messenger.IsRead){
            $scope.isReadLastMessage = true;
          }
          if($scope.messages && $scope.messages[$scope.messages.length - 1].UserId !== $scope.UserId){
            $scope.isReadLastMessage = false;
          }
        });
      }


      $scope.openChat = function(messenger){
        $scope.choosenMessenger=messenger;
        getMessages(messenger);
        checkIsOnline(messenger,function(data){
          $scope.isChoosenOnline = data.IsOnline; 
        });
        MessengerSrv.getMessengers().success(function(messengers){
          $scope.messengers = angular.copy(messengers); 
          $scope.page = 2;
          $scope.hasPages = true;
          $('#message').val('');
        })
      }

      $scope.sendMessage = function(messenger_id,message){
        MessengerSrv.sendMessage(messenger_id,message).success(function(){
          $('#message').val(''); 
          $scope.isReadLastMessage = false;
        }).error(function(data,st){
          if(st == 406){ 
            toastr.error(data);
          }
        })
      }

      $scope.moreMessages = function(){
        MessengerSrv.getMessages($scope.choosenMessenger.MessengerId,
            $scope.page).success(function(messages){
          var rev_msgs =_.chain(messages).reverse().value();
          angular.forEach(rev_msgs,function(msg){
            $scope.messages.unshift(msg);
          })
          MessengerSrv.countMessages($scope.choosenMessenger.MessengerId)
            .success(function(data){
              if($scope.messages.length < data.Total){
                $scope.page += 1;
              }else{
                $scope.hasPages = false;
              }
            })
        })
      }
      $scope.invite = function(messenger){
        CalendarSrv.openCalendar(messenger.UserId);
      }

      $scope.ignore = function(messenger){
        MessengerSrv.ignoreMessenger(messenger.MessengerId).success(function(){
          toastr.info("Chat ignored");
          $rootScope.$emit("ignore:messenger",{MessengerId:messenger.MessengerId})
            getMessengers();
          $scope.choosenMessenger = null;
          $scope.messages = [];
        })
      }


    }])
