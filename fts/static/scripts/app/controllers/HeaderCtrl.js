app.controller("HeaderCtrl",
    ["$scope","$rootScope", "MessengerSrv", "UserSrv","MessengerChanSrv","toastr",'localize',
    function($scope,$rootScope, MessengerSrv,UserSrv,MessengerChanSrv,toastr,localize){

      $scope.unreadMessages =0;
      $scope.showMessage = true;
      $scope.isLoggedIn = false;
      $scope.ignoreMessengers = [];

      $rootScope.$on("ignore:messenger",function(ev,data){
        $scope.ignoreMessengers.push(data.MessengerId);
      })
      function getUnreadMessages(){
        MessengerChanSrv.onNewMessage(function(data){
          var msg = JSON.parse(data);
          if($scope.showMessage){
            $scope.unreadMessages += 1;
            var im_ls =  _.filter($scope.ignoreMessengers,function(n){
              return n === data.MessengerId;
            });
            if(im_ls.length == 0){
              toastr.info(msg.Name+" says \""+msg.Text + "\"");
            }
          }
        })
        MessengerSrv.getMessengers().success(function(messengers){
          angular.forEach(messengers, function(messenger){
            $scope.unreadMessages += messenger.UnreadMessages;
          })
        }) 
      }

      $scope.openMessenger = function(){
        $scope.showMessage = false;
        $scope.unreadMessages = 0;

        var mi = MessengerSrv.openMessenger(null);
        mi.result.then(function(){
          $scope.showMessage = true;
        },function(){
          $scope.showMessage = true;
        });
      }
      getUnreadMessages();

      function getProfile(){
      UserSrv.getProfile()
        .success(function(data){
          $scope.isLoggedIn = true;
          $scope.owner = data; 
          $scope.flag = localize.getFlag(data.Language);
          console.log(data);
        }).error(function(){
          $scope.isLoggedIn = false;
        })
      }
      getProfile();

      $scope.$on("updated:user", function(){
       getProfile(); 
      });


      $scope.logout = function(){
        $rootScope.$broadcast("successful:logout");
      }

      $scope.$on("successful:login", function(){
        $scope.isLoggedIn = true;
        getProfile();
      });
      $scope.$on("successful:logout", function(){
        $scope.isLoggedIn = false; 
      });

    }])
