app.controller("AppCtrl",
    ['$scope', '$rootScope', '$route', '$document',
    function($scope, $rootScope, $route, $document,AuthSrv){
      $window = $(window);

        $scope.main ={
            brand: 'Pullmeout',
            name: 'Lisa Doe'
        }


        $scope.pageTransitionOpts = [
        {"name": 'Fade up',
        "class": 'animate-fade-up'}
        ,   
        {"name": 'Scale up',
          "class": 'ainmate-scale-up'}
        ,   
        { "name": 'Slide in from right',
            "class": 'ainmate-slide-in-right'}
        ,   
        {"name": 'Flip Y',
            "class": 'animate-flip-y'}
        ]

        $scope.admin ={
            layout: 'wide' ,                                 
            menu: 'vertical',                               
            fixedHeader: true ,                             
            fixedSidebar: true,                              
            pageTransition: $scope.pageTransitionOpts[0],
            skin: '11' ,
        }

        $scope.$watch('admin', function(newVal, oldVal){

            if (newVal.menu === 'horizontal' && oldVal.menu === 'vertical'){
                 $rootScope.$broadcast('nav:reset')
                 return
            }
            if( newVal.fixedHeader == false && newVal.fixedSidebar == true){
                if(oldVal.fixedHeader == false && oldVal.fixedSidebar == false){
                    $scope.admin.fixedHeader = true 
                    $scope.admin.fixedSidebar = true
                }
                if(oldVal.fixedHeader == true && oldVal.fixedSidebar == true){
                    $scope.admin.fixedHeader = false 
                    $scope.admin.fixedSidebar = false 
                }
                return
            }
            if (newVal.fixedSidebar == true){
                $scope.admin.fixedHeader = true
            }
            if (newVal.fixedHeader == false){ 
                $scope.admin.fixedSidebar = false
            }

            return
        }
        , true)

        $scope.color ={
            primary:        '#5B90BF',
            success:        '#A3BE8C',
            info:           '#7FABD2',
            infoAlt:        '#B48EAD',
            warning:        '#EBCB8B',
            danger:         '#BF616A',
            gray:           '#DCDCDC',
        }

        $rootScope.$on("$routeChangeSuccess", function(event, currentRoute, previousRoute){
            $document.scrollTo(0, 0);
        })

       }])
