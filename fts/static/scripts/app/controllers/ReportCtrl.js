app.controller("ReportCtrl", 
    ["$scope", "$modalInstance", "ReportSrv", "UserSrv", "user_id",
    function($scope, $modalInstance, ReportSrv, UserSrv, user_id){
 $scope.report = {
  Type:"Racist",
  Description:""
 }
 UserSrv.getUserInfo(user_id).success(function(data){
  $scope.info=data;
 })
 ReportSrv.getTypes().success(function(data){
  $scope.typeList = data;
 })

 $scope.ok = function (report) {
    $modalInstance.close(report);
  }

  $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
  };

}]);
