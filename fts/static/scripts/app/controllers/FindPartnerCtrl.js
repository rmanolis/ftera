app.controller("FindPartnerCtrl", 
    ["$scope","$rootScope","$location","userId","toastr", "UserSrv",
    function($scope,$rootScope,$location,userId,toastr, UserSrv) {
      $scope.preference ={FromAge:18, ToAge:30};
      $scope.users =[];
      $scope.currentPage = 0;
      $scope.userSize = 9;
      $scope.kilometers = [1,2,3,8]
      $scope.userId = userId.data;

      $scope.posts = []; 

      function searchUsers(page){
        UserSrv.searchUsers($scope.userSize,page).success(function(data){
          $scope.users=_.chunk(data.Rows,3)
            $scope.totalUsers = data.Total;
          $scope.numPages = data.Pages ;
          console.log(data);
        }).error(function(data){
          console.log(data);
        })
      }

      searchUsers(0);

      $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
        searchUsers(pageNo);
      };



      UserSrv.getSearchOptions().success(function(data){
        $scope.searchOptions = angular.copy(data);

      })
      UserSrv.getSexList().success(function(data){
        $scope.sexList = angular.copy(data);
      })

      $scope.preference={
        FromAge:20,
        ToAge:30,
        Kilometer:2,
      }
      var age_slider = $( "#age-slider" ).slider({
        formatter: function(value) {
          return 'Age: ' + value[0] + "-" + value[1];
        }
      });   
      age_slider.on("slide",function(event){
        $scope.$apply(function(){
          $scope.preference.FromAge = event.value[0];
          $scope.preference.ToAge = event.value[1]
        })
      });


      UserSrv.getPreferences($scope.maxSize,$scope.currentPage).success(function(data){

        $scope.preference = data;
        age_slider.slider('setValue',[$scope.preference.FromAge, $scope.preference.ToAge]);
      })



      $scope.find= function(preferences){
        UserSrv.setPreferences(preferences).success(function(){
          searchUsers(0)
        }).error(function(data){
          toastr.error(data);
        });
      }

      $scope.showUser = function(user){
        $location.path("/users/profile/"+user.UserId);
      }


    }]);
