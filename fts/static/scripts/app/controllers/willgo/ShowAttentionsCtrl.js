app.controller("ShowAttentionsCtrl",
    ["$scope", "$modalInstance", "WillGoSrv","$location", "event_id",
    function($scope, $modalInstance, WillGoSrv,$location, event_id){
  $scope.attentions = [];
  
  WillGoSrv.getAttentions(event_id).success(function(attentions){
    console.log(attentions);
    $scope.attentions = attentions;
  });

  $scope.openUser = function(user_id){
    $location.path("/users/profile/"+user_id);
  }




}])
