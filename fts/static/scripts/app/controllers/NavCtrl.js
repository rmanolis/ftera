app.controller("NavCtrl", 
    ["$scope", "$http", "MessengerSrv",
      "UserSrv", "CalendarSrv","MessengerChanSrv",
    function($scope, $http, MessengerSrv,
      UserSrv, CalendarSrv,MessengerChanSrv) {
  var getNavigation;
  $scope.sitemap = [];
  $scope.unreadMessages = 0;
  getNavigation = function() {
    return $http.get('/sitemap').success(function(data) {
      console.log(data);
      $scope.sitemap = [];
      return $scope.sitemap = data;
    });
  };

  getNavigation();
  $scope.$on("successful:login", function(){
    getNavigation();
  });
  $scope.$on("successful:logout", function(){
    $scope.sitemap=[]; 
  });
  
  function isBusiness(){
    getNavigation()
  }
  $scope.$on("is:business", isBusiness)
  MessengerSrv.getMessengers().success(function(messengers){
    angular.forEach(messengers, function(messenger){
      $scope.unreadMessages += messenger.UnreadMessages
         MessengerChanSrv.onMessage(messenger.MessengerId, function(message){
          $scope.$apply(function(){   
            $scope.unreadMessages += 1; 
          })
        })
    })
  })

  $scope.openCalendar = function(){
    CalendarSrv.openCalendar();
  }
}]);
