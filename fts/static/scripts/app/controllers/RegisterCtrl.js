app.controller('RegisterCtrl', 
    ["$scope", "$routeParams",
    "$location","AuthSrv",
    function($scope, $routeParams,
      $location,AuthSrv){
      $scope.user = {
        name : "",
        email : "",
        password : "" };

      $scope.save = function(user){
        AuthSrv.register(user)
          .success(function(data){
            alert("we sent an email to validate your registration");
            $location.path('/');
          }).error(function(data){
            console.log(data.errors);
          });

      };

      $scope.login = function(){
        $location.path('/login')
      };

    }])
