app.controller("AddPostCtrl",
    ["$scope","$modalInstance","Upload", "PostSrv", 
      "UserSrv","BusinessSrv","EventSrv","toastr", "event_id","business_id",
    function($scope,$modalInstance,Upload, PostSrv, 
      UserSrv,BusinessSrv,EventSrv,toastr, event_id,business_id ){
  $scope.post = {
    Text:"",
    Files:[]
  }

  console.log(business_id + " " + event_id);
  if(!business_id && !event_id){
    UserSrv.getProfileInfo().success(function(user){
      $scope.post.Name = user.Name;
    })
  }
  if(business_id){
    BusinessSrv.showBusiness(business_id).success(function(business){
      $scope.post.Name = business.Name;
    })
  }
  if(event_id){
    EventSrv.showEvent(event_id).success(function(event){
      $scope.post.Name = event.Name;
    })
  }

  $scope.upload = function(files){
    $scope.post.Files=files;
    var reader = new FileReader();
    var preview =  $('#preview');
    reader.onload = function (e) {
     preview.attr('src', e.target.result);
    }
    if (files.length > 0) {
      reader.readAsDataURL(files[0]);
    }else{
      preview.src = "";
    }
  }

    $scope.addPost = function(post){
    if(!business_id && !event_id){
      PostSrv.addUserPost(post.Files, post.Text).success(function(post_id){
        
        $modalInstance.close(); 
        
      }).error(function(data){
        toastr.error(data);
      })

    }else if(event_id){
      PostSrv.addEventPost(event_id, post.Files, post.Text).success(function(post_id){
          $modalInstance.close(); 
      }).error(function(data){
        toastr.error(data);
      })

    }else if(business_id){
      PostSrv.addBusinessPost(business_id,post.Files, post.Text).success(function(post_id){
          $modalInstance.close(); 
      }).error(function(data){
        toastr.error(data);
      })

    }
  }


}])
