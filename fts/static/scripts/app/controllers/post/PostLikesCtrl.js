app.controller("PostLikesCtrl",
    ["$scope", "$modalInstance",
      "LikeSrv","$location", "post_id",
    function($scope, $modalInstance,
      LikeSrv,$location, post_id){
  $scope.likes = [];

  LikeSrv.getLikes(post_id).success(function(likes){
    $scope.likes = likes;
  })

  $scope.openUser = function(user_id){
    $location.path("/users/profile/"+user_id);
  }


}])
