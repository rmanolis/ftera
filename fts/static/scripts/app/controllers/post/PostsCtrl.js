app.controller("PostsCtrl",
    [ '$scope','PostUISrv','PostSrv','PostChanSrv','LikeSrv',
    function($scope,PostUISrv,PostSrv,PostChanSrv, LikeSrv){
      $scope.numPostPages = 1;
      $scope.hasPostPages = true;
      $scope.postSize = 5;

      $scope.showOwner = PostUISrv.openOwnerUrl;
      $scope.showDate = PostUISrv.showDate;

      function getPosts(){
        PostSrv.getNearPosts(0,$scope.postSize).success(function(posts){
          $scope.posts = [];
          $scope.posts = posts;
          if(posts.length >= $scope.postSize){
            $scope.hasPostPages = true;

          }else{
            $scope.hasPostPages = false;

          }
        })
      }
      $scope.getPosts= getPosts;

      getPosts();
      $scope.morePosts = function(){
        PostSrv.getNearPosts($scope.numPostPages,$scope.postSize).success(function(posts){
          if(posts.length > 0){
            angular.forEach(posts, function(post){
              $scope.posts.push(post);
            })
            $scope.numPostPages += 1;
          }else{
            $scope.hasPostPages = false;
          }
        })
      }
      $scope.addPost = function(){
        var modalInstance = PostSrv.openAddPost(null,null);
        modalInstance.result.then(function () {
        });
      }

      PostChanSrv.onNewPost(function(data){
        PostSrv.getPost(data).success(function(post){
          if ($scope.userId === post.OwnerId){
            post.isOwner=true; 
          }else{
            post.isOwner=false; 
          };

          $scope.posts.unshift(post);
        })
      })

      $scope.showPost = function(post){
        PostSrv.openShowPost(post.Id);
      }

      $scope.deletePost = function(post){
        PostSrv.deletePost(post.Id).success(function(){
          getPosts();
        })
      }

      function countLikes(post){
        LikeSrv.countLikes(post.Id).success(function(data){
          post.Likes = data.Count;
        })
      }

      $scope.like = function(post){
        LikeSrv.addLike(post.Id).success(function(){
          post.MyLike = true;
          countLikes(post);
        }).error(function(data){
          console.log(data);
        })
      }


      $scope.dislike = function(post){
        LikeSrv.removeLike(post.Id).success(function(){
          post.MyLike = false;
          countLikes(post);
        })
      }

      $scope.showLikes = function(post){
        LikeSrv.openLikeList(post.Id);
      }

    }])
