app.controller("CommonPostsCtrl",[
  "$scope","PostSrv","LikeSrv",
  function($scope,PostSrv,LikeSrv){

      $scope.showPost = function(post){
        PostSrv.openShowPost(post.Id);
      }

      $scope.deletePost = function(post){
        PostSrv.deletePost(post.Id).success(function(){
          $scope.getPosts();
        })
      }

      function countLikes(post){
        LikeSrv.countLikes(post.Id).success(function(data){
          post.Likes = data.Count;
        })
      }

      $scope.like = function(post){
        LikeSrv.addLike(post.Id).success(function(){
          post.MyLike = true;
          countLikes(post);
        }).error(function(data){
          console.log(data);
        })
      }


      $scope.dislike = function(post){
        LikeSrv.removeLike(post.Id).success(function(){
          post.MyLike = false;
          countLikes(post);
        })
      }

      $scope.showLikes = function(post){
        LikeSrv.openLikeList(post.Id);
      }
 
  }
])
