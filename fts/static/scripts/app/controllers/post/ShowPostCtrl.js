app.controller("ShowPostCtrl",
    ["$scope","$modalInstance","LikeSrv","PostSrv","post_id",
    function($scope,$modalInstance,LikeSrv,PostSrv,post_id){
  PostSrv.getPost(post_id).success(function(post){
    $scope.post = post;

  })
  $scope.isLiked = false;
  $scope.numLikes = 0;
  function countLikes(){
    LikeSrv.countLikes(post_id).success(function(data){
      $scope.numLikes = data.Count;
    })
  }
  countLikes();
  LikeSrv.getLike(post_id).success(function(){
    $scope.isLiked = true;
  }).error(function(){
    $scope.isLiked = false;
  })
  $scope.like = function(){
    LikeSrv.addLike(post_id).success(function(){
      $scope.isLiked = true;
      countLikes();
    }).error(function(data){
      console.log(data);
    })
  }

  $scope.dislike = function(){
    LikeSrv.removeLike(post_id).success(function(){
      $scope.isLiked = false;
      countLikes();
    })
  }

  $scope.likes = function(){
    LikeSrv.openLikeList(post_id);
  }

}])
