/* it needs $scope.userId and $scope.isOtherUser*/
app.controller("UserPostsCtrl",
    ["$scope","PostSrv","UserSrv",
    function($scope,PostSrv,UserSrv){

      console.log($scope.userId);
      $scope.posts= [];
      $scope.numPostPages = 1;
      $scope.hasPostPages = false; 
      function getPosts(){
        PostSrv.getUserPosts($scope.userId)
          .success(function(posts){
            $scope.posts = posts;
          })
      }
      if($scope.isOtherUser){
        getPosts();  
      }else{
        UserSrv.getProfile()
          .success(function(data){
            $scope.userId = data.Id; 
            getPosts();
          })
      }
      $scope.getPosts= getPosts;


      $scope.showPost = function(post_id){
        PostSrv.openShowPost(post_id);
      }

      $scope.addPost = function(){
        var modelInstance = PostSrv.openAddPost(null,null);
        modelInstance.result.then(function () {
          getPosts(); 
        });
      }

      $scope.morePosts = function(){
        PostSrv.getUserPosts($scope.userId,$scope.numPostPages)
          .success(function(posts){
            if(posts.length > 0){
              $scope.hasPostPages = true;               
              angular.forEach(posts, function(post){
                $scope.posts.push(post);
              })
              $scope.numPostPages += 1;
            }else{
              $scope.hasPostPages = false;
            }
          })
      }



    }])
