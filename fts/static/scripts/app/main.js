

var app = angular.module('App', ['ngRoute','toastr',
    'ui.bootstrap','ngWebSocket',
    'angular-confirm',
    'ngAnimate',
    'ui.bootstrap',
    'easypiechart',
    'ui.tree',
    'ngTagsInput',
    'angular-loading-bar',
    'duScroll',
    'luegg.directives',
    'ngFileUpload',
    'ui.bootstrap.datetimepicker']);
/*
 ,'ui.bootstrap.datetimepicker', 
    'angularFileUpload','ngTagsInput',
    "ngScrollbar",
 */

app.run(
    ["$rootScope","$timeout","$location","AuthSrv", "SocketSrv","UserSrv",
    function($rootScope,$timeout,$location,AuthSrv, SocketSrv,UserSrv){
    
    // enumerate routes that don't need authentication
    SocketSrv.setSession();
    SocketSrv.startWithSocketsOn();
    var routesThatDontRequireAuth = ['/login','/register','/forgot'];
            // check if current location matches route
    var routeClean = function (route) {
        return _.find(routesThatDontRequireAuth,
            function (noAuthRoute) {
                return route === noAuthRoute;
            });
    };

    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        // if route requires auth and user is not logged in
        AuthSrv.auth_user().error(function(){
            if (!routeClean($location.url()) ) {
                $location.path("/login");
            }
        }).success(function(userid){
          $rootScope.userId=userid;
          UserSrv.getIsNew().success(function(data){
           if(data.Is){
              $location.path('/users/edit/info');
            }          
          })
        })
    });
}]);


app.config(["$routeProvider",function ($routeProvider) {
    
    $routeProvider
        .when('/', {
          templateUrl: '/static/views/find_partner.html',
          controller: 'FindPartnerCtrl',
          resolve:{
            userId:function(AuthSrv){
              var d = AuthSrv.auth_user();
              return d;
            }
          }
        })

        .when('/login', {
          templateUrl: '/static/views/login.html',
          controller: 'LoginCtrl',
        })
        
        .when('/register', {
          templateUrl: '/static/views/register.html',
          controller: 'RegisterCtrl'
        })

        .when('/forgot',{
          templateUrl: '/static/views/forgot_password.html',
          controller: 'ForgotPasswordCtrl'
        })


        .when('/find_place',{
          templateUrl: '/static/views/calendar/find_place.html',
          controller: 'FindPlaceCtrl',
        })
        
        
        //User 
        .when('/users/edit/info',{
          templateUrl: '/static/views/user/edit_user_info.html',
          controller:'EditUserInfoCtrl'
        })

        .when('/users/edit/settings',{
          templateUrl: '/static/views/user/edit_user_settings.html',
          controller:'EditUserSettingsCtrl'
        })

        .when('/users/profile',{
          templateUrl: '/static/views/user/profile.html',
          controller: 'ProfileCtrl'
        })
        .when('/users/profile/:id',{
          templateUrl: '/static/views/user/profile.html',
          controller: 'UserCtrl'
        })

        
        
       
        //Business
        .when('/businesses',{
          templateUrl: '/static/views/business/my_businesses.html',
          controller:"MyBusinessCtrl"
        })
        .when('/businesses/add',{
          templateUrl: '/static/views/business/add_edit_business.html',
          controller:"AddEditBusinessCtrl"
        })
        .when('/businesses/edit/:id',{
          templateUrl: '/static/views/business/add_edit_business.html',
          controller:"AddEditBusinessCtrl"
        })

        .when('/businesses/show/:id',{
          templateUrl: '/static/views/business/show_business.html',
          controller:"ShowBusinessCtrl"
        })



        //Event
        .when('/events/add',{
          templateUrl: '/static/views/business/add_edit_event.html',
          controller:"AddEditEventCtrl"
        })
        .when('/events/edit/:event_id',{
          templateUrl: '/static/views/business/add_edit_event.html',
          controller:"AddEditEventCtrl"
        })
        .when('/events/show/:event_id',{
          templateUrl: '/static/views/business/show_event.html',
          controller:"ShowEventCtrl"
        })

        
}]);

