app.factory("localize",
    ['$http', '$rootScope', '$window',
    function($http, $rootScope, $window){
      var obj = {};
      obj.language = '';
      obj.url = null;
      obj.resourceFileLoaded=false;

      obj.successCallback = function(data){
        obj.dictionary = data;
        obj.resourceFileLoaded = true;
        $rootScope.$broadcast('localizeResourcesUpdated');
      }

      obj.setLanguage = function(value){
        obj.language = value.toLowerCase().split("-")[0];
        obj.initLocalizedResources();
      }

      obj.setUrl= function(value){
        obj.url = value;
        obj.initLocalizedResources();
      }

      obj.buildUrl= function(){
            if(!obj.language){
                obj.language = ($window.navigator.userLanguage || $window.navigator.language).toLowerCase(); 
                obj.language = obj.language.split("-")[0];
            }
            return '/static/i18n/resources-locale_' + obj.language + '.js';
      }

      obj.initLocalizedResources= function(){
        var url = obj.url || obj.buildUrl();
        $http( { method: "GET", url: url, cache: false } )
        .success( obj.successCallback )
        .error(function(){
            $rootScope.$broadcast('localizeResourcesUpdated')
        });
      }

    obj.getLocalizedString = function(value){
        var result;

        if (obj.dictionary && value){
            var valueLowerCase = value.toLowerCase();
            if (obj.dictionary[valueLowerCase] == ''){
                result = value;
            }else{
                result = obj.dictionary[valueLowerCase];
            }
        }else{
            result = value;
        }
        return result;
    }

    obj.getLanguage = function(){
      return $http.get("/users/profile/language");
    }
    obj.getFlag = function(lang){
      if(lang === 'English'){
        return 'United-Kingdom';
      }else if(lang === 'Greek'){
        return 'Greece';
      }
    }

    

      return obj;
    }])

app.directive("i18n",
    ['localize',
    function(localize){
      var i18nDirective = {
        restrict:"EA",
        updateText: function(ele,input,placeholder){
          var result = null;
          if (input === 'i18n-placeholder'){
            var result = localize.getLocalizedString(placeholder);
            ele.attr('placeholder', result);
          }else if(input.length >= 1){
            var result = localize.getLocalizedString(input);
            ele.text(result);
          }
        },
        link:function(scope,ele,attrs){
          scope.$on('localizeResourcesUpdated', function(){
            i18nDirective.updateText(ele, attrs.i18n, attrs.placeholder)
          })

          attrs.$observe('i18n', function(value){
            i18nDirective.updateText(ele, value, attrs.placeholder)
          })
        }
      }
      return i18nDirective
    }])

app.controller("LangCtrl",
    ['$scope', 'localize','UserSrv',
    function($scope, localize,UserSrv){
    $scope.lang = 'English';
    $scope.flag = 'United-Kingdom';
    $scope.setLang = function(lang){
      UserSrv.setLanguage(lang);      
      if(lang === 'English'){
        $scope.lang = 'English';
        $scope.flag = 'United-Kingdom';
        return localize.setLanguage('EN-US');
      }else if(lang === 'Greek'){
        $scope.lang = 'Greek';
        $scope.flag = 'Greece';
        return localize.setLanguage('EL-GR');
      }
    }
    localize.getLanguage().success(function(lang){
      if(lang){
        $scope.setLang(lang);
      }
    })


    $scope.getFlag = function(){
      var lang = $scope.lang;
      if(lang === 'English'){
        return 'United-Kingdom';
      }else if(lang === 'Greek'){
        return 'Greece';
      }
    }


}])



