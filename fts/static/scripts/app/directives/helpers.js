app.directive('imgHolder', [ 
    function(){
      return {
        restrict: 'A',
        link: function(scope, ele, attrs){
          Holder.run(
              { images: ele[0]}
              )
        }
      }
    }
])


app.directive('customPage', function(){
  return {
    restrict: "A",
    controller: [
      '$scope', '$element', '$location',
      function($scope, $element, $location){
        var path = function(){
          return $location.path();
        }

        var addBg = function(path){
          $element.removeClass('body-wide body-err body-lock body-auth');

          if(path == '/404' || path == '/pages/404' || path == '/pages/500'){
            $element.addClass('body-wide body-err');
          }
          if(path == '/pages/signin' || path== '/pages/signup' || path == '/pages/forgot-password'){
            $element.addClass('body-wide body-auth');
          }
          if(path == '/pages/lock-screen') {
            $element.addClass('body-wide body-lock');
          }
        }

        addBg( $location.path() );

        $scope.$watch(path, function(newVal, oldVal){
          if(newVal == oldVal){
            return;
          }
          addBg($location.path());
        })
      }]
  }
})

app.directive('uiColorSwitch', [
    function(){
      return {
        restrict: 'A',
        link: function(scope, ele, attrs){
          ele.find('.color-option').on('click', function(event){
            var $this = $(this);
            var hrefUrl;

            var style = $this.data('style')
              if(style == 'loulou'){
                hrefUrl = 'static/styles/main.css';
                $('link[href^="static/styles/main"]').attr('href',hrefUrl);
              }else if(style){
                style = '-' + style;
                hrefUrl = 'styles/main' + style + '.css';
                $('link[href^="styles/main"]').attr('href',hrefUrl);
              } else{
                return false;
              }

            event.preventDefault();
          })
        }
      }
    }])


app.directive('goBack', [ 
    function(){
      return {
        restrict: "A",
        controller: [
          '$scope', '$element', '$window',
          function($scope, $element, $window){
            $element.on('click', function(){
              $window.history.back();
            }
            )
          }
        ]
      }
    }])

