app.directive('toggleNavCollapsedMin',
    ['$rootScope',
    function($rootScope){
      return {
        restrict: 'A',
        link: function(scope, ele, attrs){
          var app = $('#app');

          ele.on('click', function(e){
            if(app.hasClass('nav-collapsed-min')){
              app.removeClass('nav-collapsed-min');
            }else{
              app.addClass('nav-collapsed-min');
              $rootScope.$broadcast('nav:reset');
            }
            e.preventDefault();
          })
        }
      }

    }]);


app.directive('collapseNav',[
    function(){
      return {
        restrict: 'A',
        link: function(scope, ele, attrs){
          var $window = $(window);
          var $lists = ele.find('ul').parent('li'); 
          $lists.append('<i class="ti-angle-down icon-has-ul-h"></i><i class="ti-angle-double-right icon-has-ul"></i>')
            var $a = $lists.children('a');
          var $listsRest = ele.children('li').not($lists);
          var $aRest = $listsRest.children('a');

          var $app = $('#app');
          var $nav = $('#nav-container');

          $a.on('click', function(event){

            if ($app.hasClass('nav-collapsed-min') ||
                ($nav.hasClass('nav-horizontal') && $window.width() >= 768) ){
              return false;
            }

            var $this = $(this);
            var $parent = $this.parent('li');
            $lists.not( $parent ).removeClass('open').find('ul').slideUp();
            $parent.toggleClass('open').find('ul').stop().slideToggle()

              event.preventDefault();
          });

          $aRest.on('click', function(event){
            $lists.removeClass('open').find('ul').slideUp();
          });

          scope.$on('nav:reset', function(event){
            $lists.removeClass('open').find('ul').slideUp();
          })


          var Timer;
          var prevWidth = $window.width();
          var updateClass = function(){
            var currentWidth = $window.width();

            if(currentWidth < 768){
              $app.removeClass('nav-collapsed-min');
            }
            if (prevWidth < 768 && currentWidth >= 768 && $nav.hasClass('nav-horizontal')){
              $lists.removeClass('open').find('ul').slideUp();
            }

            prevWidth = currentWidth;
          }


          $window.resize( function(){
            var t = setTimeout(updateClass, 300);
            clearTimeout(t);
            
          })
        }
      }
    }
])

app.directive('highlightActive', [ 
    function(){
      return {
        restrict: "A",
        controller: [
          '$scope', '$element', '$attrs', '$location',
          function($scope, $element, $attrs, $location){
            var links = $element.find('a');
            var path = function(){
              return $location.path();
            }
            var highlightActive = function(links, path){
              path = '#' + path;

              angular.forEach(links, function(link){
                var  $link = angular.element(link);
                var $li = $link.parent('li');
                var href = $link.attr('href');

                if ($li.hasClass('active')){
                  $li.removeClass('active');
                }
                if (path.indexOf(href) == 0){
                  $li.addClass('active');
                }
              });
            }

            highlightActive(links, $location.path());

            $scope.$watch(path, function(newVal, oldVal){
              if(newVal === oldVal){
                return;
              }
              highlightActive(links, $location.path());
            })
          }]

      }
    }])


app.directive('toggleOffCanvas', [function(){
  return {
    restrict: 'A',
    link: function(scope, ele, attrs){
      ele.on('click',function(){
        $('#app').toggleClass('on-canvas');
      })
    }
  }
}])
