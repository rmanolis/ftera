app.factory('AuthSrv',
    ["$http",
    function($http){
  var obj = {};

  obj.login = function(data){
    return $http.post('/auth/login', data);
  }


  obj.register = function(data){
    return $http.post('/auth/register', data);
  }

  obj.forgotPassword = function(email){
    return $http.post('/auth/forgot',email);
  }

 
  obj.auth_user = function(){
    return $http.get('/auth/user');
  }

  obj.disconnect = function(){
    return $http.get('/auth/disconnect');
  }

  return obj;

}])
