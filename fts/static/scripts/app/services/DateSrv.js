app.factory("DateSrv", 
    [function(){
  var obj = {};
  
  obj.format = 'dd/MM/yyyy';
  obj.formatWithTime = 'dd/MM/yyyy HH:mm';
  obj.momFormat= "DD/MM/YYYY HH:mm";
  obj.formatDate = function(date,format){
    return moment(date).format(format);
  }
  obj.dateOptions = {
    'year-format': "yyyy",
    'starting-day': 1
  };
  obj.openCalendar = function($event ,  model_name) {
        var self= this;
        $event.preventDefault();
        $event.stopPropagation();
        if(self[model_name]){
          self[model_name] = false;
        }else{
          self[model_name] = true;
        }
  };
  obj.disabled = function (date, mode) {
    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  };
  

  return obj;
}])
