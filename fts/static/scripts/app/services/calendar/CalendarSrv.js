app.factory("CalendarSrv", 
    ["$http","$modal","CommonSrv",
    function($http,$modal,CommonSrv){
  var obj = {};

  obj.search= function(query){
    return $http.post("/calendar/search", query);
  }

  obj.searchBusinesses = function(query,page,size){
    return $http.post("/calendar/search/businesses" + 
        CommonSrv.getPageSize(page,size), query);
  }

  obj.searchEvents = function(query,page,size){
    return $http.post("/calendar/search/events" + 
        CommonSrv.getPageSize(page,size), query);

  }

  obj.invite= function(invitation){
    return $http.post("/calendar/invite", invitation);
  }

  obj.getInvitations= function(){
    return $http.get("/invitations")
  }

  obj.getInvitation = function(inv_id){
    return $http.get("/invitations/"+inv_id);
  }

  obj.acceptInvitation = function(inv_id){
    return $http.put("/invitations/"+inv_id +"/accept");
  }

   obj.openCalendar = function(other_id){ 
    var modalInstance = $modal.open({
      templateUrl: '/static/views/calendar/calendar.html',
      controller: 'CalendarCtrl',
      size: 'lg',
      resolve: {
        other_id: function () {
          return other_id;
        }
      }
    });

    modalInstance.result.then(function () {
    }, function () {
    });
  }


  return obj;
}])
