app.factory("InvitationSrv", 
    ["$http","$modal",
    function($http,$modal){
  var obj ={};
  
  obj.getToInvitations = function(){
    return $http.get("/to/invitations");
  }

  obj.getFromInvitations = function(){
    return $http.get("/from/invitations");
  }


  obj.sendInvitation = function(inv){
    return $http.post("/invitations",inv);
  }

  obj.getInvitation = function(inv_id){
    return $http.get("/invitations/"+inv_id);
  }

  obj.acceptInvitation = function(inv_id){
    return $http.put("/invitations/"+inv_id+"/accept");
  }

  obj.declineInvitation = function(inv_id){
    return $http.put("/invitations/"+inv_id+"/decline");
  }

  obj.openInvitation = function(inv_id){
   var modalInstance = $modal.open({
      templateUrl: '/static/views/calendar/invitation.html',
      controller: 'InvitationCtrl',
      size: 'sm',
      resolve: {
        invitation_id: function () {
          return inv_id;
        }
      }
    });

    modalInstance.result.then(function () {
          }, function () {   
    });
 
  }

  return obj;
}])
