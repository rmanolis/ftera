app.factory('SocketSrv', 
    ["$http","toastr","WebsocketSrv", "InvitationSrv", "AuthSrv",
    function ($http,toastr,WebsocketSrv, InvitationSrv, AuthSrv) {
  var obj ={};
  //TODO this may be exploited
  function onMySession() {
    WebsocketSrv.toChannel("sendMySession","");
    WebsocketSrv.onChannel("mySession",function(msg){
      console.log("your session is " + msg);
      return $http.put("/users/profile/session", 
          {SessionId:msg})
    });
  }

  function onConnection(){
    WebsocketSrv.onOpen(onMySession);
  }

  function onDisconnection(){
    WebsocketSrv.onClose(function(){
      AuthSrv.disconnect();
    });
  }


  function onInvited(){
    WebsocketSrv.onChannel("invited",function(data){
      var invitation = JSON.parse(data);
      InvitationSrv.openInvitation(invitation.Id);  
    })
  }

  function onInvitationAccepted(){
    WebsocketSrv.onChannel("invitation-accepted",function(d){
      var data = JSON.parse(d);
      toastr.info(data.ToUserName + " accepted the invitation to " +
          data.WhereName + " at " + data.Date);
    })
  }

  function onInvitationDeclined(){
    WebsocketSrv.onChannel("invitation-declined",function(d){
      var data = JSON.parse(d);
      toastr.warning(data.ToUserName + " declined the invitation to " +
          data.WhereName + " at " + data.Date);
    })
  }

  function onNewAttendance(){
    WebsocketSrv.onChannel("new-attendance", function(d){
      var data = JSON.parse(d);
      toastr.info(data.UserName + " will also go to the event  " +
          data.EventName);

    });
  }

  function onNewLike(){
    WebsocketSrv.onChannel("new-like", function(d){
      var data = JSON.parse(d);
      toastr.info(data.UserName + " liked a post of yours  ");
    });
  }



  obj.startWithSocketsOn = function(){

    onConnection();
    onDisconnection();

    onInvited();
    onInvitationAccepted();
    onInvitationDeclined();

    onNewAttendance();
    onNewLike();
  }

  obj.setSession = function(){
    onMySession();
  }


  return obj;
}])
