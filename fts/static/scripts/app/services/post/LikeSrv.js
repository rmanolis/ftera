app.factory("LikeSrv", 
    ["$http","$modal",
    function($http, $modal){

  var obj = {};

  obj.getLikes = function(post_id){
    return $http.get("/posts/"+post_id+"/likes");
  }

  obj.getLike = function(post_id){
    return $http.get("/posts/"+post_id+"/one/like");
  }
  obj.countLikes = function(post_id){
    return $http.get("/posts/"+post_id+"/count/likes");
  }

  obj.addLike = function(post_id){
    return $http.post("/posts/"+post_id+"/likes");
  }

  obj.removeLike = function(post_id){
    return $http.delete("/posts/" + post_id + "/likes");
  }

  obj.openLikeList = function(post_id){ 
    return $modal.open({
      templateUrl: '/static/views/post/post_likes.html',
      controller: 'PostLikesCtrl',
      size: 'sm',
      resolve: {
        post_id: function () {
          return post_id;
        }
      }
    });
 } 

  return obj;
}])
