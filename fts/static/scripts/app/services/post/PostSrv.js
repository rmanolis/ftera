app.factory("PostSrv",
    ["$http","$modal","Upload","CommonSrv",
    function($http, $modal, Upload, CommonSrv){
  var obj = {};
  obj.addUserPost = function(files, text){
      return Upload.upload({
            url: "/users/profile/posts",
            fields:{Text:text},
            file: files
          });
  }

  obj.addEventPost = function(event_id,files, text){
     return Upload.upload({
            url: "/events/"+event_id +"/posts",
            fields:{Text:text},
            file: files
          });

  }

  obj.addBusinessPost = function(business_id,files, text){
     return Upload.upload({
            url:"/businesses/"+business_id +"/posts",
            fields:{Text:text},
            file: files
          });

    }
  
  

  obj.deletePost = function(id){
    return $http.delete("/posts/"+id);
  }

  obj.getPost = function(id){
    return $http.get("/posts/"+id);
  }
  obj.getNearPosts = function(page,size){
    return $http.get("/posts" + CommonSrv.getPageSize(page,size));
  }
  obj.isMyPost = function(id){
    return $http.get("/posts/"+id +"/is");
  }

 obj.openAddPost= function(business_id,event_id){ 
    return $modal.open({
      templateUrl: '/static/views/post/add_post.html',
      controller: 'AddPostCtrl',
      size: 'sm',
      resolve: {
        business_id: function () {
          return business_id;
        },
        event_id: function () {
          return event_id;
        },

      }
    });
    
 }

 obj.openShowPost = function(post_id){
  return $modal.open({
      templateUrl: '/static/views/post/show_post.html',
      controller: 'ShowPostCtrl',
      size: 'md',
      resolve: {
        post_id: function () {
          return post_id;
        }
      }
    });

 }



  obj.getUserPosts = function(user_id,page,size){
    return $http.get("/users/other/"+user_id +"/posts"+ CommonSrv.getPageSize(page,size));

  }

  obj.getEventPosts = function(id,page,size){
    return $http.get("/events/"+id+"/posts"+ CommonSrv.getPageSize(page,size));

  }

  obj.getBusinessPosts = function(id,page,size){
    return $http.get("/businesses/"+id+"/posts"+ CommonSrv.getPageSize(page,size));

  }

 
  return obj;
}])
