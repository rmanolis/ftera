app.factory("PostUISrv",
    ["$location","BusinessSrv","EventSrv",
    function($location,BusinessSrv,EventSrv){
  var obj ={};
  
  obj.openOwnerUrl = function(post){
    if(post.UserId){
      $location.path("/users/profile/" + post.UserId)
    }else if(post.EventId){
      EventSrv.openEvent(post.EventId);
    }else if(post.BusinessId){
      BusinessSrv.openBusiness(post.BusinessId);
    }
  }
  
  obj.showDate = function(post){
    var date = new Date(post.Date)
    var v = moment(post.Date).format("DD/MM hh:mm")
    return v;
  }

  return obj;
}])
