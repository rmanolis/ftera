app.factory("TagSrv", 
    ["$http",
    function($http){
  var obj ={};
  obj.TagTypes = {
    "MusicType":0,
    "StuffType":1,
    "BusinessType":2,
    "EventType":3,
    "ProductType":4,
    "Product":5,
  }
  obj.searchTags = function(name,type){
    return $http.post("/tags/search", { Search:name, Type:type});
  }
  return obj;
}])
