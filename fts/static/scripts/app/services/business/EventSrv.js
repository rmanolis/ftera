app.factory("EventSrv", 
    ["$http","$modal",
    function($http, $modal){
  var obj = {};
  
  obj.getEvents = function(){
    return $http.get("/users/profile/events");
  }

  obj.getEvent = function( event_id){
    return $http.get("/users/profile/events/"+event_id);
  }

  obj.addEvent = function( event){
    return $http.post("/users/profile/events", event);
  }

  obj.editEvent = function(event_id, event){
    return $http.put("/users/profile/events/"+event_id,event);
  }

  obj.deleteEvent = function(event_id){
    return $http.delete("/users/profile/events/"+event_id);
  }

  obj.showEvent = function( event_id){
    return $http.get("/events/"+event_id );
  }

  obj.getStrDate = function(event){
    return moment(event.StartDate).format("DD/MM/YYYY HH:mm");
  }

  obj.openEvent = function(event_id){ 
    var modalInstance = $modal.open({
      templateUrl: '/static/views/business/show_event.html',
      controller: 'ShowEventCtrl',
      size: 'lg',
      resolve: {
        event_id: function () {
          return event_id;
        }
      }
    });

    modalInstance.result.then(function () {
    }, function () {
    });
  }


  return obj;
}]);
