app.factory("BusinessSrv",
    ["$http","$modal",
    function($http,$modal){
  var obj = {};
  
  obj.getBusinesses = function(){
    return $http.get("/users/profile/businesses");
  }

  obj.addBusiness = function(business){
    return $http.post("/users/profile/businesses", business);
  }

  obj.editBusiness = function(business_id,business){
    return $http.put("/users/profile/businesses/"+business_id, business);
  }

  obj.deleteBusiness = function(business_id){
    return $http.delete("/users/profile/businesses/"+business_id);
  }


  obj.getBusiness = function(business_id){
    return $http.get("/users/profile/businesses/"+business_id);
  }
 
  obj.showBusiness = function(business_id){
    return $http.get("/businesses/"+business_id);
  }

  obj.getDays = function(){
    return $http.get("/days");
  }


  obj.getStrTime = function(business){
    if(business.Duration){
    var str_time = moment(business.Duration.StartTime).format("HH:mm");
    var end_time = moment(business.Duration.EndTime).format("HH:mm");
    return str_time + "-" + end_time;
    }
  }

  obj.openBusiness = function(business_id){ 
    var modalInstance = $modal.open({
      templateUrl: '/static/views/business/show_business.html',
      controller: 'ShowBusinessCtrl',
      size: 'lg',
      resolve: {
        business_id: function () {
          return business_id;
        }
      }
    });

    modalInstance.result.then(function () {
    }, function () {
    });
  }



  return obj;
}])
