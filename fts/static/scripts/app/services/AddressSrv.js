app.factory("AddressSrv", 
    ["$http","toastr",
    function($http, toastr){
  var obj={};
  
  obj.geocoder = new google.maps.Geocoder();
  

  obj.findAddress = function(address, cb) {
    var self=this;
    self.geocoder.geocode( { 'address':address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var geo = results[0].geometry.location;       
        var lat = geo.A;
        var lng = geo.F;
        var geolocation =  [lng, lat];
        cb(geolocation);
      } else {
        toastr.warning("Add correct address to save the geolocation");
      }
    });
  }

  obj.getAutocompletion = function(address) {
    return $http.get('https://maps.googleapis.com/maps/api/geocode/json', {
      params: {
        address: address,
        sensor: false
      }
    }).then(function(response){
      return response.data.results.map(function(item){
        return item.formatted_address;
      });
    });
  };



  return obj;
}])
