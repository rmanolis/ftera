app.factory('UserSrv', 
    ["$http",
    function($http){
  var obj = {};

 
  obj.getProfile = function(){
    return $http.get('/users/profile');
  }

  obj.getProfileId = function(){
    return $http.get('/users/profile/id');
  }

  obj.getUser = function(id){
    return $http.get('/users/other/'+id);
  }

  obj.deleteUser = function(){
    return $http.delete('/users/profile/delete');
  }

  obj.deletePhoto = function(){
    return $http.delete('/users/profile/photo');
  }

  obj.getUserInfo = function(id){
      return $http.get('/users/other/'+id+"/info");
  }

  obj.getProfileInfo = function(){
    return $http.get('/users/profile/info');
  }

  obj.setSettings = function(data){
    return $http.put('/users/profile/settings', data);
  }

  obj.getPreferences = function(){
    return $http.get('/users/profile/preferences');
  }

  obj.setPreferences = function(data){
    return $http.put('/users/profile/preferences',data);
  }


  obj.searchUsers = function(size, page){
    return $http.put('/users/search',{'page':page,'size':size});
  }


  obj.setProfileInfo = function(data){
    return $http.put('/users/profile/info', data);
  }

  obj.setLanguage = function(lang){
    return $http.post('/users/profile/language',lang)
  }

  obj.getSexList = function(){
    return $http.get('/sexes');
  }

  
  obj.getSearchOptions = function(){
    return $http.get('/search/options');
  }

  obj.getIsBusiness = function(){
    return $http.get('/users/profile/isbusiness');
  }

  obj.getIsNew = function(){
    return $http.get('/users/profile/isnew');
  }



  obj.blockUser = function(user_id){
    return $http.post('/users/profile/blocks/'+user_id);
  }

  obj.isBlockingUser = function(user_id){
    return $http.get('/users/profile/blocks/'+user_id);
  }

  obj.isBlockedByUser = function(user_id){
    return $http.get('/users/profile/blocks_by/'+user_id);
  }

  obj.unblockUser = function(user_id){
    return $http.put('/users/profile/unblock/' + user_id);
  }

  obj.getBlockedUsers = function(){
    return $http.get('/users/profile/blocks');
  }

  obj.getBlockedByUsers = function(){
    return $http.get('/users/profile/blocks_by');
  }

  obj.logout = function(){
    return $http.get("/auth/logout");
  }

  
  return obj;

}])
