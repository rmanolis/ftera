app.factory("ReportSrv",
    ["$http","$modal",
    function($http, $modal){
  var obj = {};

  obj.reportUser = function(user_id, type, description){
    return $http.post('/report/users/'+user_id, {
      Type: type, Description: description
    })
  }

  obj.getTypes = function(){
    return $http.get('/report/types');
  }


  obj.openReport = function(user_id){
    
    var modalInstance = $modal.open({
      templateUrl: '/static/views/report.html',
      controller: 'ReportCtrl',
      size: 'lg',
      resolve: {
        user_id: function () {
          return user_id;
        }
      }
    });

    modalInstance.result.then(function (report) {
      obj.reportUser(user_id, report.Type,report.Description).success(function(){
        console.log("Reported successfully");
      }).error(function(data){
        console.log(data)
      })
    }, function () {
      //Do nothing after cancel     
    });
  }

  return obj;
}])
