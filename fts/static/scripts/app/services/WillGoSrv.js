app.factory("WillGoSrv",
    ["$http", "$modal",
    function($http, $modal){
  var obj = {};

  obj.getAttentions = function(event_id){
    return $http.get("/events/"+event_id+"/attentions");
  }

  obj.getAttention = function(event_id){
    return $http.get("/events/"+event_id+"/one/attention");
  }


  obj.countAttentions = function(event_id){
    return $http.get("/events/"+event_id+"/count/attentions");
  }

  obj.addAttention = function(event_id){
    return $http.post("/events/"+event_id+"/attentions");
  }

  obj.removeAttention = function(event_id){
    return $http.delete("/events/" + event_id + "/attentions");
  }

  obj.openAttentionList = function(event_id){ 
    return $modal.open({
      templateUrl: '/static/views/willgo/show_attentions.html',
      controller: 'ShowAttentionsCtrl',
      size: 'sm',
      resolve: {
        event_id: function () {
          return event_id;
        }
      }
    });
 } 

  return obj;
}])
