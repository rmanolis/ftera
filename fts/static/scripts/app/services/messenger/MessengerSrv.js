app.factory('MessengerSrv', 
    ["$http","$modal",
    function($http,$modal){
  var obj = {};
  

  /*
   * Returns {messenger_id, user_id, photo_id, name}
   */
  obj.getMessengers = function(){
   return $http.get('/messengers');
  }

  obj.getMessages = function(messenger_id, page, size){
    var url = '/messengers/'+messenger_id+'/messages?';
    if(size){
      url += "size=" + size +"&";
    }
    if(page){
      url += "page=" + page +"&";
    }
    return $http.get(url);
  }

  obj.countMessages = function(messenger_id){
    return $http.get("/messengers/"+messenger_id + "/count/messages");
  }

  obj.sendMessage = function(messenger_id, message){
    console.log(message);
    return $http.post('/messages',
        {MessengerId:messenger_id, Message:message});
  }

  obj.createMessenger = function(user_id){
   return $http.post('/messengers/users/'+user_id);
  }

  obj.ignoreMessenger = function(messenger_id){
    return $http.post('/messengers/'+messenger_id+'/ignore');
  }

  obj.blockMessenger = function(messenger_id){
   return $http.put('/messengers/'+messenger_id+'/block');
  }

   obj.openMessenger = function(user_id){ 
    var modalInstance = $modal.open({
      templateUrl: '/static/views/messenger/messenger.html',
      controller: 'MessengerCtrl',
      size: 'lg',
      resolve: {
        user_id: function () {
          return user_id;
        }
      }
    });

    return modalInstance;
   }



 
  return obj;

}])
