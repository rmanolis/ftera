app.factory("MessengerChanSrv",
    ["WebsocketSrv",
    function(WebsocketSrv){

  var obj = {};
  
  obj.onMessage = function(messenger_id,cb){
    WebsocketSrv.onChannel(messenger_id,function(msg){
      cb(msg);
    })
  }

  obj.onNewMessage = function(cb){
    WebsocketSrv.onChannel("new-message",function(msg){
      cb(msg);
    })
  }

  obj.onReadMessage = function(cb){
    WebsocketSrv.onChannel("read-message",function(msgrId){
      cb(msgrId);
    })
  }

  obj.readMessage = function(messengerId){
    WebsocketSrv.toChannel("readMessage",messengerId);
  }




  return obj;
}])
