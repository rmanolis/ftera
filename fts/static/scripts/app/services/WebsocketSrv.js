app.factory("WebsocketSrv",
    ["$websocket","toastr",
    function($websocket,toastr){
  var link = "wss://pullmeout.com/ws";
  //var link = "ws://localhost:3001/ws";
  
  var ws = $websocket(link);

       
  var obj = {};
  obj.onChannel = function(channel,cb){
    ws.onMessage(function(evt) {
      var msg = JSON.parse(evt.data); 
      if(msg.channel == channel){
        cb(msg.message)
      }
    })
  };

  obj.onClose = function(cb){
    ws.onClose(function(evt){
      cb();
    })
  }

  obj.onOpen = function(cb){
    ws.onOpen(function(evt){
      cb();
    })
  }


  obj.toChannel = function(channel,message){
    ws.send(JSON.stringify({channel: channel, 
      message:message }));
  }

  return obj;
}])
