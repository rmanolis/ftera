package ftsctrls

import (
	"ftera/fts/ctrls/attention"
	"ftera/fts/ctrls/business"
	"ftera/fts/ctrls/inviter"
	"ftera/fts/ctrls/like"
	"ftera/fts/ctrls/messenger"
	"ftera/fts/ctrls/post"
	"ftera/fts/ctrls/user"
	"ftera/fts/utils"
	"log"

	"net/http"

	"github.com/gorilla/mux"
)

func LoadRoutes() *mux.Router {
	index := Index{}
	auth := Auth{}
	router := mux.NewRouter()
	router.HandleFunc("/", index.Welcome).Methods("GET")
	router.HandleFunc("/git", func(w http.ResponseWriter, r *http.Request) {
		log.Println("Got a git")
	}).Methods("POST")
	router.HandleFunc("/sitemap", ftsutils.AuthHandler(index.Sitemap))

	a := router.PathPrefix("/auth").Subrouter()
	a.HandleFunc("/login", auth.Login).Methods("POST")
	a.HandleFunc("/logout", ftsutils.AuthHandler(auth.Logout)).Methods("GET")
	a.HandleFunc("/user", auth.IsUser).Methods("GET")
	a.HandleFunc("/token/{id}", auth.ValidateToken).Methods("GET")
	a.HandleFunc("/register", auth.Register).Methods("POST")
	a.HandleFunc("/forgot", auth.ForgotPassword).Methods("POST")
	ctrluser.Routes(router)
	ctrlmessenger.Routes(router)
	ctrlbusiness.Routes(router)
	ctrlinviter.Routes(router)
	ctrlpost.Routes(router)
	ctrlattention.Routes(router)
	ctrllike.Routes(router)
	return router
}
