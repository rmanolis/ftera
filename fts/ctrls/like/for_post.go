package ctrllike

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/models/enums"

	"ftera/fts/utils"
	"ftera/fts/ws/senders"

	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func AddLike(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	post_id := vars["post_id"]
	if !bson.IsObjectIdHex(post_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	post, err := ftsmodels.GetPost(db, bson.ObjectIdHex(post_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	is_exists := user.IsLikeExists(db, post.Id)
	if is_exists {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	al, err := user.AddLike(db, post.Id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	owner_user, err := ftsmodels.GetUserById(db, post.OwnerId.Hex())
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	ftswsenders.UserLikedPost(db, user, owner_user, post.Id)
	w.Write([]byte(al.Id))
}

func GetLike(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	post_id := vars["post_id"]
	if !bson.IsObjectIdHex(post_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	post, err := ftsmodels.GetPost(db, bson.ObjectIdHex(post_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	al, err := post.Like(db, user.Id)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	out, _ := json.Marshal(al)
	w.Write(out)
}

type LikeJson struct {
	UserId  string
	Name    string
	Sex     string
	PhotoId string
	PostId  string
}

func GetLikes(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	post_id := vars["post_id"]
	if !bson.IsObjectIdHex(post_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	post, err := ftsmodels.GetPost(db, bson.ObjectIdHex(post_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	likes, _ := post.Likes(db)
	aj_ls := []LikeJson{}
	for _, v := range likes {
		aj := LikeJson{}
		other, err := ftsmodels.GetUserById(db, v.UserId.Hex())
		if err == nil {
			aj.Name = other.Name
			aj.UserId = other.Id.Hex()
			aj.PhotoId = other.Info.PhotoId.Hex()
			aj.Sex = enums.SexList[other.Info.Sex]
			aj.PostId = v.PostId.Hex()
			aj_ls = append(aj_ls, aj)
		}
	}
	out, _ := json.Marshal(aj_ls)
	w.Write(out)

}

func CountLikes(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	post_id := vars["post_id"]
	if !bson.IsObjectIdHex(post_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	post, err := ftsmodels.GetPost(db, bson.ObjectIdHex(post_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	n := post.CountLikes(db)
	out, _ := json.Marshal(map[string]int{"Count": n})
	w.Write(out)

}

func RemoveLike(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	post_id := vars["post_id"]
	if !bson.IsObjectIdHex(post_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	post, err := ftsmodels.GetPost(db, bson.ObjectIdHex(post_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	at, err := post.Like(db, user.Id)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	err = at.Remove(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)

}
