package ctrllike

import (
	"ftera/fts/utils"

	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	router.HandleFunc("/posts/{post_id}/likes",
		ftsutils.AuthHandler(AddLike)).Methods("POST")
	router.HandleFunc("/posts/{post_id}/likes",
		ftsutils.AuthHandler(GetLikes)).Methods("GET")
	router.HandleFunc("/posts/{post_id}/count/likes",
		ftsutils.AuthHandler(CountLikes)).Methods("GET")
	router.HandleFunc("/posts/{post_id}/one/like",
		ftsutils.AuthHandler(GetLike)).Methods("GET")
	router.HandleFunc("/posts/{post_id}/likes",
		ftsutils.AuthHandler(RemoveLike)).Methods("DELETE")
}
