package ftsctrls

import (
	"encoding/json"
	"ftera/fts/models"
	"net/http"
)

type Index struct{}
type UrlLocation struct {
	Url   string
	Title string
	Icon  string
}

func (i Index) Welcome(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, "static/index.html")
}

func (i Index) Sitemap(w http.ResponseWriter, req *http.Request) {
	sitemap := []UrlLocation{
		UrlLocation{Url: "#/",
			Title: "Find Partner",
			Icon:  "ti-home"},
		UrlLocation{Url: "#/users/profile",
			Title: "Profile",
			Icon:  "ti-user"},
		UrlLocation{Url: "#/users/edit/settings",
			Title: "Settings",
			Icon:  "ti-settings"},
		UrlLocation{Url: "#/find_place",
			Title: "Find Place",
			Icon:  "ti-search"},
	}
	user, err := ftsmodels.LoadUser(req)

	if err == nil && user.IsBusiness {
		sitemap = append(sitemap, UrlLocation{Url: "#/businesses",
			Title: "My Businesses",
			Icon:  "ti-clipboard"})
	}

	sitemap = append(sitemap, UrlLocation{Url: "auth/logout",
		Title: "Log out",
		Icon:  "ti-export"})

	js, _ := json.Marshal(sitemap)
	w.Write(js)
}
