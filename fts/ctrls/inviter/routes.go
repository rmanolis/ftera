package ctrlinviter

import (
	"ftera/fts/utils"

	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {

	router.HandleFunc("/calendar/search/businesses",
		SearchBusinesses).Methods("POST")
	router.HandleFunc("/calendar/search/events",
		SearchEvents).Methods("POST")
	router.HandleFunc("/invitations",
		ftsutils.AuthHandler(InviteUser)).Methods("POST")

	router.HandleFunc("/to/invitations",
		ftsutils.AuthHandler(GetToInvitations)).Methods("GET")
	router.HandleFunc("/from/invitations",
		ftsutils.AuthHandler(GetFromInvitations)).Methods("GET")

	router.HandleFunc("/invitations/{id}/accept",
		ftsutils.AuthHandler(AcceptInvitation)).Methods("PUT")

	router.HandleFunc("/invitations/{id}/decline",
		ftsutils.AuthHandler(DeclineInvitation)).Methods("PUT")

	router.HandleFunc("/invitations/{id}",
		ftsutils.AuthHandler(GetInvitation)).Methods("GET")

}
