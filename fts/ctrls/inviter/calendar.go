package ctrlinviter

import (
	"encoding/json"
	"ftera/fts/logics/inviter"
	"ftera/fts/models"
	"ftera/fts/utils"
	"ftera/fts/ws/senders"
	"ftera/ftutils"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func SearchEvents(w http.ResponseWriter, r *http.Request) {
	cb := new(logicinviter.CalendarBody)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&cb)
	if err != nil {
		log.Println("Error: " + err.Error())
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	db := ftsutils.GetDB(r)

	page, size := ftutils.PageSize(r)
	events, err := logicinviter.SearchEvents(db, cb, page, size)
	if err != nil {
		log.Println("Error: " + err.Error())
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	out, _ := json.Marshal(events)
	w.Write(out)

}

func SearchBusinesses(w http.ResponseWriter, r *http.Request) {
	cb := new(logicinviter.CalendarBody)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&cb)
	if err != nil {
		log.Println("Error: " + err.Error())
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	db := ftsutils.GetDB(r)
	page, size := ftutils.PageSize(r)
	businesses, err := logicinviter.SearchBusinesses(db, cb, page, size)
	if err != nil {
		log.Println("Error: " + err.Error())
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	out, _ := json.Marshal(businesses)
	w.Write(out)

}

type InvitationBody struct {
	Date       time.Time
	EventId    string
	BusinessId string
	ToUserId   string
}

func InviteUser(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	ib := new(InvitationBody)
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&ib)

	log.Println(ib)
	inv := new(ftsmodels.Invitation)
	if bson.IsObjectIdHex(ib.EventId) {
		var id = bson.ObjectIdHex(ib.EventId)
		inv.EventId = &id
	}

	if bson.IsObjectIdHex(ib.BusinessId) {
		var id = bson.ObjectIdHex(ib.BusinessId)
		inv.BusinessId = &id
	}

	inv.Date = ib.Date

	user, _ := ftsmodels.LoadUser(r)

	if !bson.IsObjectIdHex(ib.ToUserId) {
		http.Error(w, "the other user does not exist", http.StatusNotAcceptable)
		return
	}
	other, err := ftsmodels.GetUserById(db, ib.ToUserId)
	if err != nil {
		http.Error(w, "the other user does not exist", http.StatusNotAcceptable)
		return
	}

	err = ftsmodels.ValidateInvitation.CheckMessenger(db, user, other)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = inv.Insert(db, user.Id, other.Id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	out, _ := json.Marshal(map[string]string{
		"InvitationId": inv.Id.Hex()})
	ftswsenders.InviteUser(other, inv)
	w.Write(out)
}

func AcceptInvitation(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	inv_id := vars["id"]
	if !bson.IsObjectIdHex(inv_id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	user, _ := ftsmodels.LoadUser(r)
	inv, err := ftsmodels.GetInvitation(db, bson.ObjectIdHex(inv_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	if inv.ToUserId != user.Id {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	inv.IsAccepted = true
	err = inv.Update(db)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	from_user, err := ftsmodels.GetUserById(db, inv.FromUserId.Hex())
	if err == nil {
		ftswsenders.ReplyInvitation(from_user, user, inv.Name(db), inv.Date, true)
	}

	w.WriteHeader(http.StatusAccepted)
}

func DeclineInvitation(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	inv_id := vars["id"]
	if !bson.IsObjectIdHex(inv_id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	user, _ := ftsmodels.LoadUser(r)
	inv, err := ftsmodels.GetInvitation(db, bson.ObjectIdHex(inv_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	if inv.ToUserId != user.Id {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	inv.IsAccepted = false
	err = inv.Update(db)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	from_user, err := ftsmodels.GetUserById(db, inv.FromUserId.Hex())
	if err == nil {
		ftswsenders.ReplyInvitation(from_user, user, inv.Name(db), inv.Date, false)
	}

	w.WriteHeader(http.StatusAccepted)
}

type UserJson struct {
	UserId  string
	Name    string
	PhotoId string
}

type WhereJson struct {
	Id         string
	Name       string
	PhotoId    string
	IsBusiness bool
}

type InvitationJson struct {
	FromUser   UserJson
	Invitation ftsmodels.Invitation
	Where      WhereJson
}

func changeInvitationsToJson(db *mgo.Database, inv_ls []ftsmodels.Invitation, is_to bool) []InvitationJson {
	invjson_ls := []InvitationJson{}
	for _, v := range inv_ls {
		user_id := ""
		if is_to {
			user_id = v.FromUserId.Hex()
		} else {
			user_id = v.ToUserId.Hex()
		}
		fuser, _ := ftsmodels.GetUserById(db, user_id)
		fuser_json := UserJson{
			UserId:  fuser.Id.Hex(),
			Name:    fuser.Name,
			PhotoId: fuser.Info.PhotoId.Hex(),
		}

		where := WhereJson{}
		if v.BusinessId != nil {
			b, _ := ftsmodels.GetBusiness(db, *v.BusinessId)
			where.Name = b.Name
			where.Id = b.Id.Hex()
			where.PhotoId = b.PhotoId.Hex()
			where.IsBusiness = true
		}

		if v.EventId != nil {
			e, _ := ftsmodels.GetEvent(db, *v.EventId)
			where.Name = e.Name
			where.Id = e.Id.Hex()
			where.PhotoId = e.PhotoId.Hex()
			where.IsBusiness = false
		}

		invjson_ls = append(invjson_ls, InvitationJson{
			FromUser:   fuser_json,
			Invitation: v,
			Where:      where,
		})
	}
	return invjson_ls
}

func GetToInvitations(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	inv_ls, err := user.ToInvitations(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	out, _ := json.Marshal(changeInvitationsToJson(db, inv_ls, true))
	w.Write(out)
}

func GetFromInvitations(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	inv_ls, err := user.FromInvitations(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	out, _ := json.Marshal(changeInvitationsToJson(db, inv_ls, false))
	w.Write(out)
}

func GetInvitation(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	inv_id := vars["id"]

	if !bson.IsObjectIdHex(inv_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	inv_oid := bson.ObjectIdHex(inv_id)
	inv, err := ftsmodels.GetInvitation(db, inv_oid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	if inv.FromUserId != user.Id && inv.ToUserId != user.Id {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	out, _ := json.Marshal(inv)
	w.Write(out)

}
