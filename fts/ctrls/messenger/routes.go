package ctrlmessenger

import (
	"ftera/fts/utils"

	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	router.HandleFunc("/messengers", ftsutils.AuthHandler(GetMessengers)).Methods("GET")
	router.HandleFunc("/messengers/users/{id}", ftsutils.AuthHandler(CreateMessenger)).Methods("POST")
	router.HandleFunc("/messengers/{id}/messages", ftsutils.AuthHandler(GetMessages)).Methods("GET")
	router.HandleFunc("/messengers/{id}/count/messages", ftsutils.AuthHandler(CountMessages)).Methods("GET")
	router.HandleFunc("/messengers/{id}/ignore", ftsutils.AuthHandler(IgnoreMessenger)).Methods("POST")
	router.HandleFunc("/messages", ftsutils.AuthHandler(AddMessage)).Methods("POST")
}
