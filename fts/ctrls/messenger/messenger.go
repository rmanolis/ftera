package ctrlmessenger

import (
	"encoding/json"
	"ftera/fts/logics/messenger"
	"ftera/fts/models"
	"ftera/fts/utils"
	"ftera/fts/ws/senders"

	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	"strconv"
)

func GetMessengers(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	chats, err := logicmessenger.GetChats(db, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	out, _ := json.Marshal(chats)
	w.Write(out)
}

func CreateMessenger(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	userid := vars["id"]

	if !bson.IsObjectIdHex(userid) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	user_oid := bson.ObjectIdHex(userid)
	msgr, err := user.SetMessenger(db, user_oid)
	if err != nil {
		log.Fatalln(err.Error())
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	out, _ := json.Marshal(msgr)
	w.Write(out)
}

type MessageBody struct {
	MessengerId string
	Message     string
}

func AddMessage(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	decode := json.NewDecoder(r.Body)
	var mb MessageBody
	err := decode.Decode(&mb)
	if err != nil {
		log.Fatalln(err.Error())
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	if !bson.IsObjectIdHex(mb.MessengerId) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	messenger_oid := bson.ObjectIdHex(mb.MessengerId)
	messenger, err := user.Messenger(db, messenger_oid)
	if err != nil {
		log.Fatalln(err.Error())
		http.Error(w, "", http.StatusBadRequest)
		return
	}
	err = ftsmodels.ValidateMessage.StopSpammer(messenger, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	msg, err := user.AddMessage(db, messenger, mb.Message)
	if err != nil {
		http.Error(w, "", http.StatusBadRequest)
		return
	}
	user.CountNewMessage(db, messenger.Id)
	other, err := user.OtherFromChat(db, messenger.Id)
	if err == nil {
		ftswsenders.NewMessageToOther(user, other, messenger, msg)
	}
	messenger.IsRead = false
	messenger.UpdateWithDate(db)
	w.WriteHeader(http.StatusAccepted)
}

func GetMessages(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	messengerid := vars["id"]
	params := r.URL.Query()

	size_str := params.Get("size")
	size, err := strconv.Atoi(size_str)
	if err != nil {
		size = 10
	}

	page_str := params.Get("page")
	page, err := strconv.Atoi(page_str)
	if err != nil {
		page = 1
	}

	log.Println(r.URL.String())
	if !bson.IsObjectIdHex(messengerid) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	log.Println(messengerid)
	mrid := bson.ObjectIdHex(messengerid)
	msgs_json, err := logicmessenger.GetMessages(db, user, mrid,
		size, page)

	out, _ := json.Marshal(msgs_json)
	w.Write(out)
}

func CountMessages(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	messengerid := vars["id"]
	user, _ := ftsmodels.LoadUser(r)
	if !bson.IsObjectIdHex(messengerid) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	messenger_oid := bson.ObjectIdHex(messengerid)
	messenger, err := user.Messenger(db, messenger_oid)
	if err != nil {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	total := messenger.CountMessages(db)
	out, _ := json.Marshal(map[string]int{"Total": total})
	w.Write(out)
}

func IgnoreMessenger(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	messengerid := vars["id"]
	user, _ := ftsmodels.LoadUser(r)
	if !bson.IsObjectIdHex(messengerid) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	messenger_oid := bson.ObjectIdHex(messengerid)

	err := user.IgnoreMessenger(db, messenger_oid)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}
