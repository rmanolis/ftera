package ctrlpost

import (
	"ftera/fts/utils"

	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	router.HandleFunc("/users/profile/posts",
		ftsutils.AuthHandler(UserAddPost)).Methods("POST")
	router.HandleFunc("/users/other/{user_id}/posts",
		ftsutils.AuthHandler(GetUserPosts)).Methods("GET")

	router.HandleFunc("/posts",
		ftsutils.AuthHandler(GetNearPosts)).Methods("GET")
	router.HandleFunc("/posts/{id}/photo",
		ftsutils.AuthHandler(EditPhotoToPost)).Methods("POST")
	router.HandleFunc("/posts/{id}",
		ftsutils.AuthHandler(DeletePost)).Methods("DELETE")
	router.HandleFunc("/posts/{id}/is",
		ftsutils.AuthHandler(IsUserPost)).Methods("GET")
	router.HandleFunc("/posts/{id}",
		ftsutils.AuthHandler(GetPost)).Methods("GET")

	router.HandleFunc("/businesses/{business_id}/posts",
		ftsutils.AuthHandler(BusinessAddPost)).Methods("POST")
	router.HandleFunc("/businesses/{business_id}/posts",
		ftsutils.AuthHandler(GetBusinessPosts)).Methods("GET")

	router.HandleFunc("/events/{event_id}/posts",
		ftsutils.AuthHandler(EventAddPost)).Methods("POST")
	router.HandleFunc("/events/{event_id}/posts",
		ftsutils.AuthHandler(GetEventPosts)).Methods("GET")

}
