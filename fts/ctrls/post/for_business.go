package ctrlpost

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/utils"
	"ftera/ftutils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func BusinessAddPost(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	business_id := vars["business_id"]
	if !bson.IsObjectIdHex(business_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	business_oid := bson.ObjectIdHex(business_id)
	business, err := user.Business(db, business_oid)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	post := new(ftsmodels.Post)
	post.Text = r.FormValue("Text")

	err = ftsmodels.ValidatePost.CheckTimeForBusiness(db, business)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = business.AddPost(db, post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = addPhotoToPost(r, db, user, post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(post.Id.Hex()))

}

func GetBusinessPosts(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)

	vars := mux.Vars(r)
	business_id := vars["business_id"]
	if !bson.IsObjectIdHex(business_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	business_oid := bson.ObjectIdHex(business_id)
	business, err := ftsmodels.GetBusiness(db, business_oid)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	page, size := ftutils.PageSize(r)
	posts, err := business.Posts(db, page, size)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	posts_json := postsToJson(db, user, posts)
	out, _ := json.Marshal(posts_json)
	w.Write(out)

}
