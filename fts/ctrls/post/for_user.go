package ctrlpost

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/utils"
	"ftera/fts/ws/senders"
	"log"

	"ftera/ftutils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func addPhotoToPost(r *http.Request, db *mgo.Database, user *ftsmodels.User, post *ftsmodels.Post) error {
	files, err := ftutils.Files(r)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	for _, v := range files {
		photo, err := user.NewPhoto(db, v)
		if err == nil {
			post.PhotoId = photo.Id
			post.Update(db)
			return nil
		} else {
			post.Delete(db)
			return err
		}
	}
	return nil
}

func UserAddPost(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)

	post := new(ftsmodels.Post)
	post.Text = r.FormValue("Text")
	err := ftsmodels.ValidatePost.CheckTimeForUser(db, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = ftsmodels.ValidatePost.CheckTextLen(post.Text)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = user.AddPost(db, post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = addPhotoToPost(r, db, user, post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	ftswsenders.PostToNearUsers(db, post)
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(post.Id.Hex()))
}

type PostJson struct {
	ftsmodels.Post
	Likes   int
	MyLike  bool
	IsOwner bool
}

func postsToJson(db *mgo.Database, user *ftsmodels.User, posts []ftsmodels.Post) []PostJson {
	posts_json := []PostJson{}
	for _, v := range posts {
		pj := PostJson{}
		pj.Post = v
		pj.MyLike = user.IsLikeExists(db, v.Id)
		pj.Likes = pj.CountLikes(db)
		pj.IsOwner = (user.Id == v.OwnerId)
		posts_json = append(posts_json, pj)
	}
	return posts_json
}

func GetNearPosts(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	page, size := ftutils.PageSize(r)
	posts, _ := user.NearPosts(db, page, size)
	posts_json := postsToJson(db, user, posts)
	out, _ := json.Marshal(posts_json)
	w.Write(out)
}

func GetUserPosts(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	id := vars["user_id"]
	user, err := ftsmodels.GetUserById(db, id)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	page, size := ftutils.PageSize(r)
	posts, err := user.Posts(db, page, size)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	posts_json := postsToJson(db, user, posts)
	out, _ := json.Marshal(posts_json)
	w.Write(out)
}

func EditPhotoToPost(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	user, _ := ftsmodels.LoadUser(r)
	post, err := ftsmodels.GetPost(db, bson.ObjectIdHex(id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	if post.OwnerId != user.Id {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	files, err := ftutils.Files(r)
	if err == nil {
		for _, v := range files {
			photo, err := user.NewPhoto(db, v)
			if err == nil {
				post.PhotoId = photo.Id
			}
		}
	}

	err = post.Update(db)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	w.WriteHeader(http.StatusAccepted)
}

func DeletePost(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	user, _ := ftsmodels.LoadUser(r)
	post, err := ftsmodels.GetPost(db, bson.ObjectIdHex(id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	if post.OwnerId != user.Id {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	err = post.Delete(db)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	w.WriteHeader(http.StatusAccepted)

}

func IsUserPost(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	is := true
	user, _ := ftsmodels.LoadUser(r)
	post, err := ftsmodels.GetPost(db, bson.ObjectIdHex(id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	if post.OwnerId != user.Id {
		is = false
	}

	w.WriteHeader(http.StatusAccepted)
	out, _ := json.Marshal(map[string]bool{"Is": is})
	w.Write(out)
}

func GetPost(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	post, err := ftsmodels.GetPost(db, bson.ObjectIdHex(id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	out, _ := json.Marshal(post)
	w.Write(out)
}
