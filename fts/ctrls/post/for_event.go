package ctrlpost

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/utils"
	"ftera/ftutils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func EventAddPost(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	event_oid := bson.ObjectIdHex(event_id)
	event, err := ftsmodels.GetEvent(db, event_oid)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	if event.UserId != user.Id {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	post := new(ftsmodels.Post)
	post.Text = r.FormValue("Text")

	err = ftsmodels.ValidatePost.CheckTimeForEvent(db, event)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = event.AddPost(db, post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = addPhotoToPost(r, db, user, post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(post.Id.Hex()))

}

func GetEventPosts(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)

	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	event_oid := bson.ObjectIdHex(event_id)
	event, err := ftsmodels.GetEvent(db, event_oid)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	page, size := ftutils.PageSize(r)
	posts, err := event.Posts(db, page, size)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	posts_json := postsToJson(db, user, posts)
	out, _ := json.Marshal(posts_json)
	w.Write(out)

}
