package ctrlattention

import (
	"ftera/fts/utils"

	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	router.HandleFunc("/events/{event_id}/attentions",
		ftsutils.AuthHandler(AddAttention)).Methods("POST")
	router.HandleFunc("/events/{event_id}/attentions",
		ftsutils.AuthHandler(GetAttentions)).Methods("GET")
	router.HandleFunc("/events/{event_id}/count/attentions",
		ftsutils.AuthHandler(CountAttentions)).Methods("GET")
	router.HandleFunc("/events/{event_id}/one/attention",
		ftsutils.AuthHandler(GetAttention)).Methods("GET")
	router.HandleFunc("/events/{event_id}/attentions",
		ftsutils.AuthHandler(RemoveAttention)).Methods("DELETE")
}
