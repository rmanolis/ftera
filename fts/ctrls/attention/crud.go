package ctrlattention

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/models/enums"

	"ftera/fts/utils"
	"ftera/fts/ws/senders"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func AddAttention(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	event, err := ftsmodels.GetEvent(db, bson.ObjectIdHex(event_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	is_exists := user.IsAttentionExists(db, bson.ObjectIdHex(event_id))
	if is_exists {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	at, err := user.AddAttention(db, bson.ObjectIdHex(event_id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	ftswsenders.UserWillGo(db, user, event)

	w.Write([]byte(at.Id))
}

func GetAttention(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	event, err := ftsmodels.GetEvent(db, bson.ObjectIdHex(event_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	at, err := event.Attention(db, user.Id)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	out, _ := json.Marshal(at)
	w.Write(out)
}

type AttentionJson struct {
	UserId  string
	Name    string
	Sex     string
	PhotoId string
	EventId string
}

func GetAttentions(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	event, err := ftsmodels.GetEvent(db, bson.ObjectIdHex(event_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	attentions, _ := event.Attentions(db)
	aj_ls := []AttentionJson{}
	for _, v := range attentions {
		aj := AttentionJson{}
		other, err := ftsmodels.GetUserById(db, v.UserId.Hex())
		if err == nil {
			aj.Name = other.Name
			aj.UserId = other.Id.Hex()
			aj.PhotoId = other.Info.PhotoId.Hex()
			aj.Sex = enums.SexList[other.Info.Sex]
			aj.EventId = v.EventId.Hex()
			aj_ls = append(aj_ls, aj)
		}
	}
	out, _ := json.Marshal(aj_ls)
	w.Write(out)
}

func CountAttentions(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	event, err := ftsmodels.GetEvent(db, bson.ObjectIdHex(event_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	n, _ := event.CountAttentions(db)
	out, _ := json.Marshal(map[string]int{"Count": n})
	w.Write(out)
}

func RemoveAttention(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	event, err := ftsmodels.GetEvent(db, bson.ObjectIdHex(event_id))
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	at, err := event.Attention(db, user.Id)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	err = at.Delete(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}
