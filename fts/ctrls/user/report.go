package ctrluser

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/utils"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	"net/http"
)

type ReportBody struct {
	Type        string
	Description string
}

func ReportUser(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "this is not an id", 404)
		return
	}

	decoder := json.NewDecoder(r.Body)
	report := new(ReportBody)
	decoder.Decode(&report)
	other := new(ftsmodels.User)
	err := other.Get(db, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	err = user.ReportUser(db, other.Id,
		report.Type, report.Description)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}

func GetTypeReports(w http.ResponseWriter, r *http.Request) {
	out, _ := json.Marshal(ftsmodels.TypeReportList)
	w.Write(out)
}
