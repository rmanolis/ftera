package ctrluser

import (
	"encoding/json"
	"ftera/fts/logics/user"
	"ftera/fts/models"
	"ftera/fts/models/enums"
	"log"

	"ftera/fts/utils"
	"net/http"
)

func GetSearchOptions(w http.ResponseWriter, r *http.Request) {
	out, _ := json.Marshal(enums.SearchOptions)
	w.Write(out)
}

func GetPreferences(w http.ResponseWriter, r *http.Request) {
	user := new(ftsmodels.User)
	user.Load(r)
	upb := new(logicuser.UserPreferencesBody)

	upb.Sex = user.PrefSex()
	upb.SearchOption = user.PrefSearchOption()
	upb.ToAge = user.Preferences.ToAge
	upb.FromAge = user.Preferences.FromAge
	upb.Kilometer = user.Preferences.Kilometer
	out, _ := json.Marshal(upb)
	w.Write(out)

}

func SearchUsers(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user := new(ftsmodels.User)
	user.Load(r)
	data := map[string]int{"page": 0, "size": 9}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	log.Println(data)
	res := logicuser.CollectUsers(db, user, data["size"], data["page"])
	log.Println(res)
	out, _ := json.Marshal(res)
	w.WriteHeader(http.StatusAccepted)
	w.Write(out)
}

func SetPreferences(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	decoder := json.NewDecoder(r.Body)
	var data = new(logicuser.UserPreferencesBody)
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	user, err := ftsmodels.LoadUser(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = logicuser.SetPreferences(db, user, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}
