package ctrluser

import (
	"encoding/json"
	"ftera/fts/logics/user"
	"ftera/fts/models"
	"ftera/fts/models/enums"
	"ftera/fts/utils"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func GetSexes(w http.ResponseWriter, r *http.Request) {
	out, _ := json.Marshal(enums.SexList)
	w.Write(out)
}

func GetProfileInfo(w http.ResponseWriter, r *http.Request) {
	user := new(ftsmodels.User)
	user.Load(r)
	out, _ := logicuser.WriteUserInfo(user)
	w.Write(out)
}

func GetUserInfo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	if bson.IsObjectIdHex(id) {
		userid := bson.ObjectIdHex(id)
		if user.IsBlockingUser(db, userid) || user.IsBlockedByUser(db, userid) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
	}

	other := new(ftsmodels.User)
	err := other.Get(db, id)

	if err != nil {
		w.WriteHeader(http.StatusNotAcceptable)
	} else {
		out, _ := logicuser.WriteUserInfo(other)
		w.Write(out)
	}
}

func SetProfileInfo(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	decoder := json.NewDecoder(r.Body)
	var data = new(logicuser.UserInfoBody)
	user := new(ftsmodels.User)
	user.Load(r)
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	log.Println(data)
	user.SetBio(data.Bio)
	user.SetSex(data.Sex)
	err = user.SetBirthDate(data.BirthDate)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	if data.Geolocation[0] == 0 && data.Geolocation[1] == 0 {
		log.Println("error: geolocation have zeros for location ", data.Location)
		data.Location = "Athens, Greece"
		data.Geolocation = []float64{23.729359899999963, 37.983917}
	}

	user.SetLocation(data.Location, data.Geolocation)
	if user.IsNew {
		user.IsNew = false
	}
	err = user.Update(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return

	}
	w.WriteHeader(http.StatusAccepted)

}
