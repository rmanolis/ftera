package ctrluser

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/utils"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	"net/http"
)

type UserBlockJson struct {
	UserId bson.ObjectId
	Name   string
}

func GetBlocks(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	blocks, err := user.Blocks(db)
	var bl_ls = []UserBlockJson{}
	if err != nil {
		out, _ := json.Marshal(bl_ls)
		w.Write(out)
		return
	}

	for _, v := range blocks {
		u, _ := ftsmodels.GetUserById(db, v.BlockUserId.Hex())
		if u != nil {
			ubj := UserBlockJson{
				UserId: v.BlockUserId,
				Name:   u.Name,
			}
			bl_ls = append(bl_ls, ubj)
		}
	}
	out, _ := json.Marshal(bl_ls)
	w.Write(out)
}

func IsBlockingUser(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if bson.IsObjectIdHex(id) {
		userid := bson.ObjectIdHex(id)
		is := user.IsBlockingUser(db, userid)
		if is {
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func IsBlockedByUser(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if bson.IsObjectIdHex(id) {
		userid := bson.ObjectIdHex(id)
		is := user.IsBlockedByUser(db, userid)
		if is {
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func GetBlocksByOthers(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	blocks, err := user.BlocksByOthers(db)
	bl_ls := []UserBlockJson{}

	if err != nil {
		out, _ := json.Marshal(bl_ls)
		w.Write(out)
		return
	}

	for _, v := range blocks {
		u, _ := ftsmodels.GetUserById(db, v.UserId.Hex())
		if u != nil {
			ubj := UserBlockJson{
				UserId: v.UserId,
				Name:   u.Name,
			}
			bl_ls = append(bl_ls, ubj)
		}
	}
	out, _ := json.Marshal(bl_ls)
	w.Write(out)
}

func SetBlock(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	userid := vars["id"]
	if bson.IsObjectIdHex(userid) {
		other, err := ftsmodels.GetUserById(db, userid)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}
		err = user.BlockUser(db, other.Id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusAccepted)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func DeleteBlock(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	userid := vars["id"]
	if bson.IsObjectIdHex(userid) {
		other, err := ftsmodels.GetUserById(db, userid)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}
		err = user.UnblockUser(db, other.Id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusAccepted)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}

}
