package ctrluser

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/utils"
	"ftera/ftutils"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func GetUser(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if bson.IsObjectIdHex(id) {
		userid := bson.ObjectIdHex(id)
		if user.IsBlockingUser(db, userid) || user.IsBlockedByUser(db, userid) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
	}
	other, err := ftsmodels.GetUserById(db, id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	out, _ := json.Marshal(struct {
		Name     string
		Id       string
		IsOnline bool
	}{
		other.Name,
		other.Id.Hex(),
		other.IsOnline,
	})
	w.Write(out)
}

func GetProfileId(w http.ResponseWriter, r *http.Request) {
	user, _ := ftsmodels.LoadUser(r)
	out, _ := json.Marshal(map[string]string{
		"Name": user.Name,
		"Id":   user.Id.Hex(),
	})

	w.Write(out)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	err := user.Delete(db)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}

func SetLanguage(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	lang, _ := ioutil.ReadAll(r.Body)
	user.Language = string(lang)
	log.Println(string(lang))
	user.Update(db)
	w.WriteHeader(http.StatusAccepted)
}

func GetLanguage(w http.ResponseWriter, r *http.Request) {
	user, _ := ftsmodels.LoadUser(r)
	w.Write([]byte(user.Language))
}

func Profile(w http.ResponseWriter, r *http.Request) {
	user_id, _ := ftsutils.UserId(r)
	db := ftsutils.GetDB(r)
	user := new(ftsmodels.User)
	user.Get(db, user_id)
	user.Password = ""
	out, _ := json.Marshal(user)
	w.Write(out)

}

const MAX_MEMORY = 3 * 1024 * 1024

func SetImage(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(MAX_MEMORY)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusForbidden)
	}

	user := new(ftsmodels.User)
	user.Load(r)
	db := ftsutils.GetDB(r)

	buf_ls, err := ftutils.Files(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	for _, buf := range buf_ls {
		photo, err := user.NewPhoto(db, buf)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotAcceptable)
			return
		}
		/* We will not delete the previous, because Post, Attentions and Likes use the photoid
		old_photo, err := user.GetPhoto(db)
		if old_photo != nil {
			err := old_photo.Delete(db)
			if err != nil {
				http.Error(w, err.Error(), http.StatusNotAcceptable)
				return
			}
		}*/
		user.SetPhoto(photo.Id)
		err = user.Update(db)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotAcceptable)
			return
		}

	}

	w.WriteHeader(http.StatusAccepted)

}

func DeleteImage(w http.ResponseWriter, r *http.Request) {
	user := new(ftsmodels.User)
	user.Load(r)
	db := ftsutils.GetDB(r)
	user.Info.PhotoId = ""
	user.Update(db)
	w.WriteHeader(http.StatusAccepted)
}

type SessionBody struct {
	SessionId string
}

func SetSession(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	decoder := json.NewDecoder(r.Body)
	var session SessionBody
	decoder.Decode(&session)
	if len(session.SessionId) == 0 {
		w.WriteHeader(http.StatusNotAcceptable)
		return
	}
	for _, v := range user.SessionIds {
		if v == session.SessionId {
			w.WriteHeader(http.StatusNotAcceptable)
			return
		}
	}
	user.SessionIds = append(user.SessionIds, session.SessionId)
	user.IsOnline = true
	err := user.Update(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	return
}
