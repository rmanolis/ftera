package ctrluser

import (
	"ftera/fts/utils"

	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	a := router.PathPrefix("/users").Subrouter()

	a.HandleFunc("/profile", ftsutils.AuthHandler(Profile)).Methods("GET")
	a.HandleFunc("/profile/id", ftsutils.AuthHandler(GetProfileId)).Methods("GET")
	a.HandleFunc("/profile/delete", ftsutils.AuthHandler(DeleteUser)).Methods("DELETE")
	a.HandleFunc("/profile/settings", ftsutils.AuthHandler(SetSettings)).Methods("PUT")
	a.HandleFunc("/profile/isnew", ftsutils.AuthHandler(GetIsNew)).Methods("GET")

	a.HandleFunc("/profile/language", ftsutils.AuthHandler(SetLanguage)).Methods("POST")
	a.HandleFunc("/profile/language", ftsutils.AuthHandler(GetLanguage)).Methods("GET")

	a.HandleFunc("/profile/session", ftsutils.AuthHandler(SetSession)).Methods("PUT")
	a.HandleFunc("/profile/info", ftsutils.AuthHandler(GetProfileInfo)).Methods("GET")
	a.HandleFunc("/profile/info", ftsutils.AuthHandler(SetProfileInfo)).Methods("PUT")

	a.HandleFunc("/other/{id}", ftsutils.AuthHandler(GetUser)).Methods("GET")
	a.HandleFunc("/other/{id}/info", ftsutils.AuthHandler(GetUserInfo)).Methods("GET")

	a.HandleFunc("/profile/photo", ftsutils.AuthHandler(SetImage)).Methods("POST")
	a.HandleFunc("/profile/photo", ftsutils.AuthHandler(DeleteImage)).Methods("DELETE")

	a.HandleFunc("/profile/preferences", ftsutils.AuthHandler(GetPreferences)).Methods("GET")
	a.HandleFunc("/profile/preferences", ftsutils.AuthHandler(SetPreferences)).Methods("PUT")

	a.HandleFunc("/profile/blocks", ftsutils.AuthHandler(GetBlocks)).Methods("GET")
	a.HandleFunc("/profile/blocks/{id}", ftsutils.AuthHandler(SetBlock)).Methods("POST")
	a.HandleFunc("/profile/blocks/{id}", ftsutils.AuthHandler(IsBlockingUser)).Methods("GET")
	a.HandleFunc("/profile/blocks_by", ftsutils.AuthHandler(GetBlocksByOthers)).Methods("GET")
	a.HandleFunc("/profile/blocks_by/{id}", ftsutils.AuthHandler(IsBlockedByUser)).Methods("GET")

	a.HandleFunc("/profile/unblock/{id}", ftsutils.AuthHandler(DeleteBlock)).Methods("PUT")

	a.HandleFunc("/search", ftsutils.AuthHandler(SearchUsers)).Methods("PUT")

	router.HandleFunc("/sexes", GetSexes).Methods("GET")

	router.HandleFunc("/report/types", GetTypeReports).Methods("GET")
	router.HandleFunc("/report/users/{id}", ftsutils.AuthHandler(ReportUser)).Methods("POST")

	router.HandleFunc("/search/options", GetSearchOptions).Methods("GET")
}
