package ctrluser

import (
	"encoding/json"
	"fmt"
	"ftera/fts/models"
	"ftera/fts/utils"
	"net/http"
)

type UserSettingsBody struct {
	Name        string
	Email       string
	IsPublic    bool
	IsBusiness  bool
	OldPassword *string
	NewPassword *string
}

func SetSettings(w http.ResponseWriter, r *http.Request) {
	var data = new(UserSettingsBody)
	db := ftsutils.GetDB(r)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	if err != nil {
		fmt.Fprint(w, err.Error())
		return
	}

	user := new(ftsmodels.User)
	user.Load(r)
	if data.OldPassword != nil && data.NewPassword != nil {
		err = user.SetPassword(db, *data.OldPassword,
			*data.NewPassword)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotAcceptable)
			return
		}
	}
	user.IsPublic = data.IsPublic
	user.IsBusiness = data.IsBusiness
	err = user.SetEmail(db, data.Email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = user.SetName(data.Name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = user.Update(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}

func GetIsNew(w http.ResponseWriter, r *http.Request) {
	user := new(ftsmodels.User)
	user.Load(r)
	out, _ := json.Marshal(map[string]bool{"Is": user.IsNew})
	w.WriteHeader(http.StatusAccepted)
	w.Write(out)
}
