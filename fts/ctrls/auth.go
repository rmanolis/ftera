package ftsctrls

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"ftera/fts/logics"
	"ftera/fts/models"
	"ftera/fts/utils"
	"ftera/ftutils"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/goincremental/negroni-sessions"
	"github.com/gorilla/mux"
)

type Credentials struct {
	Email    string
	Password string
}

type Auth struct{}

func (a Auth) Login(w http.ResponseWriter, r *http.Request) {
	log.Println("Login")
	decoder := json.NewDecoder(r.Body)
	credentials := new(Credentials)
	err := decoder.Decode(&credentials)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, "Wrong password or email", http.StatusNotAcceptable)
		return
	}

	db := ftsutils.GetDB(r)
	user := new(ftsmodels.User)
	err = user.Authenticate(db, credentials.Email, credentials.Password)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, "Wrong password or email", http.StatusNotAcceptable)
		return
	}

	if !user.IsValidated {
		http.Error(w, "User is not validated", http.StatusNotAcceptable)
		return
	}
	session := sessions.GetSession(r)
	session.Set("user_id", user.Id.Hex())

	w.WriteHeader(http.StatusAccepted)
}

func (a Auth) Logout(w http.ResponseWriter, r *http.Request) {
	session := sessions.GetSession(r)
	user_id := session.Get("user_id")
	log.Println(user_id)
	if user_id == nil {
		http.Redirect(w, r, "/", http.StatusMovedPermanently)
		return

	} else {
		db := ftsutils.GetDB(r)
		user, _ := ftsmodels.LoadUser(r)
		user.IsOnline = false
		user.Update(db)
		session.Delete("user_id")
		http.Redirect(w, r, "/", http.StatusMovedPermanently)
		return
	}

}

func (a Auth) IsUser(w http.ResponseWriter, r *http.Request) {
	log.Println("IsUser")
	db := ftsutils.GetDB(r)
	session := sessions.GetSession(r)
	user_id := session.Get("user_id")
	if user_id == nil {
		w.WriteHeader(403)
		return
	}
	user := new(ftsmodels.User)
	err := user.Get(db, user_id.(string))
	if err != nil {
		w.WriteHeader(http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(user_id.(string)))
}

func (a Auth) Register(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	data := map[string]string{"name": "", "email": "", "password": ""}
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	db := ftsutils.GetDB(r)

	err_ls := logic.Register(db, data["name"], data["email"], data["password"])
	err_str_ls := []string{}
	for i := range err_ls {
		err_str_ls = append(err_str_ls, err_ls[i].Error())
	}
	if len(err_ls) == 0 {

		w.WriteHeader(http.StatusAccepted)
		return
	} else {
		err_json := map[string][]string{"errors": err_str_ls}
		outData, _ := json.Marshal(err_json)
		w.WriteHeader(http.StatusNotAcceptable)
		w.Write(outData)
		return
	}
}

func (a Auth) ValidateToken(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	tokenid := vars["id"]
	err := ftsmodels.ValidateUserToken(db, tokenid)
	log.Println(err)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusMovedPermanently)
		return
	}
	http.Redirect(w, r, "/", http.StatusMovedPermanently)
}

func (a Auth) ForgotPassword(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	body, _ := ioutil.ReadAll(r.Body)
	email := string(body)
	password := ftutils.RandSeq(10)
	h := md5.New()
	io.WriteString(h, password)
	hex_password := hex.EncodeToString(h.Sum(nil))
	user, err := ftsmodels.FindUserByEmail(db, email)
	if err != nil {
		log.Println("Forgot password for email " +
			email + " got error " + err.Error())
		w.WriteHeader(http.StatusAccepted)
		return
	}
	user.Password = hex_password
	err = user.Update(db)
	if err != nil {
		log.Println("For updating password for email " +
			email + " got error " + err.Error())
		w.WriteHeader(http.StatusAccepted)
		return
	}
	eu := ftutils.GmailUser
	std := ftutils.SmtpTemplateData{
		To:      user.Email,
		Subject: "New password from Pullmeout",
		Body: "Your request for new password is accepted,\n" +
			"your password is " + password,
	}
	ftutils.SendMail(&eu, &std)
	w.WriteHeader(http.StatusAccepted)
}
