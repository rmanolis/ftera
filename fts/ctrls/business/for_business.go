package ctrlbusiness

import (
	"encoding/json"
	"ftera/fts/logics/business"
	"ftera/fts/models"
	"ftera/fts/models/enums"
	"ftera/fts/utils"
	"ftera/ftutils"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func GetDays(w http.ResponseWriter, r *http.Request) {
	out, _ := json.Marshal(enums.Days)
	w.Write(out)
}

func AddBusiness(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	b := new(logicbusiness.BusinessBody)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&b)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	business := new(ftsmodels.Business)
	logicbusiness.SetupBusiness(b, business)
	business.IsPublic = true
	err = business.Validate()
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = user.AddBusiness(db, business)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(business.Id.Hex()))
}

func EditBusiness(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "no id", http.StatusNotAcceptable)
		return
	}
	bs_id := bson.ObjectIdHex(id)
	business, err := user.Business(db, bs_id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	b := new(logicbusiness.BusinessBody)
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&b)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	logicbusiness.SetupBusiness(b, business)
	err = business.Validate()
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = business.Update(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}

func GetBusinesses(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	businesses, err := user.Businesses(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	log.Println(businesses)
	out, _ := json.Marshal(businesses)
	w.Write(out)
}

func GetBusiness(w http.ResponseWriter, r *http.Request) {

	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	bs_id := bson.ObjectIdHex(id)
	business, err := user.Business(db, bs_id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	log.Println(time.Local)

	out, _ := json.Marshal(business)
	w.Write(out)
}

func ShowBusiness(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	bs_id := bson.ObjectIdHex(id)
	business, err := ftsmodels.GetBusiness(db, bs_id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	out, _ := json.Marshal(business)
	w.Write(out)
}

func SetPhotoForBusiness(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	bs_id := bson.ObjectIdHex(id)
	business, err := user.Business(db, bs_id)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	buf_ls, err := ftutils.Files(r)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	for _, buf := range buf_ls {
		photo, err := user.NewPhoto(db, buf)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, err.Error(), http.StatusNotAcceptable)
			return
		}
		old_photo, err := business.Photo(db)
		if err == nil {
			old_photo.Delete(db)
		}
		business.PhotoId = photo.Id
	}
	err = business.Update(db)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	out, _ := json.Marshal(map[string]string{"PhotoId": business.PhotoId.Hex()})
	w.Write(out)
}

func DeleteBusiness(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	id := vars["id"]
	if !bson.IsObjectIdHex(id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	bs_id := bson.ObjectIdHex(id)
	business, err := user.Business(db, bs_id)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	err = business.Delete(db)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}
