package ctrlbusiness

import (
	"encoding/json"
	"ftera/fts/models"
	"ftera/fts/utils"
	"ftera/ftutils"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type SearchTagBody struct {
	Search string
	Type   *int
}

// Search name db.users.find({'name': {'$regex': 'sometext'}})
func SearchTags(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	st := new(SearchTagBody)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&st)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	st_ls, err := ftsmodels.SearchTags(db, st.Search, st.Type)
	if err != nil {
		http.Error(w, "[]", http.StatusNotAcceptable)
		return
	}
	var ls = []string{}
	for _, v := range st_ls {
		ls = append(ls, v.Name)
	}
	out, _ := json.Marshal(ls)
	w.Write(out)
}

func PageTagsByType(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	page, size := ftutils.PageSize(r)
	vars := mux.Vars(r)
	typ, err := strconv.Atoi(vars["type"])
	if err != nil {
		http.Error(w, "[]", http.StatusNotAcceptable)
		return
	}

	tags, err := ftsmodels.PageTagsByType(db, typ, page, size)
	if err != nil {
		http.Error(w, "[]", http.StatusNotAcceptable)
		return
	}

	out, _ := json.Marshal(tags)
	w.Write(out)
}
