package ctrlbusiness

import (
	"ftera/fts/utils"

	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	//For tag
	router.HandleFunc("/tags/search",
		SearchTags).Methods("POST")
	router.HandleFunc("/tags/type/{type}",
		PageTagsByType).Methods("GET")

	//For business
	router.HandleFunc("/users/profile/businesses",
		ftsutils.AuthHandler(GetBusinesses)).Methods("GET")
	router.HandleFunc("/users/profile/businesses",
		ftsutils.AuthHandler(AddBusiness)).Methods("POST")
	router.HandleFunc("/users/profile/businesses/{id}",
		ftsutils.AuthHandler(EditBusiness)).Methods("PUT")
	router.HandleFunc("/users/profile/businesses/{id}/photo",
		ftsutils.AuthHandler(SetPhotoForBusiness)).Methods("POST")
	router.HandleFunc("/users/profile/businesses/{id}",
		ftsutils.AuthHandler(GetBusiness)).Methods("GET")
	router.HandleFunc("/users/profile/businesses/{id}",
		ftsutils.AuthHandler(DeleteBusiness)).Methods("DELETE")
	router.HandleFunc("/businesses/{id}",
		ShowBusiness).Methods("GET")

	router.HandleFunc("/days",
		ftsutils.AuthHandler(GetDays)).Methods("GET")

	//For events
	router.HandleFunc("/users/profile/events",
		ftsutils.AuthHandler(GetEvents)).Methods("GET")
	router.HandleFunc("/users/profile/events",
		ftsutils.AuthHandler(AddEvent)).Methods("POST")
	router.HandleFunc("/users/profile/events/{event_id}",
		ftsutils.AuthHandler(GetEvent)).Methods("GET")
	router.HandleFunc("/users/profile/events/{event_id}",
		ftsutils.AuthHandler(EditEvent)).Methods("PUT")
	router.HandleFunc("/users/profile/events/{event_id}",
		ftsutils.AuthHandler(DeleteEvent)).Methods("DELETE")
	router.HandleFunc("/users/profile/events/{event_id}/photo",
		ftsutils.AuthHandler(SetPhotoForEvent)).Methods("POST")
	router.HandleFunc("/events/{event_id}",
		ShowEvent).Methods("GET")

}
