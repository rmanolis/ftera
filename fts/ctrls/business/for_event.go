package ctrlbusiness

import (
	"encoding/json"
	"ftera/fts/logics/business"
	"ftera/fts/models"
	"ftera/fts/utils"
	"ftera/ftutils"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func AddEvent(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	e := new(logicbusiness.EventBody)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&e)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	event := new(ftsmodels.Event)
	logicbusiness.SetupEvent(e, event)
	event.IsPublic = true
	err = event.Validate()
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = user.AddEvent(db, event)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(event.Id.Hex()))
}

func EditEvent(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	e := new(logicbusiness.EventBody)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&e)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	event_oid := bson.ObjectIdHex(event_id)
	event, err := user.Event(db, event_oid)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	if event.UserId != user.Id {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	logicbusiness.SetupEvent(e, event)
	err = event.Update(db)
	if err != nil {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}

func GetEvents(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)

	events, err := user.Events(db)
	if err != nil {
		http.Error(w, "[]", http.StatusAccepted)
		return
	}
	out, _ := json.Marshal(events)
	w.Write(out)
}

func GetEvent(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]

	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	event_oid := bson.ObjectIdHex(event_id)
	events, err := user.Event(db, event_oid)
	if err != nil {
		http.Error(w, "[]", http.StatusAccepted)
		return
	}
	out, _ := json.Marshal(events)
	w.Write(out)
}

func ShowEvent(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}
	event_oid := bson.ObjectIdHex(event_id)
	events, err := ftsmodels.GetEvent(db, event_oid)
	if err != nil {
		http.Error(w, "[]", http.StatusAccepted)
		return
	}
	out, _ := json.Marshal(events)
	w.Write(out)
}

func SetPhotoForEvent(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	ev_oid := bson.ObjectIdHex(event_id)
	event, err := user.Event(db, ev_oid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	buf_ls, err := ftutils.Files(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	for _, buf := range buf_ls {
		photo, err := user.NewPhoto(db, buf)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotAcceptable)
			return
		}
		old_photo, err := event.Photo(db)
		if err == nil {
			old_photo.Delete(db)
		}
		event.PhotoId = photo.Id
	}
	err = event.Update(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	out, _ := json.Marshal(map[string]string{"PhotoId": event.PhotoId.Hex()})
	w.Write(out)
}

func DeleteEvent(w http.ResponseWriter, r *http.Request) {
	db := ftsutils.GetDB(r)
	user, _ := ftsmodels.LoadUser(r)
	vars := mux.Vars(r)
	event_id := vars["event_id"]
	if !bson.IsObjectIdHex(event_id) {
		http.Error(w, "", http.StatusNotAcceptable)
		return
	}

	ev_oid := bson.ObjectIdHex(event_id)
	event, err := user.Event(db, ev_oid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	err = event.Delete(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}
