package ftconfigs

import (
	"ftera/ftutils"
)

func FteraDB() *ftutils.DataStore {
	conf, err := ftutils.Configuration("ftera_db.json")
	if err != nil {
		return nil
	}

	dt := ftutils.NewDataStore(conf["Name"], conf["IP"], conf["Port"])
	return dt
}

func AdminDB() *ftutils.DataStore {
	conf, err := ftutils.Configuration("admin_db.json")
	if err != nil {
		return nil
	}

	dt := ftutils.NewDataStore(conf["Name"], conf["IP"], conf["Port"])
	return dt

}
