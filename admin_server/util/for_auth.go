package adminutil

import (
	"errors"
	"log"
	"net/http"

	"github.com/goincremental/negroni-sessions"
)

func AdminId(r *http.Request) (string, error) {
	session := sessions.GetSession(r)
	admin_id := session.Get("admin_id")

	if admin_id == nil {
		return "", errors.New("No admin")
	} else {
		return admin_id.(string), nil
	}
}

func AuthHandler(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rec := recover(); rec != nil {
				log.Println("Recovered in f", r)
				session := sessions.GetSession(r)
				session.Delete("admin_id")
				http.Redirect(w, r, "/", http.StatusUnauthorized)
				return
			}
		}()
		_, err := AdminId(r)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		} else {
			next(w, r)

		}
	}
}
