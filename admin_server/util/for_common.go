package adminutil

type JsonResults struct {
	Total int
	Rows  interface{}
	Pages int
}
