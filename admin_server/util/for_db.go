package adminutil

import (
	"ftera/ftutils"
	"net/http"

	"gopkg.in/mgo.v2"
)

func GetAdminDB(r *http.Request) *mgo.Database {
	return ftutils.GetDB(r, "admin-ftera")
}

func GetServerDB(r *http.Request) *mgo.Database {
	return ftutils.GetDB(r, "ftera")
}
