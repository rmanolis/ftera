package ctrl

import (
	"encoding/json"
	"ftera/admin_server/models"
	"net/http"

	"github.com/goincremental/negroni-sessions"
	"gopkg.in/mgo.v2/bson"
)

type Credentials struct {
	Name     string
	Password string
}

func Login(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	credentials := new(Credentials)
	err := decoder.Decode(&credentials)
	if err != nil {
		panic(err)
	}

	admin, err := adminmodels.AuthenticateAdmin(r, credentials.Name, credentials.Password)
	if err != nil {
		http.Error(w, "Wrong password or email", http.StatusNotAcceptable)
		return
	}

	session := sessions.GetSession(r)
	session.Set("admin_id", admin.Id.Hex())
	w.WriteHeader(http.StatusAccepted)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	session := sessions.GetSession(r)
	admin_id := session.Get("admin_id")
	if admin_id == nil {
		http.Redirect(w, r, "/", http.StatusNotAcceptable)
		return
	}
	session.Delete("admin_id")
	http.Redirect(w, r, "/", http.StatusMovedPermanently)

}

func IsAdmin(w http.ResponseWriter, r *http.Request) {
	session := sessions.GetSession(r)
	admin_id := session.Get("admin_id")
	if admin_id == nil {
		w.WriteHeader(403)
		return

	}

	_, err := adminmodels.GetAdministrator(r, bson.ObjectIdHex(admin_id.(string)))
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(admin_id.(string)))
}
