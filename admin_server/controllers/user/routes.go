package ctrluser

import (
	"ftera/admin_server/util"

	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	router.HandleFunc("/users", adminutil.AuthHandler(ListUsers)).Methods("GET")
}
