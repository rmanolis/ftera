package ctrl

import (
	"encoding/json"
	"ftera/admin_server/util"
	"net/http"
)

type UrlLocation struct {
	Url   string `json:"Url"`
	Title string `json:"Title"`
}

func Welcome(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, "static/index.html")
}

func Sitemap(w http.ResponseWriter, req *http.Request) {
	sitemap := []UrlLocation{
		UrlLocation{Url: "#/login",
			Title: "Login"},
	}
	sitemap_for_user := []UrlLocation{
		UrlLocation{Url: "/auth/logout",
			Title: "Logout"},
		UrlLocation{},
	}
	_, err := adminutil.AdminId(req)
	if err == nil {
		js, _ := json.Marshal(sitemap_for_user)
		w.Write(js)
	} else {
		js, _ := json.Marshal(sitemap)
		w.Write(js)
	}
}
