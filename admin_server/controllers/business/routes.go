package ctrlbusiness

import (
	"ftera/admin_server/util"

	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	router.HandleFunc("/businesses", adminutil.AuthHandler(ListBusinesses)).Methods("GET")
}
