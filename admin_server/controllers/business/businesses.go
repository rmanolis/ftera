package ctrlbusiness

import (
	"encoding/json"
	"ftera/admin_server/models"
	"ftera/admin_server/util"
	"ftera/ftutils"
	"log"
	"math"
	"net/http"
)

func ListBusinesses(w http.ResponseWriter, r *http.Request) {
	page, size := ftutils.PageSize(r)
	lusers, err := adminmodels.ListBusinesses(r, page, size)
	if err != nil {
		log.Println(err.Error())
	}
	total, _ := adminmodels.CountBusinesses(r)
	pages := int(math.Ceil(float64(total) / float64(size)))
	results := adminutil.JsonResults{
		Total: total,
		Rows:  lusers,
		Pages: pages,
	}

	out, _ := json.Marshal(results)
	w.Write(out)
}
