package ctrlevent

import (
	"ftera/admin_server/util"

	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	router.HandleFunc("/events", adminutil.AuthHandler(ListEvents)).Methods("GET")
}
