package ctrlevent

import (
	"encoding/json"
	"ftera/admin_server/models"
	"ftera/admin_server/util"
	"ftera/ftutils"
	"log"
	"math"
	"net/http"
)

func ListEvents(w http.ResponseWriter, r *http.Request) {
	page, size := ftutils.PageSize(r)
	lusers, err := adminmodels.ListEvents(r, page, size)
	if err != nil {
		log.Println(err.Error())
	}
	total, _ := adminmodels.CountEvents(r)
	pages := int(math.Ceil(float64(total) / float64(size)))
	results := adminutil.JsonResults{
		Total: total,
		Rows:  lusers,
		Pages: pages,
	}

	out, _ := json.Marshal(results)
	w.Write(out)
}
