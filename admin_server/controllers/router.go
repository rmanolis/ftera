package ctrl

import (
	"ftera/admin_server/controllers/business"
	"ftera/admin_server/controllers/event"
	"ftera/admin_server/controllers/user"

	"github.com/gorilla/mux"
)

func LoadRoutes() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", Welcome).Methods("GET")
	router.HandleFunc("/sitemap", Sitemap).Methods("GET")

	router.HandleFunc("/auth/login", Login).Methods("POST")
	router.HandleFunc("/auth/logout", Logout).Methods("GET")
	router.HandleFunc("/auth/admin", IsAdmin).Methods("GET")
	ctrluser.Routes(router)
	ctrlbusiness.Routes(router)
	ctrlevent.Routes(router)
	return router
}
