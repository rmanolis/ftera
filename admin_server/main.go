package main

import (
	"ftera/admin_server/controllers"
	"ftera/admin_server/models"
	"ftera/admin_server/models/tablenames"
	"ftera/ftconfigs"
	"ftera/ftutils"
	"net/http"

	"github.com/codegangsta/negroni"
	"github.com/goincremental/negroni-sessions"
	"github.com/goincremental/negroni-sessions/cookiestore"
	"gopkg.in/mgo.v2/bson"
)

func AddAdmin(dt *ftutils.DataStore) {
	ses := dt.Session.Clone()
	defer ses.Close()
	db := ses.DB(dt.DBName)
	c := db.C(tablenames.Administrator)
	n, _ := c.Find(bson.M{"name": "admin"}).Count()
	if n == 0 {
		admin := new(adminmodels.Administrator)
		admin.Id = bson.NewObjectId()
		admin.Name = "admin"
		admin.Password = ftutils.EncodePassword("341414")
		c.Insert(admin)
	}
}

func main() {
	store := cookiestore.New([]byte("secretkey789"))
	router := ctrl.LoadRoutes()
	n := negroni.Classic()
	static := negroni.NewStatic(http.Dir("static"))
	static.Prefix = "/static"
	n.Use(static)
	n.Use(sessions.Sessions("global_session_store", store))
	ftera_dt := ftconfigs.FteraDB()
	defer ftera_dt.Session.Close()
	admin_dt := ftconfigs.AdminDB()
	defer admin_dt.Session.Close()
	AddAdmin(admin_dt)
	n.Use(negroni.HandlerFunc(ftera_dt.MgoMiddleware))
	n.Use(negroni.HandlerFunc(admin_dt.MgoMiddleware))
	n.UseHandler(router)
	n.Run(":3002")
}
