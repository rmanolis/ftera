package adminmodels

import (
	"ftera/admin_server/util"
	"ftera/fts/models"
	"ftera/fts/models/tablenames"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

type Business ftsmodels.Business

func ListBusinesses(r *http.Request, page, size int) ([]Business, error) {
	db := adminutil.GetServerDB(r)
	c := db.C(tablenames.Business)
	skip := size * page
	businesses := []Business{}
	err := c.Find(bson.M{}).Skip(skip).Limit(size).All(&businesses)
	return businesses, err
}

func CountBusinesses(r *http.Request) (int, error) {
	db := adminutil.GetServerDB(r)
	c := db.C(tablenames.Business)
	return c.Find(bson.M{}).Count()
}
