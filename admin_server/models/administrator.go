package adminmodels

import (
	"ftera/admin_server/models/tablenames"
	"ftera/admin_server/util"
	"ftera/ftutils"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

type Administrator struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Name     string
	Password string
}

func NewAdministrator(r *http.Request, name, pass string) error {
	db := adminutil.GetAdminDB(r)
	c := db.C(tablenames.Administrator)
	admin := new(Administrator)
	admin.Password = ftutils.EncodePassword(pass)
	admin.Name = name
	admin.Id = bson.NewObjectId()
	return c.Insert(admin)
}

func GetAdministrator(r *http.Request, id bson.ObjectId) (*Administrator, error) {
	db := adminutil.GetAdminDB(r)
	c := db.C(tablenames.Administrator)
	admin := new(Administrator)
	err := c.Find(bson.M{"_id": id}).One(&admin)
	return admin, err
}

func LoadAdmin(r *http.Request) (*Administrator, error) {
	id, err := adminutil.AdminId(r)
	if err != nil {
		return nil, err
	}

	return GetAdministrator(r, bson.ObjectIdHex(id))
}

func AuthenticateAdmin(r *http.Request, username, password string) (*Administrator, error) {
	db := adminutil.GetAdminDB(r)
	hex_password := ftutils.EncodePassword(password)
	admin := new(Administrator)
	err := db.C(tablenames.Administrator).Find(bson.M{
		"password": hex_password,
		"name":     username,
	}).One(&admin)
	return admin, err
}
