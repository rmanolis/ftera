package adminmodels

import (
	"ftera/admin_server/util"
	"ftera/fts/models"
	"ftera/fts/models/tablenames"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

type User ftsmodels.User

func ListUsers(r *http.Request, page, size int) ([]User, error) {
	db := adminutil.GetServerDB(r)
	c := db.C(tablenames.User)
	skip := size * page
	users := []User{}
	err := c.Find(bson.M{}).Skip(skip).Limit(size).All(&users)
	return users, err
}

func CountUsers(r *http.Request) (int, error) {
	db := adminutil.GetServerDB(r)
	c := db.C(tablenames.User)
	return c.Find(bson.M{}).Count()
}
