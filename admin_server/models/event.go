package adminmodels

import (
	"ftera/admin_server/util"
	"ftera/fts/models"
	"ftera/fts/models/tablenames"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

type Event ftsmodels.Event

func ListEvents(r *http.Request, page, size int) ([]Event, error) {
	db := adminutil.GetServerDB(r)
	c := db.C(tablenames.Event)
	skip := size * page
	events := []Event{}
	err := c.Find(bson.M{}).Skip(skip).Limit(size).All(&events)
	return events, err
}

func CountEvents(r *http.Request) (int, error) {
	db := adminutil.GetServerDB(r)
	c := db.C(tablenames.Event)
	return c.Find(bson.M{}).Count()
}
