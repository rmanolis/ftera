app.controller("NavMenuCtrl",function($scope,$http){
  var getNavigation = function() {
    return $http.get('/sitemap').success(function(data) {
      console.log(data);
      return $scope.sitemap = data;
    });
  };

  getNavigation();
  $scope.$on("successful:login", getNavigation);
  
});
