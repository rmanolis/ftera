app.controller("EventListCtrl",function($scope,$log, EventSrv){
  $scope.events=[];
  $scope.size = 10;
  $scope.currentPage = 0;
  $scope.numPages = 9;
  function listEvents(page){
    EventSrv.getEvents(page,$scope.size).success(function(data){
      console.log(data);
      $scope.total = data.Total;
      $scope.numPages = data.Pages;
      $scope.events = data.Rows;
    })
  }
  listEvents(0);

  $scope.setPage = function (pageNo) {
    console.log(pageNo);
    $scope.currentPage = pageNo;
    listEvents(pageNo);
  };
   



  

});
