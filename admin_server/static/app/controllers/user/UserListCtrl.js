app.controller("UserListCtrl",function($scope,$log, UserSrv){
  $scope.users=[];
  $scope.size = 10;
  $scope.currentPage = 0;
  $scope.numPages = 9;
  function listUsers(page){
    UserSrv.getUsers(page,$scope.size).success(function(data){
      console.log(data);
      $scope.totalUsers = data.Total;
      $scope.numPages = data.Pages;
      $scope.users = data.Rows;
    })
  }
  listUsers(0);

  $scope.setPage = function (pageNo) {
    console.log(pageNo);
    $scope.currentPage = pageNo;
    listUsers(pageNo);
  };
   



  

});
