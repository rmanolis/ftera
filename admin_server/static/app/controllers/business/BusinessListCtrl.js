app.controller("BusinessListCtrl",function($scope,$log, BusinessSrv){
  $scope.businesses=[];
  $scope.size = 10;
  $scope.currentPage = 0;
  $scope.numPages = 9;
  function listBusiness(page){
    BusinessSrv.getBusinesses(page,$scope.size).success(function(data){
      console.log(data);
      $scope.total = data.Total;
      $scope.numPages = data.Pages;
      $scope.businesses = data.Rows;
    })
  }
  listBusiness(0);

  $scope.setPage = function (pageNo) {
    console.log(pageNo);
    $scope.currentPage = pageNo;
    listBusinesses(pageNo);
  };
   
});
