app.controller("LoginCtrl",function($scope, $rootScope,$location, AuthSrv){
  $scope.user = {
    Name:"",
    Password:""
  }
  $scope.login = function (user) {
    AuthSrv.login(user).
      success(function () {
        $rootScope.$broadcast("successful:login");
        $location.path("/");
      })
    .error(function(error){
      console.log(error)
        alert('Wrong email and password');
    });
  };


});
