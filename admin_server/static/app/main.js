//loading Lodash library in angular
var lodash = angular.module('lodash', []);
lodash.factory('_', function () {
    return window._; // assumes underscore has already been loaded on the page
});


var app = angular.module('App', ['ngRoute', 
    'ui.bootstrap']);

app.run(function($rootScope,$timeout,$location,AuthSrv){
    
    // enumerate routes that don't need authentication
    var routesThatDontRequireAuth = ['/login'];
            // check if current location matches route
    var routeClean = function (route) {
        return _.find(routesThatDontRequireAuth,
            function (noAuthRoute) {
                return route === noAuthRoute;
            });
    };

    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        // if route requires auth and user is not logged in
        AuthSrv.authAdmin().error(function(){
            if (!routeClean($location.url()) ) {
                // redirect back to login
                $location.path('/login');
            }
        }).success(function(userid){
          $rootScope.adminId=userid;
        })
    });
});


app.config(function ($routeProvider) {
    
    $routeProvider
        .when('/', {
          templateUrl: '/static/app/pages/home.html',
          controller: 'HomeCtrl',
                    
        })

         .when('/users', {
          templateUrl: '/static/app/pages/user/users.html',
          controller: 'UserListCtrl',
                    
        })
         .when('/events', {
          templateUrl: '/static/app/pages/event/events.html',
          controller: 'EventListCtrl',
                    
        })
        .when('/businesses', {
          templateUrl: '/static/app/pages/business/businesses.html',
          controller: 'BusinessListCtrl',
                    
        })


        
        .when('/login', {
            templateUrl: '/static/app/pages/login.html',
            controller: 'LoginCtrl'
        })
       
});

