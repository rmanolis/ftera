app.factory("BusinessSrv", function($http,CommonSrv){
  var obj = {};
   obj.getBusinesses = function(page, size){
    var url = CommonSrv.getPageSize(page,size);
    return $http.get("/businesses" + url);
  }

  return obj;
})
