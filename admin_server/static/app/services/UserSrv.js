app.factory("UserSrv", function($http,CommonSrv){
  var obj = {};

  obj.getUsers = function(page, size){
    var url = CommonSrv.getPageSize(page,size);
    return $http.get("/users" + url);
  }
  

  return obj;
})
