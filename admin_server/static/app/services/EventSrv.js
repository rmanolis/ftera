app.factory("EventSrv", function($http,CommonSrv){
  var obj = {};
   obj.getEvents = function(page, size){
    var url = CommonSrv.getPageSize(page,size);
    return $http.get("/events" + url);
  }

  return obj;
})
