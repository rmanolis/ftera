package ftwspool

import (
	"encoding/json"
	"ftera/ftws/structs"
	"log"
)

// hub maintains the set of active connections and broadcasts messages to the
// connections.
type HubPool struct {
	// Registered connections.
	pool *SessionPool

	// Inbound messages from the connections.
	broadcast chan []byte

	onchannel chan map[*ftwsstructs.WSConn][]byte

	// Register requests from the connections.
	register chan *ftwsstructs.WSConn

	// Unregister requests from connections.
	unregister chan *ftwsstructs.WSConn
}

func NewHub() *HubPool {
	h := HubPool{
		broadcast:  make(chan []byte),
		onchannel:  make(chan map[*ftwsstructs.WSConn][]byte),
		register:   make(chan *ftwsstructs.WSConn),
		unregister: make(chan *ftwsstructs.WSConn),
		pool:       NewSessionPool(),
	}
	return &h
}

var Hub = NewHub()
var routes = map[string]func(*ftwsstructs.WSConn, *ftwsstructs.WSMessage){}

func HubRun(rs map[string]func(*ftwsstructs.WSConn, *ftwsstructs.WSMessage)) {
	routes = rs
	Hub.Run()
}

func (h *HubPool) Run() {
	for {
		select {
		case c := <-h.register:
			Hub.pool.Add(c)

		case c := <-h.unregister:
			h.pool.Remove(c)
			close(c.Send)

		case m := <-h.broadcast:
			h.pool.Broadcast(m)

		case m := <-h.onchannel:
			for k, v := range m {
				go routeChannels(k, v)
			}
		}
	}
}

func routeChannels(con *ftwsstructs.WSConn, m []byte) {
	wsm := new(ftwsstructs.WSMessage)
	err := json.Unmarshal(m, wsm)
	if err != nil {
		log.Println("could not unmarshal the wsmessage ", err)
	}
	f, ok := routes[wsm.Channel]
	if ok {
		log.Println(wsm)
		f(con, wsm)
	} else {
		log.Println("Did not find the channel ", wsm.Channel)
	}
}

func Broadcast(m []byte) {
	Hub.broadcast <- m
}

func Register(c *ftwsstructs.WSConn) {
	Hub.register <- c
	wsm := ftwsstructs.WSMessage{
		Channel: "register",
		Message: c.Id,
	}
	f, ok := routes[wsm.Channel]
	if ok {
		f(c, &wsm)
	}

}

func Unregister(c *ftwsstructs.WSConn) {
	log.Println("unregister called for ", c.Id)
	wsm := ftwsstructs.WSMessage{
		Channel: "unregister",
		Message: c.Id,
	}
	f, ok := routes[wsm.Channel]
	if ok {
		f(c, &wsm)
	}

	Hub.unregister <- c
}

func OnChannel(c *ftwsstructs.WSConn, m []byte) {
	v := map[*ftwsstructs.WSConn][]byte{
		c: m,
	}
	Hub.onchannel <- v
}

func ToSession(channel, session string, m []byte) {
	wc, ok := Hub.pool.connections[session]
	if !ok {
		log.Println("pool does not have session ", session)
		return
	}
	wc.ToChannel(channel, m)
}
