package ftwspool

import (
	"ftera/ftws/structs"
	"math/rand"
	"sync"
	"time"
)

// SessionPool is a structure for thread-safely storing sessions and broadcasting messages to them.
type SessionPool struct {
	mu          sync.RWMutex
	connections map[string]*ftwsstructs.WSConn
}

func NewSessionPool() (p *SessionPool) {
	p = new(SessionPool)
	p.connections = make(map[string]*ftwsstructs.WSConn)
	return p
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandSeq(n int) string {
	rand.Seed(time.Now().Unix())
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func createSessionId(p *SessionPool) string {
	for {
		id := RandSeq(13)
		_, ok := p.connections[id]
		if !ok {
			return id
		}
	}
}

// Add adds the given session to the session pool.
func (p *SessionPool) Add(c *ftwsstructs.WSConn) {
	p.mu.Lock()
	defer p.mu.Unlock()
	c.Id = createSessionId(p)
	p.connections[c.Id] = c
}

// Remove removes the given session from the session pool.
// It is safe to remove non-existing sessions.
func (p *SessionPool) Remove(c *ftwsstructs.WSConn) {
	p.mu.Lock()
	defer p.mu.Unlock()
	_, ok := p.connections[c.Id]
	if ok {
		delete(p.connections, c.Id)
	}
}

// Broadcast sends the given message to every session in the pool.
func (p *SessionPool) Broadcast(m []byte) {
	p.mu.RLock()
	defer p.mu.RUnlock()
	for _, c := range p.connections {
		select {
		case c.Send <- m:
		default:
			p.Remove(c)
			close(c.Send)
		}
	}
}
