package ftwsstructs

import (
	"encoding/json"
	"log"

	"github.com/gorilla/websocket"
)

// connection is an middleman between the websocket connection and the hub.
type WSConn struct {
	// The websocket connection.
	WS *websocket.Conn
	// Buffered channel of outbound messages.
	Send chan []byte
	Id   string
}

type WSMessage struct {
	Channel string `json:"channel"`
	Message string `json:"message"`
}

func (ws *WSConn) ToChannel(channel string, message []byte) {
	wsm := WSMessage{
		Channel: channel,
		Message: string(message),
	}
	out, err := json.Marshal(wsm)
	if err != nil {
		log.Println("error with jsoning wsmessage")
	}
	ws.Send <- out
}
